﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateCreater : MonoBehaviour
{
	[SerializeField]
	protected Entity_WarpPoint warpPoint;
	[SerializeField]
	private GameObject GatePrefab;
	[SerializeField]
	private GameObject LockGatePrefab;

	GameObject gate;
	Vector3 addPos;


	void Start()
	{
		CreateGate();
	}


	void CreateGate()
	{
		for (int i = 0; i < warpPoint.param.Count; i++)
		{
			var pos = new Vector3(warpPoint.param[i].Create_X, warpPoint.param[i].Create_Y, 0);

			if (warpPoint.param[i].Lock)
			{
				gate = Instantiate(LockGatePrefab, pos, Quaternion.identity);
			}
			else
			{
				gate = Instantiate(GatePrefab, pos, Quaternion.identity);
			}

			gate.transform.parent = transform;

			var nextArea = gate.GetComponent<NextArea>();

			pos = new Vector3(warpPoint.param[i].Spawn_X, warpPoint.param[i].Spawn_Y, 0f);
			nextArea.SetData(pos, warpPoint.param[i].Add, warpPoint.param[i].Lock);
		}
	}
}