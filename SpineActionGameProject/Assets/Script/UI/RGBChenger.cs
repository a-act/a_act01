﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RGBChenger : MonoBehaviour
{
	[SerializeField]
	private SpriteRenderer spriteRenderer;
	[SerializeField]
	private StanGaugeControl stanControl;
	[SerializeField]
	private bool startSwitch;

	void Update()
	{
		if (gameObject.activeSelf)
		{
			StartCoroutine(setColor());
		}
	}

	IEnumerator setColor()
	{
		if (stanControl.IsStanFlag)
		{
			spriteRenderer.color = new Color(0.5f, 0.5f, 0.5f, 1f);
		}
		else
		{
			spriteRenderer.color = new Color(1f, 1f, 1f, 1f);
		}

		yield return new WaitForSeconds(1f);

		if (startSwitch)
		{
			spriteRenderer.color = new Color(1f, 1f, 1f, 1f);
		}
		else
		{
			spriteRenderer.color = new Color(0.5f, 0.5f, 0.5f, 1f);
		}
	}
}