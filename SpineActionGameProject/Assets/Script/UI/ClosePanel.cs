﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosePanel : MonoBehaviour
{
	[SerializeField]
	GameObject titleButton;

	public void CloseWindow()
	{
		gameObject.SetActive(false);
		titleButton.SetActive(true);
	}
}