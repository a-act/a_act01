﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
	[SerializeField]
	private PauseManager pauseManager;

	public void OnActivateObj()
	{
		pauseManager.Activate();
	}
}