﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionSound : MonoBehaviour
{
	[SerializeField]
	Animator animator;

	public void OnBackButton()
	{
		animator.SetBool("Back", true);

		StartCoroutine(WaitAnimationEnd("Out"));
	}

	// アニメーション終了を判定するコルーチン.
	private IEnumerator WaitAnimationEnd(string animatorName)
	{
		bool finish = false;
		while (!finish)
		{
			AnimatorStateInfo nowState = animator.GetCurrentAnimatorStateInfo(0);
			if (nowState.IsName(animatorName))
			{
				yield return new WaitForSeconds(0.1f);
				gameObject.SetActive(false);
				finish = true;
			}
			else
			{
				yield return new WaitForSeconds(0.1f);
			}
		}
	}
}