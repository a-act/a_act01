﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageIndication : MonoBehaviour
{
	[SerializeField]
	private GameObject damageUI;    // DamageUIプレハブ.
	[SerializeField]
	private GameObject hitEffect;	// ヒット時のエフェクト.


	public void Damage(Collider2D col, int damage, bool effect = true)
	{
		#region ダメージ数値表示.
		// DamageUIをインスタンス化。登場位置は接触したコライダの中心からカメラの方向に少し寄せた位置.
		//var obj = Instantiate(damageUI,
		//					new Vector3(col.bounds.center.x + Random.Range(-1.0f, 1.0f),
		//								col.bounds.center.y + Random.Range(-2.0f, 2.0f),
		//								col.bounds.center.z),
		//					col.transform.rotation) as GameObject;

		//obj.transform.SetParent(col.transform);

		//var damagePoint = obj.transform.Find("Text").GetComponent<Text>();
		//damagePoint.text = damage.ToString();
		#endregion

		if (effect)
		{
			HitEffect(col);
		}
	}

	void HitEffect(Collider2D col)
	{
		GameObject effect = null;

		effect = Instantiate(hitEffect,
							new Vector3(col.bounds.center.x,
										col.bounds.center.y,
										col.bounds.center.z),
							Quaternion.Euler(0, 0, Random.Range(0, 180))) as GameObject;

		effect.transform.SetParent(col.transform);

		Destroy(effect, 2f);
	}
}