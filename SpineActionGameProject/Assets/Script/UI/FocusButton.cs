﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FocusButton : MonoBehaviour
{
	public void OnFocusMyButton()
	{
		EventSystem.current.SetSelectedGameObject(gameObject);
	}

	public void ActivateOrNotActivate()
	{
		EventSystem.current.SetSelectedGameObject(null);
		//gameObject.GetComponent<Button>().interactable = flag;
	}
}