﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TextController : MonoBehaviour
{
	[SerializeField, Tooltip("シナリオ進行キー")]
	private KeyCode[] keyCodes = { KeyCode.Space, KeyCode.Return };
	[SerializeField]
	private CGController cgController;
	[SerializeField]
	private Text charaName;
	[SerializeField]
	private Text textMessage;
	[SerializeField]
	private GameObject textWindow;
	[SerializeField]
	private GameObject backLogText;
	[SerializeField]
	private bool autoflag;                      // テキストオート進行モード.
	[SerializeField]
	private bool skipflag;                      // テキストスキップ進行モード.
	[SerializeField]
	Animator buttonAuto;
	[SerializeField]
	Animator buttonSkip;
	[SerializeField]
	private AudioSource audioSource;
	[SerializeField]
	private AudioClip clip;
	[SerializeField]
	private List<string> nameData = new List<string>();     // キャラ名を格納する.
	[SerializeField]
	private List<string> scenarioData = new List<string>(); // シナリオを格納する.
	[SerializeField]
	private List<int> cgNumberData = new List<int>();       // CG番号を格納する.
	[SerializeField]
	private Entity_Hscenario[] goblinHScenario;
	[SerializeField]
	private Entity_Hscenario[] bossOrcHScenario;

	//public static string scenarioLabel = string.Empty;

	private Entity_Hscenario scenario;
	private SaveLoadFile saveLoadFile;
	private ResultManager resultManager;
	private Slider timeApplyInDisplaySlider;
	private Slider autoSpeedSlider;

	private string currentName = string.Empty;  // 現在の名前（キャラ名）.
	private string currentText = string.Empty;  // 現在のテキスト（シナリオ）.

	private float timeApplyInDisplay = 0.05f;   // 1文字の表示にかける時間.
	private float timeUntilDisplay = 0;         // 表示にかかる時間.
	private float timeElapsed = 1;              // 文字列の表示を開始した時間.
	private float timeCount = 0;                // カウンタ.
	private float autoSpeed = 2.0f;             // オート進行のスピード.

	private int currentLine = 0;                // 現在の行番号.
	private int lastUpdateCharacter = -1;       // 表示中の文字数.
	private int scenarioNum = 0;                // 読み込むシナリオ番号.

	private bool inputKeyDown;                  // キー入力.

	public bool IsCompleteDisplayText
	{
		get { return Time.time > timeElapsed + timeUntilDisplay; }
	}


	void Start()
	{
		saveLoadFile = GameObject.Find("DontDestroyObjects/Manager/SaveManager").GetComponent<SaveLoadFile>();
		resultManager = GameObject.Find("DontDestroyObjects/Manager/ResultManager").GetComponent<ResultManager>();

		LoadSenario();

		#region 進行速度.
		if (timeApplyInDisplaySlider = GameObject.Find("DontDestroyObjects/Canvas/MenuPanel/InputDataPanel/TimeApplyInDisplay").GetComponent<Slider>())
		{
			timeApplyInDisplay = timeApplyInDisplaySlider.value;
		}
		else
		{
			timeApplyInDisplaySlider = null;
		}

		if (autoSpeedSlider = GameObject.Find("DontDestroyObjects/Canvas/MenuPanel/InputDataPanel/AutoSpeed").GetComponent<Slider>())
		{
			autoSpeed = autoSpeedSlider.value;
		}
		else
		{
			autoSpeedSlider = null;
		}
		#endregion

		//currentText = "Resourcesフォルダ内のテキストファイル読み込みしたテキストが表示されます。";
		currentName = nameData[currentLine];
		currentText = scenarioData[currentLine];
		timeUntilDisplay = currentText.Length * timeApplyInDisplay;
		timeElapsed = Time.time;
		lastUpdateCharacter = 0;
		currentLine++;
		ManageScroll.Log(currentName + "\n" + currentText + "\n");
	}

	void Update()
	{
		if (!textWindow.activeSelf)
		{
			autoflag = false;
			skipflag = false;
			return;
		}

		if (timeApplyInDisplaySlider != null)
		{
			timeApplyInDisplay = timeApplyInDisplaySlider.value;
		}

		if (autoSpeedSlider != null)
		{
			autoSpeed = autoSpeedSlider.value;
		}

		ShowTextCoroutine();

		int displayCharacterCount = (int)(Mathf.Clamp01((Time.time - timeElapsed) / timeUntilDisplay) * currentText.Length);
		if (displayCharacterCount > lastUpdateCharacter)
		{
			charaName.text = currentName;
			textMessage.text = currentText.Substring(0, displayCharacterCount);
			lastUpdateCharacter = displayCharacterCount;
			PlayAudio();
		}
	}

	public void PlayAudio()
	{
		audioSource.PlayOneShot(clip);
	}

	public void ChangeAuto()
	{
		autoflag = !autoflag;

		if (!autoflag)
		{
			buttonAuto.SetBool("Normal", true);
		}
		else
		{
			buttonAuto.SetBool("Highlighted", true);
		}
	}

	public void AutoOff()
	{
		buttonAuto.SetBool("Normal", true);
		autoflag = false;
	}

	public void ChangeSkip()
	{
		skipflag = !skipflag;

		if (!skipflag)
		{
			buttonSkip.SetBool("Normal", true);
		}
		else
		{
			buttonSkip.SetBool("Highlighted", true);
		}
	}

	public void SkipOff()
	{
		buttonSkip.SetBool("Normal", true);
		skipflag = false;
	}

	public void BackLogActivater()
	{
		backLogText.SetActive(!backLogText.activeSelf);
	}

	void ShowTextCoroutine()
	{
		inputKeyDown = Input.GetKeyDown(keyCodes[0]) || Input.GetKeyDown(keyCodes[1]);

		// 文字の表示が完了してるならクリック時に次の行を表示する.
		if (IsCompleteDisplayText)
		{
			if (autoflag)
			{
				if (skipflag)
				{
					buttonSkip.SetBool("Normal", true);
					skipflag = false;
				}

				if (currentLine < scenarioData.Count)
				{
					timeCount += Time.deltaTime;
					if (timeCount >= autoSpeed)
					{
						SetNextLine();
						timeCount = 0;
					}
				}
				else
				{
					ChangeAuto();
				}
			}
			else if (skipflag)
			{
				if (autoflag)
				{
					buttonAuto.SetBool("Normal", true);
					autoflag = false;
				}

				if (currentLine < scenarioData.Count)
				{
					SetNextLine();
				}
				else
				{
					ChangeSkip();
				}
			}
			else
			{
				if (inputKeyDown)
				{
					SetNextLine();
				}
			}
		}
		else
		{
			if (skipflag)
			{
				timeCount += Time.deltaTime;
				if (timeCount >= 0.1f)
				{
					timeUntilDisplay = 0;
					timeCount = 0;
				}
			}

			// 完了してないなら文字をすべて表示する
			if (inputKeyDown)
			{
				timeUntilDisplay = 0;
			}
		}
	}

	/// <summary>
	/// シナリオファイルのロード.
	/// </summary>
	void LoadSenario()
	{
		var sheet = SelectLoadData();

		for (int i = 0; i < sheet.param.Count; i++)
		{
			nameData.Add(sheet.param[i].CharaName);
			scenarioData.Add(sheet.param[i].ScenarioText);
			cgNumberData.Add(sheet.param[i].CG);
		}

		ManageScroll.LogDelete();
		currentLine = 0;
	}

	/// <summary>
	/// 緊急対応(仮実装).
	/// </summary>
	/// <returns></returns>
	Entity_Hscenario SelectLoadData()
	{
		switch (CGController.enemyName)
		{
			case Constants.ENEMY_NAME.bossOrc:
			case Constants.ENEMY_NAME.bossOrc2:
				scenarioNum = saveLoadFile.sexCount.IsBossOrc % 3;
				scenario = bossOrcHScenario[scenarioNum];
				break;
			case Constants.ENEMY_NAME.gobline:
			case Constants.ENEMY_NAME.orc:
				scenarioNum = saveLoadFile.sexCount.IsGobline % 3;
				scenario = goblinHScenario[scenarioNum];
				break;
			default:
				scenario = goblinHScenario[0];
				break;
		}
		return scenario;
	}

	/// <summary>
	/// 次の行へ.
	/// </summary>
	void SetNextLine()
	{
		// 読み込んだシナリオがまだ残っていれば次を表示.
		if (currentLine < scenarioData.Count)
		{
			if (scenarioData[currentLine] != "")
			{
				cgController.SetSpriteNumber(cgNumberData[currentLine]);
				currentName = nameData[currentLine];
				currentText = scenarioData[currentLine];
				ManageScroll.Log(currentName + "\n" + currentText + "\n");
				timeUntilDisplay = currentText.Length * timeApplyInDisplay;
				timeElapsed = Time.time;
				lastUpdateCharacter = -1;
			}
			currentLine++;
		}
		else if (currentLine == scenarioData.Count)
		{
			//currentName = "";
			//currentText = "";
			currentLine++;

			int count = 0;
			switch (CGController.enemyName)
			{
				case Constants.ENEMY_NAME.gobline:
				case Constants.ENEMY_NAME.orc:
					saveLoadFile.sexCount.IsGobline = 1;
					count = saveLoadFile.sexCount.IsGobline;
					break;
				//case Constants.ENEMY_NAME.orc:
				//	saveLoadFile.sexCount.IsOrc = 1;
				//	break;
				case Constants.ENEMY_NAME.bossOrc:
				case Constants.ENEMY_NAME.bossOrc2:
					saveLoadFile.sexCount.IsBossOrc = 1;
					count = saveLoadFile.sexCount.IsBossOrc;
					break;
			}

			resultManager.ToGameOverCG(CGController.enemyName, count);
		}
	}

	public void NextLine()
	{
		// 文字の表示が完了してるならクリック時に次の行を表示する.
		if (IsCompleteDisplayText)
		{
			if (autoflag)
			{
				autoflag = false;
			}
			else
			{
				if (currentLine < scenarioData.Count)
				{
					SetNextLine();
				}
				else if (currentLine >= scenarioData.Count)
				{
					textWindow.SetActive(false);
					SetNextLine();
				}
			}
		}
		else
		{
			// 完了してないなら文字をすべて表示する
			timeUntilDisplay = 0;
		}
	}

	IEnumerator SetTextWindowCoroutine()
	{
		if (textWindow.activeSelf)
		{
			if (IsCompleteDisplayText)
			{
				yield return null;

				if (currentLine < scenarioData.Count && currentLine >= 0)
				{
					SetNextLine();
				}
				else if (currentLine >= scenarioData.Count)
				{
					textWindow.SetActive(false);
					currentLine = 0;
					SetNextLine();
				}
			}
			else
			{
				timeUntilDisplay = 0;
			}
		}
		else
		{
			textWindow.SetActive(true);
			currentLine = 1;
		}
	}
}