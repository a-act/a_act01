﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameClearUI : MonoBehaviour
{
	[SerializeField, Tooltip("on->フェードイン, off->フェードアウト")]
	private bool fadeType;
	[SerializeField]
	private float speed = 0.01f;        // 透明化の速さ.
	[SerializeField]
	private float alphaMax = 0.6f;   // α値の最大値.
	[SerializeField, Tooltip("最初にフォーカスするゲームオブジェクト")]
	private GameObject firstSelect;

	private Text description;
	private StageManager stageManager;

	float alpha = 0f;			// α値を操作するための変数.
	float red, green, blue;     // RGBを操作するための変数.


	void Start()
	{
		stageManager = GameObject.Find("StageManager").GetComponent<StageManager>();
		description = transform.Find("MenuDescription").GetComponent<Text>();

		// Panelの色を取得.
		red = GetComponent<Image>().color.r;
		green = GetComponent<Image>().color.g;
		blue = GetComponent<Image>().color.b;

		EventSystem.current.SetSelectedGameObject(firstSelect);
	}

	void Update()
	{
		GetComponent<Image>().color = new Color(red, green, blue, alpha);

		if (fadeType)
		{
			if (alpha < alphaMax)
			{
				alpha += speed;
			}
		}
		else
		{
			if (alpha > 0f)
			{
				alpha -= speed;
			}
		}
	}

	/// <summary>
	/// NEXTステージボタンを押下した際の処理.
	/// </summary>
	/// <param name="nextStageButton"></param>
	public void NextStageButton(GameObject nextStageButton)
	{
		Destroy(this.gameObject);
	}

	/// <summary>
	/// タイトルボタンを押下した際の処理.
	/// </summary>
	public void OnClickTitle()
	{
		// タイトル画面に戻す.
		FadeManager.Instance.LoadScene(Constants.SCENE_NAME.Title.ToString(), 1.0f);
	}
}