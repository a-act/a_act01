﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenMenu : MonoBehaviour
{
	[SerializeField]
	private ActivateButton select;      // ボタンのインタラクティブに関する処理が書かれているスクリプト.

	void Start()
	{
		if (gameObject.activeSelf)
		{
			select.ActivateOrNotActivate(true);
		}
	}
}