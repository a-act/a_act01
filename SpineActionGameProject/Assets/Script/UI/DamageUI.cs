﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageUI : MonoBehaviour
{
	private Text damageText;			// ダメージを表示するテキスト.
	private float alpha;				// テキストの透明度.
	private float fadeOutSpeed = 1f;	// フェードアウトするスピード.
	[SerializeField]
	private float moveValue = 0.4f;		// 移動値.

	void Start()
	{
		damageText = GetComponentInChildren<Text>();
		// 不透明度は最初は(1.0f).
		alpha = 1f;
	}

	void LateUpdate()
	{
		// 少しずつ透明にしていく.
		alpha -= fadeOutSpeed * Time.deltaTime;
		// テキストのcolorを設定.
		damageText.color = new Color(1f, 1f, 1f, alpha);

		transform.rotation = Camera.main.transform.rotation;
		transform.position += Vector3.up * moveValue * Time.deltaTime;

		if (alpha < 0f)
		{
			Destroy(gameObject);
		}
	}
}