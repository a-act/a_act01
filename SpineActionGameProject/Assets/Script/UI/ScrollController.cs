﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollController : MonoBehaviour
{
	[SerializeField]
	RectTransform prefab;
	[SerializeField]
	int saveDataNum = 10;

	void Start()
	{
		for (int i = 1; i <= saveDataNum; i++)
		{
			var item = GameObject.Instantiate(prefab) as RectTransform;
			item.SetParent(transform, false);

			var text = item.GetComponentInChildren<Text>();
			text.text = "SaveData_" + i.ToString();
		}
	}
}