﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// ゲームオーバー時のUIを制御するクラス.
/// </summary>
public class GameOverUI : MonoBehaviour
{
	[SerializeField, Tooltip("最初にフォーカスするゲームオブジェクト")]
	private GameObject firstSelect;
	[SerializeField]
	private Animator animator;

	private SaveLoadFile saveLoadFile;


	private void OnEnable()
	{
		EventSystem.current.SetSelectedGameObject(firstSelect);
	}

	void Start()
	{
		saveLoadFile = GameObject.Find("DontDestroyObjects/Manager/SaveManager").GetComponent<SaveLoadFile>();
	}

	/// <summary>
	/// コンテニューボタンを押下した際の処理.
	/// </summary>
	public void OnClickContinue()
	{
		StartCoroutine(OnClickContinueCoroutine());
	}
	IEnumerator OnClickContinueCoroutine()
	{
		AnimatorStateInfo nowState = animator.GetCurrentAnimatorStateInfo(0);
		if (!nowState.IsName("Loop"))
		{
			yield break;
		}
		saveLoadFile.ToContinue();
		Destroy(gameObject, 1.0f);
	}

	/// <summary>
	/// タイトルボタンを押下した際の処理.
	/// </summary>
	public void OnClickTitle()
	{
		StartCoroutine(OnClickTitleCoroutine());
	}
	IEnumerator OnClickTitleCoroutine()
	{
		AnimatorStateInfo nowState = animator.GetCurrentAnimatorStateInfo(0);
		if (!nowState.IsName("Loop"))
		{
			yield break;
		}
		Destroy(gameObject, 1.0f);
		saveLoadFile.ToTitle();
	}
}