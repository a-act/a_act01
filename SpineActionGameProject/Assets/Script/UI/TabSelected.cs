﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TabSelected : MonoBehaviour
{
	KeyCode tabKey = KeyCode.Tab;
	KeyCode leftShiftKey = KeyCode.LeftShift;

	// 自身のボタンやトグル.
	private Selectable mySelectable;

	void Start()
	{
		mySelectable = GetComponent<Selectable>();
	}

	public void SetSelectable()
	{
		// 左のシフトキー、左のコントロールキーを押しながらTabキーを押した時は反対向きにフォーカスを移動する.
		if (Input.GetKeyDown(tabKey) && Input.GetKey(leftShiftKey))
		{
			EventSystem.current.SetSelectedGameObject(mySelectable.navigation.selectOnUp.gameObject);
		}
		// タブキーを押されたらSelectOnDownに選択された物をフォーカスする.
		else if (Input.GetKeyDown(tabKey))
		{
			EventSystem.current.SetSelectedGameObject(mySelectable.navigation.selectOnDown.gameObject);
		}
	}
}