﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ActivateButton : MonoBehaviour
{
	[SerializeField, Tooltip("最初にフォーカスするゲームオブジェクト")]
	private GameObject firstSelect;

	private void OnEnable()
	{
		ActivateOrNotActivate(false);
		ActivateOrNotActivate(true);
	}


	public void ActivateOrNotActivate(bool flag)
	{
		//for (int i = 1; i < transform.childCount; i++)
		//{
		//	transform.GetChild(i).GetComponent<Button>().interactable = flag;
		//}

		if (flag)
		{
			EventSystem.current.SetSelectedGameObject(firstSelect);
		}
		else
		{
			EventSystem.current.SetSelectedGameObject(null);
		}
	}
}