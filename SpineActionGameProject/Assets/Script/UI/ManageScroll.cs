﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ManageScroll : MonoBehaviour
{
	public static string Logs = "\n";	// ScrollViewに表示するログ.
	private string oldLogs = "";		// ログの差分を取得するための入れ物.
	private ScrollRect scrollRect;		// ScrollViewのScrollRect.
	private Text textLog;				// ScrollViewのText.


	void Start()
	{
		scrollRect = this.gameObject.GetComponent<ScrollRect>();
		textLog = scrollRect.content.GetComponentInChildren<Text>();
	}

	void Update()
	{
		// logsとoldLogsが異なるときにTextを更新.
		if (scrollRect != null && Logs != oldLogs)
		{
			textLog.text = Logs;
			// Textが追加されたときに５フレーム後に自動でScrollViewのBottomに移動するようにする.
			StartCoroutine(DelayMethod(5, () => { scrollRect.verticalNormalizedPosition = 0; }));
			oldLogs = Logs;
		}
	}


	/// <summary>
	/// ログを表示.
	/// </summary>
	/// <param name="logText"></param>
	public static void Log(string logText)
	{
		Logs += (logText + "\n");
		//Debug.Log(logText);
	}

	/// <summary>
	/// ログを削除.
	/// </summary>
	public static void LogDelete()
	{
		Logs = "\n";
	}

	/// <summary>
	/// 指定したフレーム数後にActionが実行される.
	/// </summary>
	/// <param name="delayFrameCount"></param>
	/// <param name="action"></param>
	/// <returns></returns>
	private IEnumerator DelayMethod(int delayFrameCount, Action action)
	{
		for (var i = 0; i < delayFrameCount; i++)
		{
			yield return null;
		}
		action();
	}
}