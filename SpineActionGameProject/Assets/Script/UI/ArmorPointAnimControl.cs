﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorPointAnimControl : MonoBehaviour
{
	public Animator[] animator;

	PlayerLife playerLife;

	private int armorPoint;
	private int armorPointMax;


	void Start()
	{
		playerLife = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLife>();
		armorPointMax = armorPoint = playerLife.IsArmorPoint;

		for (int i = 0; i < armorPoint; i++)
		{
			animator[i].SetBool("ActiveAP", true);
		}
	}

	void Update()
	{
		if (armorPoint != playerLife.IsArmorPoint)
		{
			if (armorPoint < playerLife.IsArmorPoint)
			{
				UpArmorPoint();
			}
			else
			{
				DownArmorPoint();
			}
		}
	}

	void DownArmorPoint()
	{
		armorPoint = playerLife.IsArmorPoint;

		for (int i = armorPointMax - 1; i >= armorPoint; i--)
		{
			animator[i].SetBool("ActiveAP", false);
		}
	}

	void UpArmorPoint()
	{
		armorPoint = playerLife.IsArmorPoint;

		for (int i = 0; i < armorPoint; i++)
		{
			animator[i].SetBool("ActiveAP", true);
		}
	}
}