﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapeGaugeControl : MonoBehaviour
{
	[SerializeField]
	private bool onDisplay = false;     // 表示するか.
	[SerializeField, Range(0, 50)]
	private int inputTimesMax = 10;     // 入力回数最大値.
	[SerializeField]
	private SpriteRenderer spriteBack;
	[SerializeField]
	private SpriteRenderer spriteGauge;
	[SerializeField]
	private Transform maskTransform;
	[SerializeField]
	private Player player;
	[SerializeField]
	private PlayerLife playerLife;
	[SerializeField]
	private SpriteRenderer RightButtonImage;
	[SerializeField]
	private SpriteRenderer LeftButtonImage;


	private bool escapeFlag;            // 脱出フラグ.

	private float inputTimes;			// 入力回数値.
	private float gaugeRate;
	private float maskGaugeX;           // -2.0~0(now)可変.

	private Vector2 size;               // 2.0	 (max)固定.
	private Vector2 directionalInput;   // 入力.

	private Color downColor = new Color(0.5f, 0.5f, 0.5f);
	private Color upColor = new Color(1, 1f, 1f);

	public Vector2 IsDirectionalInput
	{
		get { return directionalInput; }
		set { directionalInput = value; }
	}
	public bool IsStanFlag
	{
		get { return escapeFlag; }
	}


	void Start()
	{
		gaugeRate = 1f;
		maskGaugeX = 0f;

		// 画像サイズ(1/100)を取得.
		// spr_bg.size (100/1の画像サイズ固定).
		// spr.bounds.size(1/100の画像サイズ可変（Scaleに比例して値が変わる）).
		size = new Vector2(spriteBack.bounds.size.x, spriteBack.bounds.size.y);

		iTween.FadeTo(gameObject, iTween.Hash("alpha", 0.0f,
												"time", 0.0f));
	}

	private void Update()
	{
		if ((player.CurrentFlag & Player.FlagTypes.sex) != Player.FlagTypes.sex)
		{
			gameObject.SetActive(false);
			return;
		}
		else if (inputTimes >= inputTimesMax)
		{
			inputTimes = 0;
		}

		StanHeeling();
		GaugeUp();

		if (inputTimes >= inputTimesMax)
		{
			playerLife.ResetStanPoint();
			player.ChangeSexFlag();
		}
	}

	/// <summary>
	/// 左右連打で回復.
	/// </summary>
	public void StanHeeling()
	{
		if (inputTimes > 0f)
		{
			inputTimes -= Time.deltaTime;
		}
		else
		{
			inputTimes = 0f;
		}

		if (directionalInput.x > 0 && !escapeFlag)
		{
			// 右.
			escapeFlag = true;
			inputTimes++;

			RightButtonImage.color = downColor;
			LeftButtonImage.color = upColor;
		}
		else if (directionalInput.x < 0 && escapeFlag)
		{
			// 左.
			escapeFlag = false;
			inputTimes++;

			RightButtonImage.color = upColor;
			LeftButtonImage.color = downColor;
		}
	}

	/// <summary>
	/// 値(ゲージ)を増やす.
	/// </summary>
	public void GaugeUp()
	{
		// Lifeの割合を求める.
		gaugeRate = (float)inputTimes / (float)inputTimesMax;
		// マスクゲージの座標を求める.
		maskGaugeX = gaugeRate * size.x;
		// マスクゲージは0がMax値なの.
		float x = -(size.x - maskGaugeX);

		maskTransform.localPosition = new Vector3(x, maskTransform.localPosition.y, maskTransform.localPosition.z);

		if (inputTimes < inputTimesMax)
		{
			//iTween.Stop(gameObject);

			iTween.FadeTo(gameObject, iTween.Hash("alpha", 1.0f,
													"time", 0.5f));
		}
		else
		{
			iTween.FadeTo(gameObject, iTween.Hash("alpha", 0.0f,
													"time", 0.5f));
		}
	}

	/// <summary>
	/// 入力回数のリセット.
	/// </summary>
	public void ResetInputTimes()
	{
		inputTimes = 0;
	}

	public void AdjustmentPosition(string enemyName)
	{
		var tmpPos = transform.localPosition;
		if (enemyName == "Gobline")
		{
			tmpPos.y = 2.5f;
		}
		else if (enemyName == "Orc")
		{
			tmpPos.y = 6.5f;
		}
		else if (enemyName == "BossOrc")
		{
			tmpPos.y = 6.5f;
		}
		else if (enemyName == "AirVibrations")
		{
			tmpPos.y = 2.5f;
		}

		transform.localPosition = tmpPos;
	}
}