﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeText : MonoBehaviour
{
	[SerializeField]
	Text text;
	[SerializeField]
	Animator animator;

	public void ChangeHeaderText(Constants.HEADER_NAME name)
	{
		animator.SetBool("ChangeText", true);
		text.text = name.ToString();
		animator.SetBool("ChangeText", false);
	}

	[EnumAction(typeof(Constants.HEADER_NAME))]
	public void ChangeHeaderTextNum(int type)
	{
		Constants.HEADER_NAME name = (Constants.HEADER_NAME)type;

		animator.SetBool("ChangeText", true);
		text.text = name.ToString();
		animator.SetBool("ChangeText", false);
	}
}