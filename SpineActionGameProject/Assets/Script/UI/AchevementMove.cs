﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchevementMove : MonoBehaviour
{
	[SerializeField]
	private Vector3 panelStartPos = new Vector3(790f, 270f, 0);
	[SerializeField]
	private Vector3 panelEndPos = new Vector3(490f, 270f, 0);
	[SerializeField]
	[Tooltip("どのくらい止まっているか")]
	private float waitTime = 3.0f;

	Vector3 panelPos;

	bool iTweenMoving = false;		// 処理が終わったかどうかを示すフラグ.


	void Start()
	{
		panelPos = gameObject.GetComponent<RectTransform>().localPosition;
	}

	void Update()
	{
		StartCoroutine(PanelMoveCorutine());
	}

	/// <summary>
	/// 処理が終わったら呼び出され、フラグをクリアする.
	/// </summary>
	void OnCompleteHandler()
	{
		iTweenMoving = false;
	}

	IEnumerator PanelMoveCorutine()
	{
		if (!iTweenMoving)
		{
			// 処理中のフラグをたてとく.
			iTweenMoving = true;

			iTween.MoveTo(gameObject, iTween.Hash(
					"x", panelEndPos.x,
					"time", 1f,
					"oncomplete", "OnCompleteHandler",
					"oncompletetarget", gameObject));
		}

		yield return new WaitForSeconds(waitTime);

		if (!iTweenMoving)
		{
			// 処理中のフラグをたてとく.
			iTweenMoving = true;

			iTween.MoveTo(gameObject, iTween.Hash(
					"x", panelStartPos.x,
					"time", 1f,
					"oncomplete", "OnCompleteHandler",
					"oncompletetarget", gameObject));
		}

		yield return new WaitForSeconds(1f);

		Destroy(gameObject);

		yield return null;
	}
}