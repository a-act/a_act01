﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhereAnimator : MonoBehaviour
{
	[SerializeField]
	private Animator animator;
	[SerializeField]
	private string animName = "Back";

	public void OnWhereAnimation()
	{
		animator.SetBool(animName, true);
	}
}