﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Expose Scene Property/ExposeScene")]
public class ExposeScene : ScriptableObject
{
	public void OnActivateObj()
	{
		GameObject obj = GameObject.Find("DontDestroyObjects/Canvas/Option_Sound");
		if (obj.activeSelf)
		{
			obj.SetActive(false);
		}
		else
		{
			obj.SetActive(true);
		}
	}
}