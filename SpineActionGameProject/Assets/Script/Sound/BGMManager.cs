﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMManager : MonoBehaviour
{
	[SerializeField]
	private AudioSource audioSource;


	public bool CheckSoundPlaying()
	{
		return audioSource.isPlaying;
	}

	public void PlayBGM(AudioClip audioData)
	{
		audioSource.clip = audioData;
		audioSource.Play();
	}

	public void StopBGM()
	{
		audioSource.Stop();
	}
}