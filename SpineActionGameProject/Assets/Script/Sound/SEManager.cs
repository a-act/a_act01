﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEManager : MonoBehaviour
{
	[SerializeField]
	private AudioSource audioSource;
	public AudioClip[] audioData;


	public void PlaySE(AudioClip audioData)
	{
		audioSource.PlayOneShot(audioData);
	}

	public void PlaySelectSE()
	{
		PlaySE(audioData[0]);
	}

	public void PlayEnterSE()
	{
		PlaySE(audioData[1]);
	}
}