﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Volume : MonoBehaviour
{
	//public OptionData optionData = new OptionData();  // データを生成し保持しているスクリプト.

	public UnityEngine.Audio.AudioMixer mixer;
	public Slider timeApplyInDisplaySlider;
	public Slider autoSpeedSlider;
	public Slider masterSlider;
	public Slider bgmSlider;
	public Slider seSlider;
	public Slider hseSlider;
	public Slider voiceSlider;

	const string TIME_APPLY_IN_DISPLAY = "TimeApplyInDisplay";
	const string AUTO_SPEED = "AutoSpeed";
	const string MASTER_VOLUME = "Master";
	const string BGM_VOLUME = "BGM";
	const string SE_VOLUME = "SE";
	const string HSE_VOLUME = "HSE";
	const string VOICE_VOLUME = "Voice";

	#region Get/Set
	public float IsMasterSliderValue
	{
		get { return masterSlider.value; }
	}
	public float IsBgmSliderValue
	{
		get { return bgmSlider.value; }
	}
	public float IsSeSliderValue
	{
		get { return seSlider.value; }
	}
	public float IsHseSliderValue
	{
		get { return hseSlider.value; }
	}
	public float IsVoiceSliderValue
	{
		get { return voiceSlider.value; }
	}
	#endregion

	//private void OnGUI()
	//{
	//	Constants.ExportLogText("Master:" + optionData.IsMasterValue + ", " +
	//							"BGM:" + optionData.IsBGMValue + ", " +
	//							"SE:" + optionData.IsSEValue + ", " +
	//							"HSE:" + optionData.IsHSEValue + ", " +
	//							"Voice:" + optionData.IsVoiceValue);
	//}

	public void MasterVolume()
	{
		mixer.SetFloat("Master", masterSlider.value);
	}

	public void BGMVolume()
	{
		mixer.SetFloat("BGM", bgmSlider.value);
	}


	public void SEVolume()
	{
		mixer.SetFloat("SE", seSlider.value);
	}

	public void HSEVolume()
	{
		mixer.SetFloat("HSE", hseSlider.value);
	}

	public void VoiceVolume()
	{
		mixer.SetFloat("Voice", voiceSlider.value);
	}

	/// <summary>
	/// データを記録.
	/// </summary>
	void SetVolumeData()
	{
		//optionData.IsMasterValue = IsMasterSliderValue;
		//optionData.IsBGMValue = IsBgmSliderValue;
		//optionData.IsSEValue = IsSeSliderValue;
		//optionData.IsHSEValue = IsHseSliderValue;
		//optionData.IsVoiceValue = IsVoiceSliderValue;

		PlayerPrefs.SetFloat(MASTER_VOLUME, IsMasterSliderValue);
		PlayerPrefs.SetFloat(BGM_VOLUME, IsBgmSliderValue);
		PlayerPrefs.SetFloat(SE_VOLUME, IsSeSliderValue);
		PlayerPrefs.SetFloat(HSE_VOLUME, IsHseSliderValue);
		PlayerPrefs.SetFloat(VOICE_VOLUME, IsVoiceSliderValue);
	}

	/// <summary>
	/// データを読込.
	/// </summary>
	public void LoadVolumeData()
	{
		//masterSlider.value = optionData.IsMasterValue;
		//bgmSlider.value = optionData.IsBGMValue;
		//seSlider.value = optionData.IsSEValue;
		//hseSlider.value = optionData.IsHSEValue;
		//voiceSlider.value = optionData.IsVoiceValue;

		masterSlider.value = PlayerPrefs.GetFloat(MASTER_VOLUME, 0);
		bgmSlider.value = PlayerPrefs.GetFloat(BGM_VOLUME, 0);
		seSlider.value = PlayerPrefs.GetFloat(SE_VOLUME, 0);
		hseSlider.value = PlayerPrefs.GetFloat(HSE_VOLUME, 0);
		voiceSlider.value = PlayerPrefs.GetFloat(VOICE_VOLUME, 0);
	}


	/// <summary>
	/// システム関係のデータを記録.
	/// </summary>
	public void SetSystemValueData()
	{
		//optionData.IsTimeApplyInDisplay = timeApplyInDisplaySlider.value;
		//optionData.IsAutoSpeed = autoSpeedSlider.value;
		PlayerPrefs.SetFloat(TIME_APPLY_IN_DISPLAY, timeApplyInDisplaySlider.value);
		PlayerPrefs.SetFloat (AUTO_SPEED, autoSpeedSlider.value);
	}

	/// <summary>
	/// システム関係のデータを読込.
	/// </summary>
	void GetSystemData()
	{
		//timeApplyInDisplaySlider.value = optionData.IsTimeApplyInDisplay;
		//autoSpeedSlider.value = optionData.IsAutoSpeed;
		timeApplyInDisplaySlider.value = PlayerPrefs.GetFloat(TIME_APPLY_IN_DISPLAY, 0.05f);
		autoSpeedSlider.value = PlayerPrefs.GetFloat(AUTO_SPEED, 2f);
	}


	/// <summary>
	/// OptionDataからの値をJsonに書き込む.
	/// </summary>
	public void SaveOptopn()
	{
		SetVolumeData();
		PlayerPrefs.Save();

		//if (optionData != null)
		//{
		//	if (XJ.Unity3D.IO.FileReadWriter.WriteFileToAssets
		//		(Constants.OPTIONDATA_PATH, JsonUtility.ToJson(optionData)) == false)
		//	{
		//		// セーブ(ファイルの書き込み)に失敗したら.
		//		//dataText.text = "ファイルの書き込みに失敗しました。";
		//		Debug.Log("File Write Failed.");
		//	}
		//	else
		//	{
		//		// セーブ(ファイルの書き込み)が完了したら.
		//		//dataText.text = "セーブ完了！";
		//		Debug.Log("Save completed.");
		//	}
		//}
		//else
		//{
		//	//dataText.text = "セーブするデータがありません！";
		//	Debug.Log("There is no data to save.");
		//}
	}

	/// <summary>
	/// Jsonから値を読み込み、optionDataに設定する.
	/// </summary>
	public void LoadOption()
	{
		LoadVolumeData();

		//try
		//{
		//	optionData = null;
		//	optionData = JsonUtility.FromJson<OptionData>
		//			(XJ.Unity3D.IO.FileReadWriter.ReadFileFromAssets(Constants.OPTIONDATA_PATH));
		//}
		//catch
		//{
		//	//dataText.text = "エラー：セーブデータがないか、正常にロードができませんでした。";
		//	Debug.Log("error：There was no saved data or loading was not successful.");
		//}
		//finally
		//{
		//	if (optionData != null)
		//	{
		//		LoadVolumeData();

		//		//dataText.text = "ロード完了！";
		//		Debug.Log("Load complete!");
		//	}
		//}
	}
}