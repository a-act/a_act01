﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMChanger : MonoBehaviour
{
	public AudioClip audioData;
	public bool startChangeBGM = true;
	public bool updateCheckSoundPlaying = false;

	private BGMManager bgmManager;


	void Start()
	{
		bgmManager = GameObject.FindGameObjectWithTag("BGMManager").GetComponent<BGMManager>();

		if (startChangeBGM)
		{
			SetBGM();
		}
	}

	private void Update()
	{
		if (updateCheckSoundPlaying)
		{
			if (!bgmManager.CheckSoundPlaying())
			{
				SetBGM();
			}
		}
	}

	void SetBGM()
	{
		bgmManager.PlayBGM(audioData);
	}

	void StopBGM()
	{
		bgmManager.StopBGM();
	}
}