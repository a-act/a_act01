﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEChanger : MonoBehaviour
{
	public AudioClip audioData;

	private SEManager seManager;


	void Start()
	{
		seManager = GameObject.FindGameObjectWithTag("SEManager").GetComponent<SEManager>();
	}

	public void SetSE()
	{
		if (seManager != null)
		{
			seManager.PlaySE(audioData);
		}
	}

	public void SetSelectSE()
	{
		if (seManager != null)
		{
			seManager.PlaySelectSE();
		}
	}

	public void SetEnterSE()
	{
		if (seManager != null)
		{
			seManager.PlayEnterSE();
		}
	}
}