﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSE : MonoBehaviour
{
	[NonSerialized]
	public AudioSource audioSource;
	[SerializeField]
	private AudioClip attackSound01;
	[SerializeField]
	private AudioClip attackSound02;
	[SerializeField]
	private AudioClip attackSound03;
	[SerializeField]
	private AudioClip knifeSound;
	[SerializeField]
	private AudioClip damageSound;

	void Start()
	{
		audioSource = GameObject.Find("DontDestroyObjects/Manager/SoundManager/SE").GetComponent<AudioSource>();
	}


	public void AttackSE_01()
	{
		audioSource.PlayOneShot(attackSound01);
	}

	public void AttackSE_02()
	{
		audioSource.PlayOneShot(attackSound02);
	}

	public void AttackSE_03()
	{
		audioSource.PlayOneShot(attackSound03);
	}

	public void KnifeSound()
	{
		audioSource.PlayOneShot(knifeSound);
	}

	public void DamageSE()
	{
		audioSource.PlayOneShot(damageSound);
	}
}