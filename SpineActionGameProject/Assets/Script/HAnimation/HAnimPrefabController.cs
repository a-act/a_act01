﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HAnimPrefabController : MonoBehaviour
{
	[SerializeField]
	private GameObject[] prefab;            // モーションPrefab.
	[SerializeField, Range(0, 1)]
	private float adjustmentValue = 0.95f;	// 位置調整.

	private GameObject sexAnimation = null; // Hモーション.
	private HAnimController hAnimController;// HAnimController.
	private CGController controlCG;         // CGController.
	private Constants.ENEMY_NAME enemyName;

	/// <summary>
	/// 指定されたObjectを生成.
	/// </summary>
	/// <param name="name"></param>
	/// <param name="pos"></param>
	/// <param name="flip"></param>
	public void CreateObject(Constants.ENEMY_NAME name, Vector3 pos, bool flip)
	{
		if (sexAnimation == null)
		{
			enemyName = name;
			sexAnimation = Instantiate(prefab[(int)enemyName]) as GameObject;
			sexAnimation.GetComponent<Transform>().localPosition = new Vector3(pos.x, (pos.y - adjustmentValue), pos.z);
			hAnimController = sexAnimation.GetComponent<HAnimController>();
			hAnimController.SetFlip(flip);
		}
	}

	/// <summary>
	/// アニメーションのループを停止.
	/// </summary>
	public void AnimationLoopStop()
	{
		/*★アニメーションが止まらない(要修正)★*/
		CGController.SetEnemyName(enemyName);

		// n秒かけてシーンを遷移.
		FadeManager.Instance.LoadScene(Constants.SCENE_NAME.CG.ToString(), 5.0f);
	}

	/// <summary>
	/// オブジェクトの削除.
	/// </summary>
	public void DeleteObject()
	{
		Destroy(sexAnimation);
		hAnimController = null;
		enemyName = 0;
	}
}