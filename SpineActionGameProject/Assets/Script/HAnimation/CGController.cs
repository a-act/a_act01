﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class CGController : MonoBehaviour
{
	[SerializeField]
	private TextController textController;
	[SerializeField]
	private WindowVisibility windowVisibility;
	[SerializeField]
	private FocusButton[] focusButton;
	[SerializeField]
	private Image image;        // CGのImage.
	[Tooltip("前へ(←)")]
	public KeyCode[] returnKey; // KeyCode.LeftArrow, A, "joystick button 8".
	[Tooltip("次へ(→)")]
	public KeyCode[] nextKey;   // KeyCode.RightArrow, D, Space.
	[SerializeField, Tooltip("ショートカットキー")]
	private KeyCode[] shortcutKeyCodes = { KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.F };

	private static string imageLabel = string.Empty;

	public static Constants.ENEMY_NAME enemyName;


	private List<Sprite> sprites = new List<Sprite>();

	private SaveLoadFile saveLoadFile;
	private GameObject optionSound;
	private WhereAnimator whereAnimator;

	private int dataCount = 0;
	private int dataNumber = 0;


	public static void SetEnemyName(Constants.ENEMY_NAME name)
	{
		enemyName = name;

		imageLabel = Constants.CG_IMAGE_PATH;

		if (imageLabel == string.Empty /*|| TextController.scenarioLabel == string.Empty*/)
		{
			switch (name)
			{
				case Constants.ENEMY_NAME.gobline:
				case Constants.ENEMY_NAME.orc:
					imageLabel = Constants.CG_PATH_GOBLIN;
					//TextController.scenarioLabel = Constants.CG_LABEL_GOBLIN;
					break;
				case Constants.ENEMY_NAME.bossOrc:
				case Constants.ENEMY_NAME.bossOrc2:
					imageLabel = Constants.CG_PATH_BOSSORC;
					//TextController.scenarioLabel = Constants.CG_LABEL_BOSSORC;
					break;
				default:
					imageLabel = string.Empty;
					//TextController.scenarioLabel = string.Empty;
					break;
			}
		}
	}

	public void Awake()
	{
		saveLoadFile = GameObject.Find("DontDestroyObjects/Manager/SaveManager").GetComponent<SaveLoadFile>();
		optionSound = GameObject.Find("DontDestroyObjects/Canvas/Option_Sound");
		whereAnimator = optionSound.transform.Find("Buttons/Op_Button").GetComponent<WhereAnimator>();

		// パスが空ならばとりあえずゴブリン1でごまかす.
		if (imageLabel == string.Empty)
		{
			imageLabel = Constants.CG_IMAGE_PATH + Constants.CG_PATH_GOBLIN + Constants.ENEMYTYPE_NORMAL + Constants.PATTERN + Constants.PATTERN_01;
			//TextController.scenarioLabel = Constants.CG_LABEL_GOBLIN + Constants.ENEMY_TYPE_NORMAL + Constants.PATTERN_01;
			//Debug.LogError("CG Image Path Empty!!");
		}
		else
		{
			SelectLoadData();
		}

		LoadData();

		dataNumber = 0;

		if (sprites != null)
		{
			//Debug.Log("sprites:" + sprites[dataNumber]);
			// Imageに[dataNumber]番目の画像をセット.
			image.sprite = sprites[dataNumber];
		}
	}

	private void Update()
	{
		//if (Debug.isDebugBuild)
		//{
		//	for (int i = 0; i < nextKey.Length; i++)
		//	{
		//		if (Input.GetKeyDown(nextKey[i]))
		//		{
		//			NextImage();
		//		}
		//	}

		//	for (int i = 0; i < returnKey.Length; i++)
		//	{
		//		if (Input.GetKeyDown(returnKey[i]))
		//		{
		//			ReturnImage();
		//		}
		//	}
		//}

		InputKeyCheck();
	}

	/// <summary>
	/// データのロード.
	/// </summary>
	/// <returns></returns>
	void LoadData()
	{
		//Debug.Log("imageLabel:" + imageLabel);

		sprites.Clear();
		sprites.AddRange(Resources.LoadAll<Sprite>(imageLabel));
		//foreach (Sprite f in sprites)
		//{
		//	Debug.Log(f.name);
		//}

		// ロードが終わったら情報をクリア.
		imageLabel = string.Empty;
	}

	void InputKeyCheck()
	{
		for (int i = 0; i < shortcutKeyCodes.Length; i++)
		{
			if (Input.GetKeyDown(shortcutKeyCodes[i]))
			{
				switch (i)
				{
					case 0:
						// オートON/OFF.
						textController.SkipOff();
						textController.ChangeAuto();
						focusButton[0].OnFocusMyButton();
						break;
					case 1:
						// スキップON/OFF.
						textController.AutoOff();
						textController.ChangeSkip();
						focusButton[1].OnFocusMyButton();
						break;
					case 2:
						// オプション表示/非表示.
						textController.AutoOff();
						textController.SkipOff();
						if (optionSound.activeSelf)
						{
							whereAnimator.OnWhereAnimation();
							focusButton[2].ActivateOrNotActivate();
						}
						else
						{
							optionSound.SetActive(true);
							focusButton[2].OnFocusMyButton();
						}
						break;
					case 3:
						if (!optionSound.activeSelf)
						{
							// ログを表示/非表示.
							textController.AutoOff();
							textController.SkipOff();
							windowVisibility.WindowActivate();
							textController.BackLogActivater();
							focusButton[3].OnFocusMyButton();
						}
						else
						{
							focusButton[3].ActivateOrNotActivate();
						}
						break;
					default:
						break;
				}
			}
		}
	}

	#region Debug.
	/// <summary>
	/// 次のspriteへ切り替え.
	/// </summary>
	void NextImage()
	{
		if (dataNumber < sprites.Count - 1)
		{
			dataNumber++;
		}
		else
		{
			dataNumber = 0;
		}

		ChangeSprite();
	}

	/// <summary>
	/// 前のspriteへ切り替え.
	/// </summary>
	void ReturnImage()
	{
		if (dataNumber > 0)
		{
			dataNumber--;
		}
		else
		{
			dataNumber = sprites.Count - 1;
		}

		ChangeSprite();
	}
	#endregion

	/// <summary>
	/// spriteの切り替え.
	/// </summary>
	void ChangeSprite()
	{
		// 指定されたImageに.
		image.sprite = sprites[dataNumber];
	}

	/// <summary>
	/// spriteを外から切り替え.
	/// </summary>
	public void SetSpriteNumber(int num)
	{
		if (num != 0 && num < sprites.Count)
		{
			// 指定されたImageに.
			image.sprite = sprites[num];
		}
	}

	void SelectLoadData()
	{
		switch (enemyName)
		{
			case Constants.ENEMY_NAME.bossOrc:
			case Constants.ENEMY_NAME.bossOrc2:
				switch (saveLoadFile.sexCount.IsBossOrc % 3)
				{
					case 0:
						imageLabel += Constants.ENEMYTYPE_NORMAL + Constants.PATTERN + Constants.PATTERN_01;
						//TextController.scenarioLabel += Constants.ENEMY_TYPE_NORMAL + Constants.PATTERN_01;
						break;
					case 1:
						imageLabel += Constants.ENEMYTYPE_NORMAL + Constants.PATTERN + Constants.PATTERN_02;
						//TextController.scenarioLabel += Constants.ENEMY_TYPE_NORMAL + Constants.PATTERN_02;
						break;
					case 2:
						imageLabel += Constants.ENEMYTYPE_NORMAL + Constants.PATTERN + Constants.PATTERN_03;
						//TextController.scenarioLabel += Constants.ENEMY_TYPE_NORMAL + Constants.PATTERN_03;
						break;
					default:
						imageLabel += Constants.ENEMYTYPE_NORMAL + Constants.PATTERN + Constants.PATTERN_01;
						//TextController.scenarioLabel += Constants.ENEMY_TYPE_NORMAL + Constants.PATTERN_01;
						break;
				}
				break;
			case Constants.ENEMY_NAME.gobline:
			case Constants.ENEMY_NAME.orc:
				switch (saveLoadFile.sexCount.IsGobline % 3)
				{
					case 0:
						imageLabel += Constants.CG_PATH_GOBLIN + Constants.ENEMYTYPE_NORMAL + Constants.PATTERN + Constants.PATTERN_01;
						//TextController.scenarioLabel += Constants.ENEMY_TYPE_NORMAL + Constants.PATTERN_01;
						break;
					case 1:
						imageLabel += Constants.CG_PATH_GOBLIN + Constants.ENEMYTYPE_NORMAL + Constants.PATTERN + Constants.PATTERN_02;
						//TextController.scenarioLabel += Constants.ENEMY_TYPE_NORMAL + Constants.PATTERN_02;
						break;
					case 2:
						imageLabel += Constants.CG_PATH_GOBLIN + Constants.ENEMYTYPE_NORMAL + Constants.PATTERN + Constants.PATTERN_03;
						//TextController.scenarioLabel += Constants.ENEMY_TYPE_NORMAL + Constants.PATTERN_03;
						break;
					default:
						imageLabel += Constants.CG_PATH_GOBLIN + Constants.ENEMYTYPE_NORMAL + Constants.PATTERN + Constants.PATTERN_01;
						//TextController.scenarioLabel += Constants.ENEMY_TYPE_NORMAL + Constants.PATTERN_01;
						break;
				}
				break;
		}
		//Constants.ExportLogText("enemyName:" + enemyName + ", " + "imageLabel:" + imageLabel + ", " + "scenarioLabel:" + TextController.scenarioLabel);
	}
}