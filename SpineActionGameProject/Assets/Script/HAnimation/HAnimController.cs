﻿using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HAnimController : MonoBehaviour
{
	[SerializeField]
	SkeletonAnimation skeletonAnimation;
	[SpineAnimation(dataField: "skeletonAnimation")]
	public string firstAnim = "firstAnim";
	[SpineAnimation(dataField: "skeletonAnimation")]
	public string secondAanim = "secondAnim";
	[SpineAnimation(dataField: "skeletonAnimation")]
	public string thaadAnim = "thaadAnim";
	[SpineAnimation(dataField: "skeletonAnimation")]
	public string forthAnim = "forthAnim";
	[SpineAnimation(dataField: "skeletonAnimation")]
	public string fifthAnim = "fifthAnim";
	[SpineAnimation(dataField: "skeletonAnimation")]
	public string sixthAnim = "sixthAanim";
	[SpineAnimation(dataField: "skeletonAnimation")]
	public string seventhAnim = "seventhAnim";
	[SpineAnimation(dataField: "skeletonAnimation")]
	public string eighthAnim = "eighthAnim";

	[Range(0, 10)]
	public float waitTime = 2f;

	bool loop;

	public bool LoopEnd
	{
		set { loop = value; }
	}


	void Start()
	{
		loop = false;
		skeletonAnimation.AnimationState.SetAnimation(0, firstAnim, false);
		StartCoroutine(startHAnim());
	}

	private void Update()
	{
		if (loop)
		{
			StartCoroutine(LoopHAnim());
		}
	}

	public void SetFlip(bool flip)
	{
		skeletonAnimation.Skeleton.FlipX = flip;
	}


	/// <summary>
	/// 再生順は、1→2→3→4[→5→6→7→8]　以降[ ]内ループ
	/// 1、4、8は再生でループ処理させず、残りはループ処理
	/// </summary>
	/// <returns></returns>
	IEnumerator startHAnim()
	{
		skeletonAnimation.AnimationState.SetAnimation(0, firstAnim, false);
		skeletonAnimation.AnimationState.AddAnimation(0, secondAanim, true, 2f);
		skeletonAnimation.AnimationState.AddAnimation(0, thaadAnim, true, 6f);
		skeletonAnimation.AnimationState.AddAnimation(0, forthAnim, false, 3.9f);

		yield return new WaitForSeconds(waitTime * 2f);

		loop = true;
	}

	IEnumerator LoopHAnim()
	{
		if (!loop)
		{
			yield break;
		}
		else
		{
			skeletonAnimation.AnimationState.AddAnimation(0, fifthAnim, true, 3f);
			skeletonAnimation.AnimationState.AddAnimation(0, sixthAnim, true, 4f);
			skeletonAnimation.AnimationState.AddAnimation(0, seventhAnim, true, 3.9f);
			skeletonAnimation.AnimationState.AddAnimation(0, eighthAnim, false, 6f);

			yield return new WaitForSeconds(waitTime * 2f);
		}
	}
}