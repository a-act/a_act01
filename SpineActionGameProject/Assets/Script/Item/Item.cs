﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
	[SerializeField, Range(0, 10)]
	private int healPoint = 1;

	private PlayerLife playerLife;


	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			col.gameObject.GetComponent<PlayerLife>().ArmorPointUp();
			Destroy(gameObject);
		}
	}
}