﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemKey : MonoBehaviour
{
	[SerializeField]
	ItemsPouch itemsPouch;


	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			itemsPouch.GetBossOrcDoorKey();
			Destroy(gameObject, 0.3f);
		}	
	}
}