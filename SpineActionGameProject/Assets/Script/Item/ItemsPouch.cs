﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsPouch : MonoBehaviour
{
	bool bossOrcDoorKey = false;

	public bool IsBossOrcDoorKey
	{
		get { return bossOrcDoorKey; }
	}

	public void GetBossOrcDoorKey()
	{
		bossOrcDoorKey = true;
	}
}