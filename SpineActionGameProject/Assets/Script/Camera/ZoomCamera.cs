﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using XInputDotNetPure; // Required in C#

public class ZoomCamera : MonoBehaviour
{
	public CinemachineVirtualCamera vcamera;
	public KeyCode zoomKey = KeyCode.Q;
	public bool zoomType;

	float RightTrigger = 0f;

	GamePadState state;

	void Update ()
	{
		//RightTrigger = Input.GetAxis("ZoomCamera");
		state = GamePad.GetState(PlayerIndex.One);
		RightTrigger = state.Triggers.Right;

		if (RightTrigger > 0)
		{
			if (vcamera.m_Lens.OrthographicSize > 3.0f)
			{
				vcamera.m_Lens.OrthographicSize -= 0.1f;
			}
			else
			{
				vcamera.m_Lens.OrthographicSize = 3.0f;
			}
		}
		else if (Input.GetKey(zoomKey))
		{
			if (zoomType)
			{
				vcamera.m_Lens.OrthographicSize = 3.0f;
			}
			else
			{
				if (vcamera.m_Lens.OrthographicSize > 3.0f)
				{
					vcamera.m_Lens.OrthographicSize -= 0.1f;
				}
				else
				{
					vcamera.m_Lens.OrthographicSize = 3.0f;
				}
			}
		}
		else if (!Input.GetKey(zoomKey))
		{
			if (zoomType)
			{
				vcamera.m_Lens.OrthographicSize = 7.0f;
			}
			else
			{
				if (vcamera.m_Lens.OrthographicSize < 7.0f)
				{
					vcamera.m_Lens.OrthographicSize += 0.2f;
				}
				else
				{
					vcamera.m_Lens.OrthographicSize = 7.0f;
				}
			}
		}
	}
}
