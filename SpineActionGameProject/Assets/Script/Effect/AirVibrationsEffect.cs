﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirVibrationsEffect : MonoBehaviour
{
	private CircleCollider2D circleCollider2D;	// CircleCollider2D.

	void Start()
	{
		circleCollider2D = GetComponent<CircleCollider2D>();
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			circleCollider2D.enabled = false;

			col.gameObject.GetComponent<Player>().ForciblyStan();
			col.gameObject.GetComponent<DamageIndication>().Damage(col, 0);

			col.SendMessage("HitAttackName",
							TagUtility.getChildTagName(gameObject.transform.parent.tag),
							SendMessageOptions.DontRequireReceiver);
		}
	}
}