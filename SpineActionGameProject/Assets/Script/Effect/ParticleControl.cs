﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleControl : MonoBehaviour
{
	private ParticleSystem particle;

	void Start()
	{
		particle = this.GetComponent<ParticleSystem>();

		// パーティクルを停止.
		particle.Stop();
	}


	public void ParticlePlay()
	{
		particle.Play();
	}

	public void ParticleStop()
	{
		particle.Stop();
	}

	public IEnumerator ParticleDestroyCoroutine()
	{
		yield return new WaitWhile(() => particle.IsAlive(true));
	}
}