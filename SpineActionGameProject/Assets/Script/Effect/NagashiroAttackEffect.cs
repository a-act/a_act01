﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NagashiroAttackEffect : MonoBehaviour
{
	[SerializeField, Range(0, 50)]
	private int attackPoint = 40;

	private Animator animator;				// Animator.
	private BoxCollider2D boxCollider2D;    // BoxCollider2D.

	private bool flip;

	void Start()
	{
		boxCollider2D = GetComponent<BoxCollider2D>();
		animator = GetComponent<Animator>();
		animator.SetBool("Enabled", true);
	}

	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			//boxCollider2D.enabled = false;
			animator.SetBool("Enabled", false);

			col.SendMessage("HitAttackName",
					TagUtility.getChildTagName(gameObject.transform.tag),
					SendMessageOptions.DontRequireReceiver);

			if (gameObject.transform.rotation.y == 0)
			{
				flip = true;
			}
			else if (gameObject.transform.rotation.y == 180)
			{
				flip = false;
			}

			col.gameObject.GetComponent<Player>().Damage(attackPoint, flip);
		}
	}
}