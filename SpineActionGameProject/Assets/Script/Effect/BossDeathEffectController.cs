﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDeathEffectController : MonoBehaviour
{
	[SerializeField]
	private GameObject disappearanceEffect;
	[SerializeField, Range(0, 30)]
	private int effectMax = 10;
	[SerializeField, Range(0, 5)]
	private float effectRange = 3;

	GameObject effect;
	Vector3 transPos;


	void Start()
	{
		transPos = transform.position;

		StartCoroutine(CreateEffects());
	}

	IEnumerator CreateEffects()
	{
		for (int i = 0; i < effectMax; i++)
		{
			effect = Instantiate(disappearanceEffect, transPos, transform.rotation) as GameObject;
			effect.transform.SetParent(transform);
			effect.transform.position = new Vector3(transPos.x + Random.Range(-effectRange, effectRange),
														transPos.y + Random.Range(-effectRange, effectRange),
														-5f);

			yield return new WaitForSeconds(0.3f);
		}
		Destroy(gameObject);
	}
}