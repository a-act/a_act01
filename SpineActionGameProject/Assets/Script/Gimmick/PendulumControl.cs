﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumControl : MonoBehaviour
{
	Vector2 fulcrum = new Vector2(0,5);	// 支点.
	float rotation = 0f;				// 角度.
	float rotationSpeed = 0f;			// 角速度.
	float length = 7;					// 紐の長さ.
	float gravity = 0.5f;               // 重力加速度.


	void Update()
	{
		fulcrum = gameObject.transform.parent.position;

		// 現在の重りの位置.
		float rad = rotation * Mathf.PI / 180;
		Vector2 objPos = gameObject.transform.position;
		objPos = new Vector2(fulcrum.x + Mathf.Cos(rad) * length, fulcrum.y + Mathf.Sin(rad) * length);

		// 重力移動量を反映した重りの位置.
		Vector2 objAddPos = new Vector2(objPos.x - fulcrum.x, objPos.y - fulcrum.y);
		float t = -(objAddPos.y * gravity) / (objAddPos.x * objAddPos.x + objAddPos.y * objAddPos.y);
		Vector2 objGravityPos = new Vector2(objPos.x + t * objAddPos.x, objPos.y + gravity + t * objAddPos.y);

		// 2つの重りの位置の角度差.
		float r = Mathf.Atan2(objGravityPos.y - fulcrum.y, objGravityPos.x - fulcrum.x) * 180 / Mathf.PI;

		// 角度差を角速度に加算.
		float sub = r - rotation;
		sub -= Mathf.Floor(sub / 360.0f) * 360.0f;
		if (sub < -180.0f)
		{
			sub += 360.0f;
		}
		if (sub > 180.0f)
		{
			sub -= 360.0f;
		}
		rotationSpeed += sub;

		// 摩擦
		//rotationSpeed *= 0.995f;

		// 角度に角速度を加算.
		rotation += rotationSpeed / 50;

		// 新しい重りの位置.
		rad = rotation * Mathf.PI / 180;
		objPos = new Vector2(fulcrum.x + Mathf.Cos(rad) * length, fulcrum.y - Mathf.Sin(rad) * length);

		// 重りの座標.
		gameObject.transform.position = objPos;

		//gameObject.transform.position = new Vector2(fulcrum.x + Mathf.Cos(rotation * Mathf.PI / 180) * length,
		//											fulcrum.y + Mathf.Sin(rotation * Mathf.PI / 180) * length);
	}

	//void FixedUpdate()
	//{
	//	transform.position = Vector3.one;
	//	transform.rotation = Quaternion.identity;
	//	transform.localScale = Vector3.one;

	//	Vector3 result = transform.localToWorldMatrix.MultiplyPoint3x4(Vector3.zero);

	//	Debug.Log(result);
	//}
}