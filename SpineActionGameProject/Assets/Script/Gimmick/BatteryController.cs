﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BatteryController : MonoBehaviour
{
	[SerializeField]
	private GameObject burret;          // 弾のprefab.
	[SerializeField]
	private float shotDelay = 1f;       // 撃つ間隔.
	[SerializeField]
	private LayerMask targetLayer;      // Linecastで判定するLayer.

	private Transform transformation;

	//private bool attackRange;

	void Start()
	{
		transformation = this.transform;
	}

	//void Update()
	//{
	//	attackRange = Physics2D.Linecast(transform.position - transform.right * 0.5f,
	//						transform.position,
	//						targetLayer);
	//	#region Debug
	//	Debug.DrawLine(transform.position - transform.right * 0.5f,
	//					transform.position,
	//					Color.red);
	//	#endregion
	//}

	private void Shot()
	{
		// 弾位置と角度を調整して生成.
		GameObject obj = Instantiate(burret, transformation.position, transformation.rotation);
		obj.transform.parent = transform;
	}

	IEnumerator ShotBurret()
	{
		Shot();
		yield return new WaitForSeconds(shotDelay);
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			StartCoroutine(ShotBurret());
		}
	}
}