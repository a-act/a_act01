﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeController : MonoBehaviour
{
	[SerializeField]
	private int attackPoint = 5;		// 攻撃力.
	[SerializeField, Range(0, 10)]
	private float speed = 0.1f;         // 移動速度.
	[SerializeField]
	private LayerMask hitObjectLayer;   // Linecastで判定するLayer.

	private Rigidbody2D rigidbody2D;	// Rigidbody2d.
	private Transform transformation;   // Transform.
	private Player player;

	private float screenEdge;			// 画面外判定用.
	private bool hitObject;             // フラグ.
	private bool flipX = false;

	void Awake()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		player.IsKnifePossession = 1;
		rigidbody2D = GetComponent<Rigidbody2D>();
		transformation = this.transform;

		if (transform.rotation.y > 0)
		{
			flipX = true;
		}
		else
		{
			flipX = false;
		}

		// 画面の端のx座標を取得.
		screenEdge = Camera.main.ViewportToWorldPoint(new Vector2(0, 0)).x;
		// 移動させる.
		//rigidbody2D.velocity = transformation.right.normalized * speed;
	}

	void FixedUpdate()
	{
		//Rigidbody2Dのsimulatedがfalse(弾不使用状態)であれば何もしない.
		//if (rigidbody2D.simulated == false)
		//{
		//	return;
		//}

		transform.Translate(speed, 0, 0);

		if (transformation.position.x < screenEdge || hitObject)
		{
			//rigidbody2D.simulated = false;
			Destroy(gameObject);
		}
		Destroy(gameObject, 5f);
	}

	void Update()
	{
		#region 判定.
		hitObject = Physics2D.Linecast(transform.position - transform.right * 0.4f,
									transform.position + transform.right * 1.0f,
									hitObjectLayer);

		Debug.DrawLine(transform.position - transform.right * 0.4f,
						transform.position + transform.right * 1.0f,
						Color.red);
		#endregion
	}

	private void OnDestroy()
	{
		player.IsKnifePossession = -1;
	}

	#region Collision.
	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.layer == hitObjectLayer)
		{
			Destroy(gameObject);
		}
	}
	#endregion

	#region Collider.
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Enemy")
		{
			col.gameObject.transform.Find("Life").GetComponent<EnemyLife>().LifeDown(attackPoint);
			col.gameObject.GetComponent<DamageIndication>().Damage(col, attackPoint);
			Destroy(gameObject);
		}
		if (col.gameObject.tag == "BossEnemy")
		{
			col.gameObject.transform.Find("Life").GetComponent<BossLife>().LifeDown(attackPoint);
			col.gameObject.GetComponent<DamageIndication>().Damage(col, attackPoint);
			Destroy(gameObject);
		}
	}
	#endregion
}