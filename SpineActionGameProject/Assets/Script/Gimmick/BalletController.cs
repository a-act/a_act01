﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalletController : MonoBehaviour
{
	public int deathPoint = 1;          // 攻撃力.
	[SerializeField]
	private LayerMask hitObjectLayer;   // Linecastで判定するLayer.

	private Rigidbody2D rigidbody2D;    // Rigidbody2d.
	private Transform transformation;   // Transform.
	private float speed = 0.1f;         // 移動速度.
	private float screenEdge;           // 画面外判定用.
	private bool hitObject;             // フラグ.

	void Awake()
	{
		rigidbody2D = GetComponent<Rigidbody2D>();
		transformation = this.transform;

		// 画面の端のx座標を取得.
		screenEdge = Camera.main.ViewportToWorldPoint(new Vector2(0, 0)).x;
		// 弾を移動させる.
		//rigidbody2D.velocity = transformation.right.normalized * speed;
	}

	void FixedUpdate()
	{
		//Rigidbody2Dのsimulatedがfalse(弾不使用状態)であれば何もしない.
		//if (rigidbody2D.simulated == false)
		//{
		//	return;
		//}

		transform.Translate(speed, 0, 0);

		if (transformation.position.x < screenEdge || hitObject)
		{
			//rigidbody2D.simulated = false;
			Destroy(gameObject);
		}
		Destroy(gameObject, 5f);
	}

	void Update()
	{
		#region 判定.
		hitObject = Physics2D.Linecast(transform.position - transform.right * 0.25f,
									transform.position + transform.right * 0.25f,
									hitObjectLayer);

		Debug.DrawLine(transform.position - transform.right * 0.25f,
						transform.position + transform.right * 0.25f,
						Color.blue);
		#endregion
	}
}