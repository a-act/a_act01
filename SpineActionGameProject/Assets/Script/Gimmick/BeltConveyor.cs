﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeltConveyor : MonoBehaviour
{
	[SerializeField]
	private float scrollSpeed = 0.5f;
	private Renderer rend;

	void Start()
	{
		rend = GetComponent<Renderer>();
	}

	void FixedUpdate()
	{
		float offset = Time.time * -scrollSpeed;
		rend.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
	}
}