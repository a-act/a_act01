﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowSpear : MonoBehaviour
{
	[SerializeField]
	private GameObject spearPrefab;
	private GameObject obj;
	[SerializeField]
	private float power = 1000f;
	[SerializeField]
	private Transform spearPoint;
	// 削除するまでの時間.
	[SerializeField]
	private float deleteTime = 10f;
	[SerializeField]
	private KeyCode inputKey = KeyCode.T;


	void Update()
	{
		if (Input.GetKeyDown(inputKey))
		{
			// 槍をインスタンス化し、前方に飛ばす.
			obj = Instantiate(spearPrefab, spearPoint.position, spearPoint.rotation) as GameObject;
			obj.GetComponent<Rigidbody>().AddForce(obj.transform.forward * power);
			// 削除時間を超えたら自動で削除.
			Destroy(obj, deleteTime);
		}
	}
}