﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwayingFloor : MonoBehaviour
{
	#region Field
	private bool move = false;
	private int timer = 0;
	[SerializeField]
	private int timeLimitMax = 60;
	[SerializeField]
	private Rigidbody2D rigidbody2D;
	[SerializeField]
	private BoxCollider2D boxCollider2d;
	#endregion


	void Update()
	{
		if (!move)
		{
			if (gameObject.transform.position.y < -20)
			{
				gameObject.SetActive(false);
			}
			return;
		}

		if (timer < timeLimitMax)
		{
			timer++;
		}
		else
		{
			rigidbody2D.gravityScale = 3;
			rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
			boxCollider2d.enabled = false;
			//gameObject.transform.position = new Vector3(transform.position.x,
			//											Mathf.PingPong(Time.time, 10),
			//											0f);
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			move = true;
		}
	}

	void OnCollisionExit2D(Collision2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			move = false;
			timer = 0;
		}
	}
}