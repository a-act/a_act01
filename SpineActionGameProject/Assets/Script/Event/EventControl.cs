﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventControl : MonoBehaviour
{
	#region Field
	[SerializeField]
	private GameObject saveScreen;
	[SerializeField]
	private Pausable pausable;
	private GameObject player;


	void start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
	}

	/// <summary>
	/// イベントの種類
	/// </summary>
	public enum EVENT_TYPE
	{
		COLLISION_ENTER_2D,     // オブジェクトが物理的に衝突したとき.
		COLLIDER_ENTER_2D,      // オブジェクトが交差したとき.
		SAVE_POINT,             // セーブポイント.
		ENEMY_DEATH             // 敵が死んだとき.
	}
	[Header("イベントのタイプ")]
	[SerializeField]
	private EVENT_TYPE eventType;
	#endregion


	//void OnCollisionEnter2D(Collision2D col)
	private void OnTriggerEnter2D(Collider2D col)
	{
		//if (col.gameObject == player)
		if (col.gameObject.tag == "Player")
		{
			if (eventType == EVENT_TYPE.COLLIDER_ENTER_2D)
			{
				StartCoroutine(PausableCoroutine());
			}
			else if (eventType == EVENT_TYPE.SAVE_POINT)
			{
				saveScreen.SetActive(true);
				StartCoroutine(PausableCoroutine());
			}
		}
	}


	IEnumerator PausableCoroutine()
	{
		pausable.SetPausable = true;
		yield return null;
	}
}
