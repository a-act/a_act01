﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Pauser : MonoBehaviour
{
	static List<Pauser> targets = new List<Pauser>();   // ポーズ対象のスクリプト.
	Behaviour[] pauseBehavs = null; // ポーズ対象のコンポーネント.

	void Start()
	{
		// ポーズ対象に追加する.
		targets.Add(this);
	}

	private void OnDestroy()
	{
		// ポーズ対象から除外する.
		targets.Remove(this);
	}

	/// <summary>
	/// ポーズされたとき.
	/// </summary>
	void OnPause()
	{
		if (pauseBehavs != null)
		{
			return;
		}

		// 有効なBehaviourを取得.
		pauseBehavs = Array.FindAll(GetComponentsInChildren<Behaviour>(), (obj) => { return obj.enabled; });

		foreach (var com in pauseBehavs)
		{
			com.enabled = false;
		}
	}

	/// <summary>
	/// ポーズ解除されたとき.
	/// </summary>
	void OnResume()
	{
		if (pauseBehavs == null)
		{
			return;
		}

		// ポーズ前の状態にBehaviourの有効状態を復元.
		foreach (var com in pauseBehavs)
		{
			com.enabled = true;
		}

		pauseBehavs = null;
	}

	/// <summary>
	/// ポーズ.
	/// </summary>
	public static void Pause()
	{
		foreach (var obj in targets)
		{
			obj.OnPause();
		}
	}

	/// <summary>
	/// ポーズ解除.
	/// </summary>
	public static void Resume()
	{
		foreach (var obj in targets)
		{
			obj.OnResume();
		}
	}
}