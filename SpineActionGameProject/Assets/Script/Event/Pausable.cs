﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Rigidbodyの速度を保存しておくクラス.
/// </summary>
public class RigidbodyVelocity
{
	public Vector3 velocity;
	public float angularVeloccity;
	public RigidbodyVelocity(Rigidbody2D rigidbody)
	{
		velocity = rigidbody.velocity;
		angularVeloccity = rigidbody.angularVelocity;
	}
}

public class Pausable : MonoBehaviour
{
	[SerializeField]
	private bool pausing;						// 現在Pause中か？.
	[Tooltip("一時停止の対象外にするGameObject")]
	public GameObject[] ignoreGameObjects;      // 無視するGameObject.
	[SerializeField]
	private GameObject pauseObject;

	bool prevPausing;							// ポーズ状態が変更された瞬間を調べるため、前回のポーズ状況を記録しておく.

	RigidbodyVelocity[] rigidbodyVelocities;    // Rigidbodyのポーズ前の速度の配列.
	Rigidbody2D[] pausingRigidbodies;			// ポーズ中のRigidbodyの配列.
	MonoBehaviour[] pausingMonoBehaviours;      // ポーズ中のMonoBehaviourの配列.
	Animator[] animators;						// ポーズ中のAnimatorの配列.

	public bool SetPausable
	{
		get { return pausing; }
		set { pausing = value; }
	}


	/// <summary>
	/// 更新処理.
	/// </summary>
	void Update()
	{
		// ポーズ状態が変更されていたら、Pause/Resumeを呼び出す。.
		if (prevPausing != pausing)
		{
			if (pausing && pauseObject.activeSelf)
			{
				// ポーズ.
				Pause();
			}
			else
			{
				// ポーズ解除.
				Resume();
			}
			prevPausing = pausing;
		}
	}

	/// <summary>
	/// 中断.
	/// </summary>
	void Pause()
	{
		// Rigidbodyの停止.
		// 子要素から、スリープ中でなく、IgnoreGameObjectsに含まれていないRigidbodyを抽出.
		Predicate<Rigidbody2D> rigidbodyPredicate =
			obj => !obj.IsSleeping() && Array.FindIndex(ignoreGameObjects, gameObject => gameObject == obj.gameObject) < 0;
		pausingRigidbodies = Array.FindAll(transform.GetComponentsInChildren<Rigidbody2D>(), rigidbodyPredicate);
		rigidbodyVelocities = new RigidbodyVelocity[pausingRigidbodies.Length];
		for (int i = 0; i < pausingRigidbodies.Length; i++)
		{
			// 速度、角速度も保存しておく.
			rigidbodyVelocities[i] = new RigidbodyVelocity(pausingRigidbodies[i]);
			pausingRigidbodies[i].Sleep();
		}

		// MonoBehaviourの停止.
		// 子要素から、有効かつこのインスタンスでないもの、IgnoreGameObjectsに含まれていないMonoBehaviourを抽出.
		Predicate<MonoBehaviour> monoBehaviourPredicate =
			obj => obj.enabled &&
				   obj != this &&
				   Array.FindIndex(ignoreGameObjects, gameObject => gameObject == obj.gameObject) < 0;
		pausingMonoBehaviours = Array.FindAll(transform.GetComponentsInChildren<MonoBehaviour>(), monoBehaviourPredicate);
		foreach (var monoBehaviour in pausingMonoBehaviours)
		{
			monoBehaviour.enabled = false;
		}

		// Animatorの停止.
		// 子要素から、有効でIgnoreGameObjectsに含まれていなAnimatorを抽出.
		Predicate<Animator> animatorPredicate =
		obj => obj.enabled && Array.FindIndex(ignoreGameObjects, gameObject => gameObject == obj.gameObject) < 0;
		animators = Array.FindAll(transform.GetComponentsInChildren<Animator>(), animatorPredicate);
		foreach (var animator in animators)
		{
			animator.SetFloat("MovingSpeed", 0.0f);
		}
	}

	/// <summary>
	/// 再開.
	/// </summary>
	void Resume()
	{
		// Rigidbodyの再開.
		for (int i = 0; i < pausingRigidbodies.Length; i++)
		{
			pausingRigidbodies[i].WakeUp();
			pausingRigidbodies[i].velocity = rigidbodyVelocities[i].velocity;
			pausingRigidbodies[i].angularVelocity = rigidbodyVelocities[i].angularVeloccity;
		}

		// MonoBehaviourの再開.
		foreach (var monoBehaviour in pausingMonoBehaviours)
		{
			if (monoBehaviour != null)
			{
				monoBehaviour.enabled = true;
			}
		}

		foreach (var animator in animators)
		{
			if (animator != null)
			{
				animator.SetFloat("MovingSpeed", 1.0f);
			}
		}
	}
}