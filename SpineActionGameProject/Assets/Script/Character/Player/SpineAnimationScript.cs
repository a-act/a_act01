﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class SpineAnimationScript : StateMachineBehaviour
{
	[Tooltip("アニメーション名")]
	public string animationName;
	[Tooltip("再生速度")]
	public float speed = 1.0f;
	[Tooltip("ループするか")]
	public bool loop;

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		SkeletonAnimation skeletonAnimation = animator.GetComponentInChildren<SkeletonAnimation>();
		skeletonAnimation.state.SetAnimation(0, animationName, loop).timeScale = speed;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// 遷移が終了し、ステートマシンがこの状態の評価を終了すると、OnStateExitが呼び出されます.
	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state.
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (stateInfo.IsName("damage"))
		{
			animator.ResetTrigger("damage");
		}
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}