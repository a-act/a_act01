﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLife : MonoBehaviour
{
	#region Field
	[SerializeField, Range(1, 999), Tooltip("現在の体力値")]
	private int nowLife = 400;      // 現在の体力値.
	[SerializeField, Range(0.1f, 1f), Tooltip("体力ゲージの最大何割")]
	private float fillProp = 0.75f; // Lifeゲージの最大何割.
	[SerializeField, Range(0.1f, 10.0f), Tooltip("暫定ダメージゲージの動く速度")]
	private float moveTime = 2.0f;  // 暫定ダメージゲージの動く速度.
	[SerializeField, Range(0.1f, 10.0f), Tooltip("ゲージを動かすまでの待ち時間")]
	private float waitTime = 2.0f;  // ダメージを受けてからゲージを動かすまでのインターバル.
	[SerializeField, Range(0, 10), Tooltip("アーマーポイント(AP)")]
	private int armorPoint = 5;     // アーマーの耐久値.
	[SerializeField, Range(0, 10), Tooltip("スタンするまでの耐性値")]
	private int stanPoint = 3;      // スタンの耐性値.
	[SerializeField, Range(0, 100), Tooltip("回復量")]
	private int healPoint = 10;     // 回復量.
	[SerializeField]
	private DamageIndication damageIndication;  // ダメージ表示.
	[SerializeField]
	private CapsuleCollider2D capsuleCollider2D;// CapsuleCollider2D.
	[SerializeField]
	private GameObject heelEffect;   // 回復時のエフェクト.

	private Image lifeGauge;            // Lifeゲージ（緑色ゲージ）.
	private Image interimLifeGauge;     // 暫定ダメージゲージ（黄色ゲージ）.
	private ResultManager resultManager;

	private int maxLife;            // 最大体力値.
	private int armorPointMax;      // アーマーポイントの最大値.
	private int stanPointMax;       // スタンするまでの回数の最大値.

	#region Get/Set
	public int IsMaxLife
	{
		get { return maxLife; }
	}
	public int IsNowLife
	{
		get { return nowLife; }
		set { nowLife = value; }
	}
	public int IsArmorPoint
	{
		get { return armorPoint; }
	}
	public int IsStanPoint
	{
		get { return stanPoint; }
	}
	#endregion
	#endregion


	void Start()
	{
		resultManager = GameObject.Find("DontDestroyObjects/Manager/ResultManager").GetComponent<ResultManager>();
		lifeGauge = GameObject.Find("CanvasUI/PlayerStatus(Clone)/Op_Position/Op_HP/Op_Status_HP_Bar").GetComponent<Image>();
		interimLifeGauge = GameObject.Find("CanvasUI/PlayerStatus(Clone)/Op_Position/Op_HP/Op_Status_HP_Bar_AfterImage").GetComponent<Image>();

		maxLife = nowLife;
		lifeGauge.fillAmount *= ((float)nowLife / (float)maxLife) * fillProp;
		interimLifeGauge.fillAmount *= ((float)nowLife / (float)maxLife) * fillProp;

		armorPointMax = armorPoint;
		stanPointMax = stanPoint;
	}


	/// <summary>
	/// Life回復.
	/// </summary>
	public void LifeUp()
	{
		if (nowLife > 0 && armorPoint > 0)
		{
			armorPoint--;
			SetLife(nowLife + healPoint);
			downLifeGreenGauge(nowLife + healPoint);
			nowLife += healPoint;

			if (nowLife > maxLife)
			{
				nowLife = maxLife;
			}

			HeelEffect();
		}
	}

	/// <summary>
	/// Life減算.
	/// </summary>
	/// <param name="attackPoint"></param>
	public void LifeDown(int attackPoint = 0)
	{
		if (armorPoint > 0)
		{
			attackPoint /= 2;
			armorPoint--;
		}
		else if (stanPoint > 0)
		{
			stanPoint--;
		}

		SetLife(nowLife - attackPoint);
		downLifeGreenGauge(nowLife - attackPoint);
		nowLife -= attackPoint;
		damageIndication.Damage(capsuleCollider2D, attackPoint);

		if (nowLife <= 0)
		{
			StartCoroutine(GameOverCoroutine());
		}
	}

	/// <summary>
	/// Hアニメ中のLifeダウン.
	/// </summary>
	/// <param name="point"></param>
	public bool HAnimLifeDown(int point = 0)
	{
		if (nowLife > 0)
		{
			SetLife(nowLife - point);
			downLifeGreenGauge(nowLife - point);
			nowLife -= point;
			damageIndication.Damage(capsuleCollider2D, point, false);

			return true;
		}

		return false;
	}

	/// <summary>
	/// 即死.
	/// </summary>
	public void LifeIsDeath()
	{
		SetLife(0);
		downLifeGreenGauge(0);
		nowLife = 0;

		StartCoroutine(GameOverCoroutine());
	}

	/// <summary>
	/// AP回復.
	/// </summary>
	public void ArmorPointUp()
	{
		armorPoint++;

		if (armorPoint > armorPointMax)
		{
			armorPoint = armorPointMax;
		}
	}

	public void ArmorStanPointClear()
	{
		armorPoint = 0;
		stanPoint = 0;
	}

	// スタン耐性値をリセット.
	public void ResetStanPoint()
	{
		stanPoint = stanPointMax;
	}

	/// <summary>
	/// 値(Life)をセット.
	/// </summary>
	public void ResetLife()
	{
		StartCoroutine(ResetLifeCoroutine());
	}

	/// <summary>
	/// ゲージを動かす.
	/// </summary>
	/// <param name="point"></param>
	void SetLife(float point)
	{
		if (point > maxLife)
		{
			point = maxLife;
		}

		iTween.ValueTo(interimLifeGauge.gameObject, iTween.Hash(
						"from", nowLife,
						"to", point,
						"time", moveTime,
						"delay", waitTime,
						"easetype", iTween.EaseType.linear,
						"onupdate", "downInterimLifeGauge",
						"onupdatetarget", gameObject));
	}

	/// <summary>
	/// 緑ゲージの見た目変更.
	/// </summary>
	/// <param name="point"></param>
	void downLifeGreenGauge(float point)
	{
		if (point > maxLife)
		{
			point = maxLife;
		}
		lifeGauge.fillAmount = (point / (float)maxLife) * fillProp;
	}

	/// <summary>
	/// 赤ゲージの見た目変更.
	/// </summary>
	/// <param name="damage"></param>
	void downInterimLifeGauge(float damage)
	{
		if (nowLife <= maxLife)
		{
			interimLifeGauge.fillAmount = (damage / (float)maxLife) * fillProp;
		}
	}

	/// <summary>
	/// 回復エフェクト生成.
	/// </summary>
	void HeelEffect()
	{
		GameObject effect = null;

		effect = Instantiate(heelEffect,
							transform.position,
							Quaternion.identity) as GameObject;

		effect.transform.SetParent(transform);

		Destroy(effect, 1f);
	}

	/// <summary>
	/// Lifeゲージのリセット.
	/// </summary>
	/// <returns></returns>
	IEnumerator ResetLifeCoroutine()
	{
		nowLife = maxLife;
		armorPoint = armorPointMax;
		stanPoint = stanPointMax;
		lifeGauge.fillAmount = ((float)nowLife / (float)maxLife) * fillProp;
		interimLifeGauge.fillAmount = ((float)nowLife / (float)maxLife) * fillProp;
		yield return null;
	}

	/// <summary>
	/// ゲームオーバー.
	/// </summary>
	/// <returns></returns>
	IEnumerator GameOverCoroutine()
	{
		yield return new WaitForSeconds(3.0f);
		resultManager.ToGameOver();
	}
}