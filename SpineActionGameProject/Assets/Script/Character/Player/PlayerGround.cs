﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGround : MonoBehaviour
{
	bool isGround = false;

	public bool IsGround
	{
		get { return isGround; }
	}


	void Start()
	{
		isGround = false;
	}

	void Update()
	{
		//Debug.Log("isGround=" + isGround);
	}

	// オブジェクトが離れた時.
	void OnCollisionExit2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Stage")
		{
			isGround = false;
			Debug.Log("false");
		}
	}

	// オブジェクトが衝突した(接触している)時.
	void OnCollisionStay2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Stage")
		{
			isGround = true;
		}
	}
}
