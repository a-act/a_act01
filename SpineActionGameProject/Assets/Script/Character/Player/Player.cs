﻿using System.Collections;
using UnityEngine;
using Spine.Unity;
using System;
using Spine;
using XInputDotNetPure; // Required in C#

public class Player : MonoBehaviour
{
	#region Field
	[SerializeField, Range(0, 20), Tooltip("歩く速度")]
	private float walkMoveSpeed = 3f;               // 歩く速度.
	[SerializeField, Range(0, 20), Tooltip("走る速度")]
	private float runMoveSpeed = 10f;               // 走る速度.
	[SerializeField, Range(0, 10), Tooltip("ジャンプ力")]
	private float jumpPower = 7f;                   // ジャンプ力.
													//[SerializeField, Range(0, 10), Tooltip("重力")]
													//private float gravity = 3f;                     // 重力.
	[SerializeField, Range(0, 10), Tooltip("無敵時間")]
	private float invincibleTimer = 1.0f;           // 無敵時間.
	[SerializeField, Range(0, 10), Tooltip("コントローラの振動時間")]
	private float vibrationTime = 1f;               // コントローラの振動時間.
	[SerializeField]
	private Vector2 blowOffPower = new Vector2(5, 5); // ダメージを食らった際に吹き飛ぶ力の設定.
	[SerializeField, Range(0, 20), Tooltip("点滅")]
	private float flashingSpeed = 10;               // 点滅速度.
	[SerializeField, Range(0, 10), Tooltip("Hアニメ中にn秒ごとにダメージを受けるか")]
	private float hAnimDamageTime = 1f;             // ダメージ間隔.
	[SerializeField, Range(0, 5)]
	private int knifeMax;                           // ナイフの最大数.
	#region Layer.
	[SerializeField]
	private LayerMask groundLayer;                  // Linecastで判定するLayer.
	[SerializeField]
	private LayerMask obstacleLayer;                // Linecastで判定するLayer.
	#endregion
	#region Script.
	[SerializeField]
	private PlayerWeapon playerWeapon;              // 武器(剣).
	[SerializeField]
	private PlayerStamina playerStamina;            // スタミナ.
	[SerializeField]
	private StanGaugeControl stanGaugeControl;      // stanGaugeControl.
	[SerializeField]
	private EscapeGaugeControl escapeGaugeControl;  // escapeGaugeControl.
	[SerializeField]
	private PlayerSE playerSE;                      // SoundEffect.
	[SerializeField]
	private HAnimPrefabController hAnimPrefabController;    // Hオブジェクトの制御.
	#endregion
	[SerializeField]
	private new Rigidbody2D rigidbody2D;            // Rigidbody2D.
	[SerializeField]
	private CapsuleCollider2D capsuleCollider2D;    // CapsuleCollider2D.
	#region Prefabs.
	[SerializeField]
	private GameObject knife;                       // ナイフのprefab.
	[SerializeField]
	private GameObject stanGauge;                   // スタンゲージのprefab.
	[SerializeField]
	private GameObject escapeGauge;                 // 脱出ゲージのprefab.
	[SerializeField]
	private GameObject dashEffect;                  // ダッシュ時のエフェクトのprefab.
	[SerializeField]
	private GameObject jumpEffect;                  // ジャンプ時のエフェクトのprefab.
	[SerializeField]
	private GameObject airJumpEffect;               // 空中ジャンプ時のエフェクトのprefab.
	#endregion

	public PlayerLife playerLife;                   // ライフ.

	private InputManager inputManager;              // InputManager.
	private GameObject effect;
	private Vector3 transPos;                       // transformPosition.
	private Vector2 directionalInput;               // 入力.
	private Vector2 StandardColliderOffset;         // コライダーの標準位置.
	private Vector2 StandardColliderSize;           // コライダーの標準サイズ.

	private bool flipX = false;                     // 向き(true:左,false:右).
	private bool groundedFlag = false;              // 着地判定.
	private bool steppingFlag = false;              // ステップ移動実行中フラグ.
	private bool inputAttackKeyFlag = false;        // キーが押された後かのフラグ.
	private bool obstacleFlag = false;              // 障害物に触れているか.

	private int storingLife;                        // Life保存用.
	private int jumpCount = 0;                      // ジャンプカウンタ.
	private int knifePossession = 0;                // ナイフの所持数.
	private int attackNum = 1;                      // 攻撃回数保管用.

	private float dashTime = 0;                     // ダッシュからの経過時間.
	private float movingDistance = 8f;              // ダッシュの移動距離.
	private float attackTime = 0;                   // 攻撃からの経過時間.
	private float hAnimDamageTimeCount = 0;

	private string attackEnemyName = string.Empty;

	[Flags]
	public enum FlagTypes
	{
		nearAttack = 0x01,  // 近接攻撃.
		farAttack = 0x02,   // 遠距離攻撃.
		damage = 0x04,      // 攻撃を受けたか.
		invincible = 0x08,  // 無敵モード中.
		death = 0x10,       // 死んだか.
		descend = 0x20,     // 強制的に降りるか？.
		knockBack = 0x40,   // ノックバック.
		stan = 0x80,        // スタン.
		sex = 0x100,        // Hモーション.
		flag10 = 0x200,     // フラグ10.
		flag11 = 0x400,     // フラグ11.
		flag12 = 0x800,     // フラグ12.
	}
	protected FlagTypes currentFlag;

	[Flags]
	private enum SpineAnimationNames
	{
		stay = 0x0001,          // 待機.
		run = 0x0002,           // 走り(移動).
		crouch = 0x0004,        // しゃがみ.
		jump = 0x0008,          // ジャンプ.
		descend = 0x0010,       // 降下.
		nearAttack = 0x0020,    // 近接攻撃.
		crouchAttack = 0x0040,  // 遠距離攻撃.
		farAttack = 0x0080,     // ダメージ(のけぞり).
		damage = 0x0100,        // ダウン.
		stan = 0x0200,          // スタン.
		dash = 0x0400,          // ダッシュ.
		flag12 = 0x0800,        // フラグ12.
	}
	private SpineAnimationNames spineAnimName;
	private SpineAnimationNames tmpName;

	#region Spineモーション.
	[SerializeField]
	private SkeletonAnimation skeletonAnimation;
	[SerializeField]
	private Spine.Unity.Modules.SkeletonGhost skeletonGhost;
	//[SpineAnimation("Attakc")] 名前が決まってればSpineObjectをアタッチするだけでOK!.
	[SpineAnimation("stay")]
	public string waitingAnim = "stay";                                 // 待機.
	[SpineAnimation("stay_break")]
	public string waitingBreakAnim = "stay_break";                      // 待機(裸).
	[SpineAnimation("run")]
	public string runAnim = "run";                                      // 走る.
	[SpineAnimation("run_break")]
	public string runBreakAnim = "run_break";                           // 走る(裸).
	[SpineAnimation("dash")]
	public string dashAnim = "dash";                                    // ダッシュ.
	[SpineAnimation("dash_break")]
	public string dashBreakAnim = "dash_break";                         // ダッシュ(裸).
	[SpineAnimation("jump")]
	public string jumpAnim = "jump";                                    // ジャンプ.
	[SpineAnimation("jump_break")]
	public string jumpBreakAnim = "jump_break";                         // ジャンプ(裸).
	[SpineAnimation("jump_down")]
	public string descendAnim = "jump_down";                            // 降下.
	[SpineAnimation("jump_down_break")]
	public string descendBreakAnim = "jump_down_break";                 // 降下(裸).
	[SpineAnimation("attack_01")]
	public string nearAttackAnim_1 = "attack_01";                       // 近接攻撃1.
	[SpineAnimation("attack_01_break")]
	public string nearAttackBreakAnim_1 = "attack_01_break";            // 近接攻撃1(裸).
	[SpineAnimation("attack_02")]
	public string nearAttackAnim_2 = "attack_02";                       // 近接攻撃2.
	[SpineAnimation("attack_02_break")]
	public string nearAttackBreakAnim_2 = "attack_02_break";            // 近接攻撃2(裸).
	[SpineAnimation("attack_03")]
	public string nearAttackAnim_3 = "attack_03";                       // 近接攻撃3.
	[SpineAnimation("attack_03_break")]
	public string nearAttackBreakAnim_3 = "attack_03_break";            // 近接攻撃3(裸).
	[SpineAnimation("jump_attack_01")]
	public string jumpAttackAnim = "jump_attack_01";                    // ジャンプ攻撃.
	[SpineAnimation("jump_attack_01_break")]
	public string jumpAttackBreakAnim = "jump_attack_01_break";         // ジャンプ攻撃(裸).
	[SpineAnimation("sub_attack_01")]
	public string farAttackAnim = "sub_attack_01";                      // 遠距離攻撃.
	[SpineAnimation("sub_attack_01_break")]
	public string farAttackBreakAnim = "sub_attack_01_break";           // 遠距離攻撃(裸).
																		/*
																			[SpineAnimation(dataField: "skeletonAnimation")]
																			public string specialSkillAnim = "SpecialSkill";	// 必殺技.
																		*/
	[SpineAnimation("syagamu")]
	public string crouchAnim = "syagamu";                               // しゃがみ.
	[SpineAnimation("syagamu_break")]
	public string crouchBreakAnim = "syagamu_break";                    // しゃがみ(裸).
	[SpineAnimation("syagamu_attack_01")]
	public string crouchAttackAnim = "syagamu_attack_01";               // しゃがみ攻撃.
	[SpineAnimation("syagamu_attack_01_break")]
	public string crouchAttackBreakAnim = "syagamu_attack_01_break";    // しゃがみ攻撃(裸).
																		/*	[SpineAnimation("walk_break")]
																			public string clothesBreakWalkAnim = "ClothesBreakWalk";// 歩き(裸).
																			[SpineAnimation("walk_break_01")]
																			public string clothesBreakAshamedWalkAnim = "ClothesBreakAshamedWalk";// 恥じらい歩き.
																		*/
	[SpineAnimation("damage")]
	public string damageAnim = "damage";                                // ダメージ.
	[SpineAnimation("damage_break")]
	public string damageBreakAnim = "damage_break";                     // ダメージ.
	[SpineAnimation("down_break")]
	public string downBreakAnim = "down_break";                         // ダウン中(スタン中).
	#endregion

	#region Get Set
	public FlagTypes CurrentFlag
	{
		get { return currentFlag; }
	}
	public bool IsFlipX
	{
		get { return flipX; }
	}
	public Vector3 IsPos
	{
		get { return transPos; }
		set { transPos = value; }
	}
	public int IsLife
	{
		get { return playerLife.IsNowLife; }
		set { playerLife.IsNowLife = value; }
	}
	public int IsKnifePossession
	{
		get { return knifePossession; }
		set { knifePossession += value; }
	}
	public Vector2 IsDirectionalInput
	{
		get { return directionalInput; }
	}
	#endregion
	#endregion


	//private void OnGUI()
	//{
	//	//Debug.Log("rigidbody2D:" + rigidbody2D.velocity);
	//	//Debug.Log("currentFlag:" + currentFlag);
	//	//Debug.Log("spineAnimName:" + spineAnimName);
	//	//Constants.ExportLogText("currentFlag:" + currentFlag + ", " + "spineAnimName:" + spineAnimName);
	//}


	void Start()
	{
		inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();

		storingLife = playerLife.IsNowLife;
		StandardColliderOffset = capsuleCollider2D.offset;
		StandardColliderSize = capsuleCollider2D.size;

		OnWatingAnim();
	}

	void Update()
	{
		// 死んでたら帰る.
		if ((currentFlag & FlagTypes.death) == FlagTypes.death)
		{
			// 死亡演出へ.
			StartCoroutine(PlayerDeathAction());
			SetSpineAnimation();

			return;
		}

		transPos = transform.position;
		SetDirectionalInput();

		#region 足元に地面があるか判定.
		if (rigidbody2D.velocity.y <= 0)
		{
			groundedFlag = Physics2D.OverlapCircle(transPos - transform.up * 0.6f, 0.2f, groundLayer);
		}
		#endregion

		#region 障害物があるか判定.
			obstacleFlag = Physics2D.Linecast(transPos - transform.right * 1.5f,
												transPos + transform.right * 1.5f,
												obstacleLayer);
			#region Debug
			Debug.DrawLine(transPos - transform.right * 1.5f,
							transPos + transform.right * 1.5f,
							Color.red);
			#endregion
		#endregion

		// 進行方向に障害物があればダッシュ(iTween)を停止.
		if (obstacleFlag)
		{
			iTween.Stop(gameObject);
		}

		LifeChecker();
		StateAbnormalChecker();

		if (groundedFlag)
		{
			spineAnimName &= ~SpineAnimationNames.descend;
			jumpCount = 0;

			if ((spineAnimName & SpineAnimationNames.nearAttack) == SpineAnimationNames.nearAttack)
			{
				rigidbody2D.velocity = Vector2.zero;
			}

			if (attackNum == 0)
			{
				attackNum = 1;
			}
		}

		if ((currentFlag & FlagTypes.stan) != FlagTypes.stan &&
			(currentFlag & FlagTypes.sex) != FlagTypes.sex)
		{
			DashMovement();
			CalculateVelocity();
			OnJumpInputDown();
			WaitAttackDelayTime();
		}

		StanChecker();
		SetSpineAnimation();
		PadVibration();

		attackEnemyName = string.Empty;
	}

	private void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere(transPos - transform.up * 0.6f, 0.2f);
	}

	#region 入力管理.
	/// <summary>
	/// 入力保管.
	/// </summary>
	public void SetDirectionalInput()
	{
		directionalInput = inputManager.DirectionalInput;
		stanGaugeControl.IsDirectionalInput = inputManager.DirectionalInput;
		escapeGaugeControl.IsDirectionalInput = inputManager.DirectionalInput;

		if ((inputManager.InputFlag & InputManager.InputField.crouchAttack) == InputManager.InputField.crouchAttack)
		{
			OnCrouchAttack();
		}

		if ((inputManager.InputFlag & InputManager.InputField.nearAttack) == InputManager.InputField.nearAttack)
		{
			OnNearAttack();
		}

		if ((inputManager.InputFlag & InputManager.InputField.lifeHeel) == InputManager.InputField.lifeHeel)
		{
			playerLife.LifeUp();
		}

		if ((inputManager.InputFlag & InputManager.InputField.apHeel) == InputManager.InputField.apHeel)
		{
			playerLife.ArmorPointUp();
		}
	}
	#endregion

	#region 移動系.
	/// <summary>
	/// ジャンプ処理.
	/// </summary>
	public void OnJumpInputDown()
	{
		if ((inputManager.InputFlag & InputManager.InputField.jump) == InputManager.InputField.jump)
		{
			if (jumpCount < 2)
			{
				// 着地判定をfalseに.
				groundedFlag = false;
				// AddForceで上方向へ力を加える.
				rigidbody2D.velocity = Vector2.zero;
				rigidbody2D.AddForce(Vector2.up * (jumpPower * 100.0f));

				if (jumpCount >= 1)
				{
					effect = Instantiate(airJumpEffect, transPos, transform.rotation) as GameObject;
					//effect.transform.SetParent(transform);
					effect.transform.position = new Vector3(transPos.x, transPos.y - 1f, -5f);

					Destroy(effect, 0.5f);
				}
				else
				{
					effect = Instantiate(jumpEffect, transPos, transform.rotation) as GameObject;
					//effect.transform.SetParent(transform);
					effect.transform.position = new Vector3(transPos.x, transPos.y - 1f, -5f);

					Destroy(effect, 0.5f);
				}

				jumpCount++;
			}

			// ジャンプ.
			if ((spineAnimName & SpineAnimationNames.dash) != SpineAnimationNames.dash)
			{
				spineAnimName = SpineAnimationNames.jump;
			}
		}
	}

	/// <summary>
	/// 移動処理.
	/// </summary>
	void CalculateVelocity()
	{
		if ((currentFlag & FlagTypes.nearAttack) != FlagTypes.nearAttack &&
			(currentFlag & FlagTypes.farAttack) != FlagTypes.farAttack &&
			(currentFlag & FlagTypes.knockBack) != FlagTypes.knockBack)
		{
			float targetVelocityX = 0f;

			// しゃがんでいなければ走る.
			if ((inputManager.InputFlag & InputManager.InputField.crouch) != InputManager.InputField.crouch)
			{
				targetVelocityX = directionalInput.x * runMoveSpeed;
				//targetVelocityX = directionalInput.x * walkMoveSpeed;
			}

			capsuleCollider2D.offset = StandardColliderOffset;
			capsuleCollider2D.size = StandardColliderSize;

			playerWeapon.ResetColliderOffset();

			#region 入力に応じてキャラの向きとモーションを変更.
			if (directionalInput.y == 1)
			{
				// 処理なし.
			}
			// しゃがみ.
			else if (directionalInput.y == -1)
			{
				capsuleCollider2D.offset = new Vector2(0.0f, 0.5f);
				capsuleCollider2D.size = new Vector2(1.0f, 2.6f);

				// しゃがみ.
				if (groundedFlag)
				{
					spineAnimName = SpineAnimationNames.crouch;
				}

				if (directionalInput.x == 1)
				{
					flipX = false;
					playerWeapon.ReverseCollider(flipX);
				}
				else if (directionalInput.x == -1)
				{
					flipX = true;
					playerWeapon.ReverseCollider(flipX);
				}
			}
			// 右移動.
			else if (directionalInput.x == 1)
			{
				flipX = false;
				playerWeapon.ReverseCollider(flipX);

				// 走る.
				if (groundedFlag &&
					(spineAnimName & SpineAnimationNames.dash) != SpineAnimationNames.dash)
				{
					spineAnimName = SpineAnimationNames.run;
				}
			}
			// 左移動.
			else if (directionalInput.x == -1)
			{
				flipX = true;
				playerWeapon.ReverseCollider(flipX);

				// 走る.
				if (groundedFlag &&
					(spineAnimName & SpineAnimationNames.dash) != SpineAnimationNames.dash)

				{
					spineAnimName = SpineAnimationNames.run;
				}
			}
			#endregion

			if (directionalInput != Vector2.zero)
			{
				rigidbody2D.velocity = new Vector2(targetVelocityX, rigidbody2D.velocity.y);
			}
			else if (directionalInput == Vector2.zero &&
					(currentFlag & FlagTypes.knockBack) != FlagTypes.knockBack)
			{

				rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);
			}

			skeletonAnimation.Skeleton.FlipX = flipX;
		}
	}

	/// <summary>
	/// ダッシュ移動の制御.
	/// </summary>
	protected void DashMovement()
	{
		// ダッシュとノックバックをしていなければ.
		if (!obstacleFlag && !steppingFlag && (currentFlag & FlagTypes.knockBack) != FlagTypes.knockBack)
		{
			// ダッシュキーが押された時にスタミナが残っていれば.
			if ((inputManager.InputFlag & InputManager.InputField.dash) == InputManager.InputField.dash &&
				playerStamina.DashMove())
			{
				effect = null;

				ChangeLayer(Constants.LAYER_NAME_DASH);
				tmpName = spineAnimName;
				spineAnimName = SpineAnimationNames.dash;
				skeletonGhost.ghostingEnabled = true;

				// ダッシュ移動開始.
				steppingFlag = true;

				//if (directionalInput.x != 0)
				//{
				//	movingDistance = 3f;
				//}
				// iTweenで瞬間移動.
				if (!flipX)
				{
					iTween.MoveAdd(gameObject, iTween.Hash("x", movingDistance,
														"time", 0.8f));

					effect = Instantiate(dashEffect, transPos, transform.rotation) as GameObject;
					effect.transform.position = new Vector3(transPos.x, transPos.y, -5f);

					Destroy(effect, 1f);
				}
				else
				{
					iTween.MoveAdd(gameObject, iTween.Hash("x", -movingDistance,
														"time", 0.8f));

					effect = Instantiate(dashEffect, transPos, new Quaternion(0, 180, 0, 1)) as GameObject;
					effect.transform.position = new Vector3(transPos.x, transPos.y, -5f);

					Destroy(effect, 1f);
				}
			}
		}

		// ダッシュキーを押してからの時間計測.
		if (steppingFlag)
		{
			// 時間計測.
			dashTime += Time.deltaTime;

			if (dashTime > 1.0f)
			{
				dashTime = 0;
				steppingFlag = false;
			}
			else if (dashTime > 0.5f)
			{
				skeletonGhost.ghostingEnabled = false;
				ChangeLayer(Constants.LAYER_NAME_PLAYER);
			}
			else if (dashTime > 0.3f)
			{
				//spineAnimName &= ~SpineAnimationNames.dash;
				spineAnimName = tmpName;
			}
			else
			{
				rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0);
			}
		}
	}
	#endregion 移動系.

	#region 攻撃系.
	/// <summary>
	/// 近接攻撃.
	/// </summary>
	public void OnNearAttack()
	{
		if ((currentFlag & FlagTypes.damage) != FlagTypes.damage &&
			(currentFlag & FlagTypes.knockBack) != FlagTypes.knockBack &&
			(currentFlag & FlagTypes.farAttack) != FlagTypes.farAttack &&
			(currentFlag & FlagTypes.stan) != FlagTypes.stan &&
			(currentFlag & FlagTypes.sex) != FlagTypes.sex)
		{
			spineAnimName = SpineAnimationNames.nearAttack;
			currentFlag |= FlagTypes.nearAttack;

			if (groundedFlag)
			{
				rigidbody2D.velocity = Vector2.zero;
				OnNearAttackAnim();
			}
			else
			{
				rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);
				OnJumpAttackAnim();
			}
			inputAttackKeyFlag = true;
		}
	}

	/// <summary>
	/// しゃがみ攻撃.
	/// </summary>
	public void OnCrouchAttack()
	{
		if ((currentFlag & FlagTypes.damage) != FlagTypes.damage &&
			(currentFlag & FlagTypes.stan) != FlagTypes.stan &&
			(currentFlag & FlagTypes.sex) != FlagTypes.sex)
		{
			rigidbody2D.velocity = Vector2.zero;
			spineAnimName = SpineAnimationNames.crouchAttack;
			currentFlag |= FlagTypes.nearAttack;
			OnCrouchAttackAnim();
		}
	}

	/// <summary>
	/// 遠距離攻撃.
	/// </summary>
	public void OnFarAttack()
	{
		if (groundedFlag &&
			(currentFlag & FlagTypes.damage) != FlagTypes.damage &&
			(currentFlag & FlagTypes.nearAttack) != FlagTypes.nearAttack &&
			(currentFlag & FlagTypes.stan) != FlagTypes.stan &&
			(currentFlag & FlagTypes.sex) != FlagTypes.sex)
		{
			rigidbody2D.velocity = Vector2.zero;
			spineAnimName = SpineAnimationNames.farAttack;
			currentFlag |= FlagTypes.farAttack;
			OnFarAttackAnim();
		}
	}

	/// <summary>
	/// 必殺技.
	/// </summary>
	public void OnSpecialSkill()
	{
		//if ((currentFlag & FlagTypes.damage) != FlagTypes.damage &&
		//	skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != farAttackAnim)
		//{
		//	OnSpecialSkillAnim();
		//	currentFlag |= FlagTypes.attack;
		//}
	}

	/// <summary>
	/// 攻撃後の連続攻撃までの時間計測.
	/// </summary>
	void WaitAttackDelayTime()
	{
		// キーを押してからの時間計測.
		if (inputAttackKeyFlag)
		{
			// 時間計測.
			attackTime += Time.deltaTime;

			if (attackTime > 0.5f)
			{
				attackNum = 1;
				attackTime = 0;
				inputAttackKeyFlag = false;

				ResetAttackFlage();
			}
			else if (attackTime > 0.3f)
			{
				//ResetAttackFlage();
				playerWeapon.SetActiveCollider(false);
			}
		}
	}

	/// <summary>
	/// 攻撃フラグを折る.
	/// </summary>
	public void ResetAttackFlage()
	{
		currentFlag &= ~FlagTypes.nearAttack;
		currentFlag &= ~FlagTypes.farAttack;

		playerWeapon.SetActiveCollider(false);
	}
	#endregion 攻撃系.

	#region ダメージ系.
	/// <summary>
	/// 敵から攻撃を食らった.
	/// </summary>
	public void Damage(int enemyAttackPoint, bool flip)
	{
		if ((currentFlag & FlagTypes.sex) == FlagTypes.sex)
		{
			return;
		}

		if ((currentFlag & FlagTypes.invincible) != FlagTypes.invincible &&
			(currentFlag & FlagTypes.stan) != FlagTypes.stan)
		{
			ResetAttackFlage();
			DamageAction(enemyAttackPoint, flip);
		}
		else if ((currentFlag & FlagTypes.invincible) != FlagTypes.invincible &&
				(currentFlag & FlagTypes.stan) == FlagTypes.stan &&
				(currentFlag & FlagTypes.sex) != FlagTypes.sex)
		{
			StanDamage();
			HAnimObjectCreater(flip);
		}
	}

	/// <summary>
	/// スタン時に攻撃されたら.
	/// </summary>
	/// <param name="obj"></param>
	public void StanDamage()
	{
		// 状態からスタンを削除.
		currentFlag &= ~FlagTypes.stan;
		// 状態にHモーションを追加.
		currentFlag |= FlagTypes.sex;

		// キャラを透明に.
		skeletonAnimation.skeleton.SetColor(new Color(0, 0, 0, 0));

		// スタン耐性値をリセット.
		playerLife.ResetStanPoint();

		// 入力回数をリセット.
		escapeGaugeControl.ResetInputTimes();
		escapeGaugeControl.AdjustmentPosition(attackEnemyName);

		escapeGauge.SetActive(true);
	}

	/// <summary>
	/// ダメージアニメーション.
	/// </summary>
	void DamageAction(int enemyAttackPoint, bool flip)
	{
		// 状態にダメージHitを追加.
		currentFlag |= FlagTypes.damage;
		StartCoroutine(WaitInvincibleCoroutine());

		// SE再生.
		playerSE.DamageSE();

		// PlayerのLifeを減らす.
		playerLife.LifeDown(enemyAttackPoint);

		// Lifeが０以下なら死亡演出へ.
		// ０超過ならノックバック.
		if (playerLife.IsNowLife <= 0)
		{
			// 死亡演出へ.
			StartCoroutine(PlayerDeathAction());
		}
		else
		{
			// ノックバック.
			StartCoroutine(KnockBack(flip));
		}
		// 状態からダメージHitを削除.
		currentFlag &= ~FlagTypes.damage;
	}

	/// <summary>
	/// 殺られ演出.
	/// </summary>
	/// <returns></returns>
	IEnumerator PlayerDeathAction()
	{
		if ((currentFlag & FlagTypes.sex) == FlagTypes.sex)
		{
			yield break;
		}

		// 状態に死を追加.
		currentFlag |= FlagTypes.death;
		StartCoroutine(WaitInvincibleCoroutine());

		// タグとの変更.
		ChangeTag(Constants.LAYER_NAME_DEATH);
		ChangeLayer(Constants.LAYER_NAME_DEATH);

		rigidbody2D.velocity = Vector2.zero;
		directionalInput = Vector2.zero;

		// モーションの再生(これスタンな).
		spineAnimName = SpineAnimationNames.stan;

		yield return null;
	}

	/// <summary>
	/// ダメージ演出.
	/// </summary>
	/// <returns></returns>
	IEnumerator PlayerDamageAction()
	{
		// 状態にダメージHitを追加.
		currentFlag |= FlagTypes.damage;
		StartCoroutine(WaitInvincibleCoroutine());

		// コントローラの振動時間をリセット.
		vibrationTime = 0f;

		// ダメージモーションへ.
		spineAnimName = SpineAnimationNames.damage;

		// 状態からダメージHitを削除.
		currentFlag &= ~FlagTypes.damage;

		// 指定秒待つ.
		yield return null;
	}

	/// <summary>
	/// ノックバック処理.
	/// </summary>
	/// <param name="flip"></param>
	/// <returns></returns>
	IEnumerator KnockBack(bool flip)
	{
		if ((currentFlag & FlagTypes.knockBack) == FlagTypes.knockBack)
		{
			yield break;
		}

		// 状態にノックバックを追加.
		currentFlag |= FlagTypes.knockBack;

		// 移動入力はいったん初期化(入力があると正しくノックバックしないため).
		directionalInput = Vector2.zero;

		// 進行方向に障害物があればダッシュ(iTween)を停止.
		if (obstacleFlag)
		{
			iTween.Stop(gameObject);
		}
		else
		{
			// 向きに合わせてノックバックさせる.
			if (flip)
			{
				// ノックバック.
				iTween.MoveAdd(gameObject, iTween.Hash(
					"x", -blowOffPower.x,
					"time", 1f,
					"oncomplete", "WaitInvincibleCoroutine"
				));
			}
			else
			{
				// ノックバック.
				iTween.MoveAdd(gameObject, iTween.Hash(
					"x", blowOffPower.x,
					"time", 1f,
					"oncomplete", "WaitInvincibleCoroutine"
				));
			}
		}

		// キャラの向きと攻撃したキャラの向きが同じなら反転.
		if (flipX == flip)
		{
			flipX = skeletonAnimation.Skeleton.FlipX = !flipX;
		}

		// ダメージ演出へ.
		StartCoroutine(PlayerDamageAction());

		yield return new WaitForSeconds(0.3f);
		currentFlag &= ~FlagTypes.knockBack;

		StartCoroutine(WaitInvincibleCoroutine());
	}

	/// <summary>
	/// Hアニメオブジェクトの生成.
	/// </summary>
	void HAnimObjectCreater(bool flip)
	{
		Constants.ENEMY_NAME eName = Constants.ENEMY_NAME.gobline;

		if (attackEnemyName == "Goblin")
		{
			eName = Constants.ENEMY_NAME.gobline;
		}
		else if (attackEnemyName == "Orc")
		{
			eName = Constants.ENEMY_NAME.orc;
		}
		else if (attackEnemyName == "BossOrc")
		{
			eName = Constants.ENEMY_NAME.bossOrc;
		}

		hAnimPrefabController.CreateObject(eName, transPos, flip);
	}

	/// <summary>
	/// Hアニメ中のダメージ.
	/// </summary>
	/// <returns></returns>
	void HAnimDamage()
	{
		if (!playerLife.HAnimLifeDown(5))
		{
			if (escapeGauge.activeSelf)
			{
				escapeGauge.SetActive(false);
				hAnimPrefabController.AnimationLoopStop();
			}
		}
		else
		{
			hAnimDamageTimeCount = 0;
		}
	}
	#endregion ダメージ系.

	#region 状態異常.
	/// <summary>
	/// 状態異常の判定.
	/// </summary>
	void StateAbnormalChecker()
	{
		if (playerLife.IsStanPoint <= 0 &&
			(currentFlag & FlagTypes.stan) != FlagTypes.stan)
		{
			// 状態にスタンを追加.
			currentFlag |= FlagTypes.stan;

			// 入力回数をリセット.
			stanGaugeControl.ResetInputTimes();

			stanGauge.SetActive(true);
		}
		else if ((currentFlag & FlagTypes.sex) != FlagTypes.sex)
		{
			// モーションオブジェクトを削除.
			hAnimPrefabController.DeleteObject();

			if ((currentFlag & FlagTypes.invincible) != FlagTypes.invincible)
			{
				// キャラの色を戻す.
				skeletonAnimation.skeleton.SetColor(new Color(1, 1, 1, 1));
			}
			else
			{
				// 点滅
				float level = Mathf.Abs(Mathf.Sin(Time.time * flashingSpeed));
				skeletonAnimation.skeleton.SetColor(new Color(0.8f, 0.8f, 0.8f, level));
			}
		}
		else if ((currentFlag & FlagTypes.sex) == FlagTypes.sex)
		{
			hAnimDamageTimeCount += Time.deltaTime;

			if (hAnimDamageTimeCount > hAnimDamageTime)
			{
				HAnimDamage();
			}
		}
	}

	/// <summary>
	/// 状態サーチ.
	/// </summary>
	void StanChecker()
	{
		if ((currentFlag & FlagTypes.stan) != FlagTypes.stan &&
			(currentFlag & FlagTypes.sex) != FlagTypes.sex)
		{
			rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
			return;
		}

		if (((spineAnimName & SpineAnimationNames.stan) == SpineAnimationNames.stan &&
			((currentFlag & FlagTypes.stan) == FlagTypes.stan ||
			(currentFlag & FlagTypes.sex) == FlagTypes.sex)) ||
			stanGauge.activeSelf)
		{
			rigidbody2D.constraints = RigidbodyConstraints2D.FreezePositionX |
										RigidbodyConstraints2D.FreezeRotation;
		}

		// モーションの再生.
		spineAnimName = SpineAnimationNames.stan;
	}

	/// <summary>
	/// 強制スタン.
	/// </summary>
	public void ForciblyStan()
	{
		if ((currentFlag & FlagTypes.stan) == FlagTypes.stan)
		{
			hAnimPrefabController.CreateObject(Constants.ENEMY_NAME.bossOrc2, transPos, flipX);

			StanDamage();
			return;
		}

		// ノックバック.
		StartCoroutine(KnockBack(!flipX));

		// AP・SPを0に.
		playerLife.ArmorStanPointClear();

		// 状態にスタンを追加.
		currentFlag |= FlagTypes.stan;

		// 入力回数をリセット.
		stanGaugeControl.ResetInputTimes();

		stanGauge.SetActive(true);
	}
	#endregion

	#region フラグ解除.
	/// <summary>
	/// フラグ解除.
	/// </summary>
	public void ChangeStanFlag()
	{
		// 状態からスタンを削除.
		currentFlag &= ~FlagTypes.stan;
		spineAnimName &= ~SpineAnimationNames.stan;

		StartCoroutine(WaitInvincibleCoroutine());
	}

	/// <summary>
	/// フラグ解除.
	/// </summary>
	public void ChangeSexFlag()
	{
		// 状態からHモーションを削除.
		currentFlag &= ~FlagTypes.sex;
	}
	#endregion

	#region コントローラ.
	/// <summary>
	/// コントローラの振動.
	/// </summary>
	void PadVibration()
	{
		if (vibrationTime < 0.3f)
		{
			// SetVibrationは、より遅い速度で送信する必要があります.
			// トリガーに従って振動を設定する.
			GamePad.SetVibration(PlayerIndex.One, 0.7f, 0.7f);

			vibrationTime += Time.deltaTime;
		}
		else
		{
			GamePad.SetVibration(PlayerIndex.One, 0f, 0f);
		}
	}
	#endregion

	#region Spineスキン.
	/// <summary>
	/// Lifeに合わせてスキンを変更する.
	/// </summary>
	void LifeChecker()
	{
		// Lifeに変化があったら動く.
		if (storingLife != playerLife.IsNowLife)
		{
			storingLife = playerLife.IsNowLife;
			if (storingLife <= 0)
			{
				StartCoroutine(PlayerDeathAction());
			}
		}

		if (playerLife.IsArmorPoint > 0)
		{
			ChangeSkine("03_side_normal");
		}
		else
		{
			ChangeSkine("04_side_break");
		}
	}

	/// <summary>
	/// スキンの変更.
	/// </summary>
	/// <param name="changeSkineName"></param>
	void ChangeSkine(string changeSkineName)
	{
		if (skeletonAnimation.skeleton.Skin.name != changeSkineName)
		{
			skeletonAnimation.skeleton.SetSkin(changeSkineName);
			skeletonAnimation.skeleton.SetSlotsToSetupPose();

			spineAnimName = SpineAnimationNames.stay;
		}
	}
	#endregion

	#region SpineAnimation
	/// <summary>
	/// Spineモーション制御.
	/// </summary>
	void SetSpineAnimation()
	{
		switch (spineAnimName)
		{
			case SpineAnimationNames.stay:
				OnWatingAnim();
				break;
			case SpineAnimationNames.run:
				OnRunAnim();
				break;
			case SpineAnimationNames.dash:
				OnDashAnim();
				break;
			case SpineAnimationNames.crouch:
				OnCrouchAnim();
				break;
			case SpineAnimationNames.jump:
				OnJumpAnim();
				break;
			case SpineAnimationNames.descend:
				OnDescendAnim();
				break;
			case SpineAnimationNames.nearAttack:
				//OnNearAttackAnim();
				break;
			case SpineAnimationNames.crouchAttack:
				//OnCrouchAttackAnim();
				break;
			case SpineAnimationNames.farAttack:
				//OnFarAttackAnim();
				break;
			case SpineAnimationNames.damage:
				OnDamageAnim();
				break;
			case SpineAnimationNames.stan:
				OnDownAnim();
				break;
			default:
				spineAnimName = 0;
				break;
		}
		// 待機.
		if (directionalInput == Vector2.zero && groundedFlag && !steppingFlag &&
			((inputManager.InputFlag & InputManager.InputField.jump) != InputManager.InputField.jump &&
			(spineAnimName & SpineAnimationNames.jump) != SpineAnimationNames.jump) &&
			(currentFlag & FlagTypes.nearAttack) != FlagTypes.nearAttack &&
			(currentFlag & FlagTypes.farAttack) != FlagTypes.farAttack &&
			(currentFlag & FlagTypes.knockBack) != FlagTypes.knockBack)
		{
			spineAnimName = SpineAnimationNames.stay;
		}

		// 降下.
		if (rigidbody2D.velocity.y < 0 &&
			(spineAnimName & SpineAnimationNames.dash) != SpineAnimationNames.dash &&
			(currentFlag & FlagTypes.knockBack) != FlagTypes.knockBack &&
			(currentFlag & FlagTypes.nearAttack) != FlagTypes.nearAttack)
		{
			spineAnimName = SpineAnimationNames.descend;
		}
	}

	#region Animation
	/// <summary>
	/// 近接攻撃モーション.
	/// </summary>
	void OnNearAttackAnim()
	{
		switch (attackNum)
		{
			case 1:
				if (playerLife.IsArmorPoint > 0)
				{
					if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != nearAttackAnim_1 &&
						attackTime == 0f)
					{
						playerWeapon.SetActiveCollider(true);
						skeletonAnimation.AnimationState.SetAnimation(0, nearAttackAnim_1, false);

						// SE再生.
						playerSE.AttackSE_01();
						attackNum++;
						attackTime = 0f;
					}
				}
				else
				{
					if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != nearAttackBreakAnim_1 &&
						attackTime == 0f)
					{
						playerWeapon.SetActiveCollider(true);
						skeletonAnimation.AnimationState.SetAnimation(0, nearAttackBreakAnim_1, false);

						// SE再生.
						playerSE.AttackSE_01();
						attackNum++;
						attackTime = 0f;
					}
				}
				break;

			case 2:
				if (playerLife.IsArmorPoint > 0)
				{
					if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != nearAttackAnim_2)
					{
						playerWeapon.SetActiveCollider(true);
						skeletonAnimation.AnimationState.SetAnimation(0, nearAttackAnim_2, false);

						// SE再生.
						playerSE.AttackSE_02();
						attackNum++;
						//Debug.Log("attackTime2:" + attackTime);
						attackTime = 0f;
					}
				}
				else
				{
					if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != nearAttackBreakAnim_2)
					{
						playerWeapon.SetActiveCollider(true);
						skeletonAnimation.AnimationState.SetAnimation(0, nearAttackBreakAnim_2, false);

						// SE再生.
						playerSE.AttackSE_02();
						attackNum++;
						//Debug.Log("attackTime2:" + attackTime);
						attackTime = 0f;
					}
				}
				break;

			case 3:
				if (playerLife.IsArmorPoint > 0)
				{
					if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != nearAttackAnim_3)
					{
						playerWeapon.SetActiveCollider(true);
						skeletonAnimation.AnimationState.SetAnimation(0, nearAttackAnim_3, false);

						// SE再生.
						playerSE.AttackSE_03();
						attackNum++;
						//Debug.Log("attackTime3:" + attackTime);
						attackTime = 0f;
					}
				}
				else
				{
					if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != nearAttackBreakAnim_3)
					{
						playerWeapon.SetActiveCollider(true);
						skeletonAnimation.AnimationState.SetAnimation(0, nearAttackBreakAnim_3, false);

						// SE再生.
						playerSE.AttackSE_03();
						attackNum++;
						//Debug.Log("attackTime3:" + attackTime);
						attackTime = 0f;
					}
				}

				//skeletonAnimation.AnimationState.Complete += PlayWatingAnimation;
				break;

			default:
				attackNum = 1;
				break;
		}

		if (!groundedFlag)
		{
			attackNum = 0;
		}
		spineAnimName &= ~SpineAnimationNames.nearAttack;
	}

	/// <summary>
	/// 空中(ジャンプ)攻撃モーション.
	/// </summary>
	void OnJumpAttackAnim()
	{
		if (playerLife.IsArmorPoint > 0)
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != jumpAttackAnim)
			{
				playerWeapon.SetActiveCollider(true);
				skeletonAnimation.AnimationState.SetAnimation(0, jumpAttackAnim, false);
				// SE再生.
				playerSE.AttackSE_01();
				attackTime = 0f;
			}
		}
		else
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != jumpAttackBreakAnim)
			{
				playerWeapon.SetActiveCollider(true);
				skeletonAnimation.AnimationState.SetAnimation(0, jumpAttackBreakAnim, false);
				// SE再生.
				playerSE.AttackSE_01();
				attackTime = 0f;
			}
		}
	}

	/// <summary>
	/// 遠距離攻撃モーション.
	/// </summary>
	void OnFarAttackAnim()
	{
		if (playerLife.IsArmorPoint > 0)
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != farAttackAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, farAttackAnim, false);
				// SE再生.
				playerSE.KnifeSound();

				if (knifePossession < knifeMax)
				{
					StartCoroutine(KnifeThrow());
				}
			}
		}
		else
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != farAttackBreakAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, farAttackBreakAnim, false);
				// SE再生.
				playerSE.KnifeSound();

				if (knifePossession < knifeMax)
				{
					StartCoroutine(KnifeThrow());
				}
			}
		}
		skeletonAnimation.AnimationState.Complete += PlayWatingAnimation;
		spineAnimName &= ~SpineAnimationNames.farAttack;
	}

	/// <summary>
	/// 必殺技モーション.
	/// </summary>
	void OnSpecialSkillAnim()
	{
		//skeletonAnimation.AnimationState.SetAnimation(0, SpecialAttackAnim, false);
		//skeletonAnimation.AnimationState.Complete += PlayWatingAnimation;
	}

	/// <summary>
	/// しゃがみ攻撃モーション.
	/// </summary>
	void OnCrouchAttackAnim()
	{
		if (playerLife.IsArmorPoint > 0)
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != crouchAttackAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, crouchAttackAnim, false);

				// SE再生.
				playerSE.AttackSE_01();
			}
		}
		else
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != crouchAttackBreakAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, crouchAttackBreakAnim, false);

				// SE再生.
				playerSE.AttackSE_01();
			}
		}

		playerWeapon.SetActiveCollider(true);
		spineAnimName &= ~SpineAnimationNames.crouchAttack;
		skeletonAnimation.AnimationState.Complete += PlayWatingAnimation;
	}

	/// <summary>
	/// しゃがむモーション.
	/// </summary>
	void OnCrouchAnim()
	{
		if (playerLife.IsArmorPoint > 0)
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != crouchAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, crouchAnim, true);
			}
		}
		else
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != crouchBreakAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, crouchBreakAnim, true);
			}
		}

		spineAnimName &= ~SpineAnimationNames.crouch;
	}

	/// <summary>
	/// ノックバックモーション.
	/// </summary>
	void OnDamageAnim()
	{
		if (playerLife.IsArmorPoint > 0)
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != damageAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, damageAnim, false);
			}
		}
		else
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != damageBreakAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, damageBreakAnim, false);
			}
		}

		spineAnimName &= ~SpineAnimationNames.damage;
	}

	/// <summary>
	/// やられモーション.
	/// </summary>
	void OnDownAnim()
	{
		if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != downBreakAnim)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, downBreakAnim, true);
		}

		spineAnimName &= ~SpineAnimationNames.stan;
	}

	/// <summary>
	/// 振り向きモーション.
	/// </summary>
	void OnHeadturnAnim()
	{
		//skeletonAnimation.AnimationState.SetAnimation(0, headturnAnim, false);
		//skeletonAnimation.AnimationState.Complete += PlayWatingAnimation;
	}

	/// <summary>
	/// 待機モーション.
	/// </summary>
	void OnWatingAnim()
	{
		if (playerLife.IsArmorPoint > 0)
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != waitingAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, waitingAnim, true);
			}
		}
		else
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != waitingBreakAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, waitingBreakAnim, true);
			}
		}

		spineAnimName &= ~SpineAnimationNames.stay;
	}

	/// <summary>
	/// ジャンプモーション.
	/// </summary>
	void OnJumpAnim()
	{
		if (playerLife.IsArmorPoint > 0)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, jumpAnim, false);
		}
		else
		{
			skeletonAnimation.AnimationState.SetAnimation(0, jumpBreakAnim, false);
		}

		spineAnimName &= ~SpineAnimationNames.jump;
	}

	/// <summary>
	/// 走るモーション.
	/// </summary>
	void OnRunAnim()
	{
		if (playerLife.IsArmorPoint > 0)
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != runAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, runAnim, true);
			}
		}
		else
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != runBreakAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, runBreakAnim, true);
			}
		}

		spineAnimName &= ~SpineAnimationNames.run;
	}

	/// <summary>
	/// ダッシュモーション.
	/// </summary>
	void OnDashAnim()
	{
		if (playerLife.IsArmorPoint > 0)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, dashAnim, false);
		}
		else
		{
			skeletonAnimation.AnimationState.SetAnimation(0, dashBreakAnim, false);
		}
	}

	/// <summary>
	/// 歩くモーション.
	/// </summary>
	void OnWalkAnim()
	{
		//skeletonAnimation.AnimationState.SetAnimation(0, clothesBreakWalkAnim, true);
	}

	/// <summary>
	/// 恥じらい歩きモーション.
	/// </summary>
	void OnAshamedWalkAnim()
	{
		//skeletonAnimation.AnimationState.SetAnimation(0, clothesBreakAshamedWalkAnim, true);
	}

	/// <summary>
	/// 降りるモーション.
	/// </summary>
	void OnDescendAnim()
	{
		if (playerLife.IsArmorPoint > 0)
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).animation.name != descendAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, descendAnim, false);
			}
		}
		else
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).animation.name != descendBreakAnim)
			{
				skeletonAnimation.AnimationState.SetAnimation(0, descendBreakAnim, false);
			}
		}

		spineAnimName &= ~SpineAnimationNames.descend;
	}

	/// <summary>
	/// 待機モーション【攻撃モーション終了後に待機モーションを再生】.
	/// </summary>
	/// <param name="trackEntry"></param>
	void PlayWatingAnimation(TrackEntry trackEntry)
	{
		if ((trackEntry.animation.Name == nearAttackAnim_3 || trackEntry.animation.Name == nearAttackBreakAnim_3) ||
			(trackEntry.animation.Name == farAttackAnim || trackEntry.animation.name == farAttackBreakAnim) /*||
			trackEntry.animation.Name == SpecialAttackAnim ||
			trackEntry.animation.Name == headturnAnim*/)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, waitingAnim, true);
			skeletonAnimation.AnimationState.Complete -= PlayWatingAnimation;
		}
		else if (trackEntry.animation.Name == crouchAttackAnim)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, crouchAnim, true);
			skeletonAnimation.AnimationState.Complete -= PlayWatingAnimation;
		}
		else if (trackEntry.animation.Name == crouchAttackBreakAnim)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, crouchBreakAnim, true);
			skeletonAnimation.AnimationState.Complete -= PlayWatingAnimation;
		}
		ResetAttackFlage();
	}

	// スタンモーション.
	void PlayDownAnimation(TrackEntry trackEntry)
	{
		//if (trackEntry.animation.Name == downInAnim)
		//{
		//	skeletonAnimation.AnimationState.SetAnimation(0, downAnim, true);
		//	skeletonAnimation.AnimationState.Complete -= PlayDownAnimation;
		//}
	}
	#endregion
	#endregion

	#region タグ変更.
	/// <summary>
	/// タグの変更.
	/// </summary>
	void ChangeTag(string name)
	{
		gameObject.tag = name;
	}

	/// <summary>
	/// レイヤーの変更.
	/// </summary>
	void ChangeLayer(string name)
	{
		gameObject.layer = LayerMask.NameToLayer(name);
	}
	#endregion

	#region リスポーン関係.
	/// <summary>
	/// コンテニュー.
	/// </summary>
	public void InitToContinue(Vector3 pos)
	{
		StartCoroutine(ReturnCharacter(pos));
		currentFlag = 0;
		ChangeTag(Constants.LAYER_NAME_PLAYER);
		ChangeLayer(Constants.LAYER_NAME_PLAYER);
	}

	/// <summary>
	/// リスポーンコルーチン.
	/// </summary>
	/// <param name="pos"></param>
	/// <returns></returns>
	IEnumerator ReturnCharacter(Vector3 pos)
	{
		FadeManager.Instance.FadeInOut(true, 0.5f);

		// 重力系を初期化.
		rigidbody2D.velocity = Vector2.zero;
		// 残像を非表示に.
		skeletonGhost.ghostingEnabled = false;
		// キャラの色をリセット.
		skeletonAnimation.skeleton.SetColor(new Color(1, 1, 1, 1));
		// Lifeをリセット.
		playerLife.ResetLife();
		// モーションを待機へ変更.
		spineAnimName = SpineAnimationNames.stay;
		// 向きをリセット.
		flipX = false;
		// 位置をリスポーン位置へ変更.
		transform.position = pos;

		currentFlag &= ~FlagTypes.damage;
		currentFlag &= ~FlagTypes.invincible;

		FadeManager.Instance.FadeInOut(false, 0.5f);
		yield return null;
	}
	#endregion

	/// <summary>
	/// 攻撃してきたキャラの名前を保管.
	/// </summary>
	/// <param name="attachmentName"></param>
	void HitAttackName(string attachmentName)
	{
		attackEnemyName = attachmentName;
	}

	/// <summary>
	/// 飛び道具(ナイフ)生成.
	/// </summary>
	/// <returns></returns>
	IEnumerator KnifeThrow()
	{
		// ナイフ位置と角度を調整して生成.
		if (flipX)
		{
			GameObject obj = Instantiate(knife, new Vector3(transPos.x, transPos.y + 2.0f, 0f), new Quaternion(0, 180, 0, 0));
		}
		else
		{
			GameObject obj = Instantiate(knife, new Vector3(transPos.x, transPos.y + 2.0f, 0f), new Quaternion(0, 0, 0, 0));
		}

		yield return null;
	}

	/// <summary>
	/// 無敵時間.
	/// </summary>
	/// <returns></returns>
	IEnumerator WaitInvincibleCoroutine()
	{
		currentFlag |= FlagTypes.invincible;
		yield return new WaitForSeconds(invincibleTimer);
		currentFlag &= ~FlagTypes.invincible;
	}

	//--------------------------------------------------------
	// 物理的にぶつからない衝突判定＝＞　Trigger系.
	// 物理的にぶつかる衝突判定＝＞　Collision系.
	//--------------------------------------------------------
	#region OnTrigger:Collider
	/// <summary>
	/// Colliderが他のトリガーイベントに侵入したとき.
	/// </summary>
	/// <param name="col"></param>
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "DeathPoint")
		{
			currentFlag |= FlagTypes.damage;
			currentFlag |= FlagTypes.invincible;

			rigidbody2D.velocity = Vector2.zero;

			// Lifeを０に.
			playerLife.LifeIsDeath();
		}

		if ((TagUtility.getParentTagName(col.gameObject) == "Enemy" || TagUtility.getParentTagName(col.gameObject) == "Boss") &&
			(currentFlag & FlagTypes.invincible) != FlagTypes.invincible &&
			(currentFlag & FlagTypes.stan) != FlagTypes.stan &&
			(currentFlag & FlagTypes.sex) != FlagTypes.sex)
		{
			StartCoroutine(KnockBack(!flipX));
			HitAttackName(TagUtility.getParentTagName(col.gameObject));
		}
	}

	/// <summary>
	/// Colliderが他のトリガーイベントより撤退したとき.
	/// </summary>
	/// <param name="col"></param>
	//void OnTriggerExit2D(Collider2D col)
	//{
	//}

	/// <summary>
	/// Colliderが他のトリガーイベントに滞在している間.
	/// </summary>
	/// <param name="col"></param>
	void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "NeedleWall")
		{
			if ((currentFlag & FlagTypes.invincible) != FlagTypes.invincible &&
				(currentFlag & FlagTypes.knockBack) != FlagTypes.knockBack &&
				(currentFlag & FlagTypes.stan) != FlagTypes.stan)
			{
				Damage(playerLife.IsMaxLife / 10, !flipX);
			}
		}
	}
	#endregion

	#region OnCollision:Collision
	/// <summary>
	/// 他のcollider/rigidbodyに触れたとき.
	/// </summary>
	/// <param name="col"></param>
	//void OnCollisionEnter2D(Collision2D col)
	//{
	//}

	/// <summary>
	/// 他のcollider/rigidbodyと触れ合うのをやめたとき.
	/// </summary>
	/// <param name="col"></param>
	//void OnCollisionExit2D(Collision2D col)
	//{
	//}

	/// <summary>
	/// 他のrigidbody/colliderに触れている間.
	/// </summary>
	/// <param name="col"></param>
	//void OnCollisionStay2D(Collision2D col)
	//{
	//	if (TagUtility.getParentTagName(col.gameObject) == "Enemy")
	//	{
	//	}
	//}
	#endregion
}