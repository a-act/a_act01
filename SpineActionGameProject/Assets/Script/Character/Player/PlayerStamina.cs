﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStamina : MonoBehaviour
{
	[SerializeField, Range(0, 100), Tooltip("現在のスタミナ値")]
	private float nowStamina = 100.0f;		// 現在のスタミナ値.
	[SerializeField, Range(0, 100), Tooltip("ダッシュで消費するスタミナ値")]
	private float consumeInDash = 30.0f;	// ダッシュで消費するスタミナ値.
	[SerializeField, Range(0, 10), Tooltip("1秒で回復するスタミナ量")]
	private int recoverySpeed = 6;			// スタミナの自然回復する速度.

	//private Image yellowGauge;              // スタミナゲージ（黄色ゲージ）.
	private Image staminaPointGauge;		// スタミナゲージ.

	private float maxStamina;      // スタミナの最大値.

	public float IsNowStamina
	{
		get { return nowStamina; }
	}


	void Start()
	{
		//yellowGauge = GameObject.Find("CanvasUI/PlayerGauge(Clone)/StaminaGauge/YellowGauge").GetComponent<Image>();
		staminaPointGauge = GameObject.Find("CanvasUI/PlayerStatus(Clone)/Op_Position/Op_Stamina/Op_Status_Stamina_Gage").GetComponent<Image>();

		maxStamina = nowStamina;
		//yellowGauge.fillAmount *= (nowStamina / maxStamina);
		staminaPointGauge.fillAmount *= (nowStamina / maxStamina);
	}

	void Update()
	{
		if (nowStamina < maxStamina)
		{
			nowStamina += Time.deltaTime * recoverySpeed;
		}

		//yellowGauge.fillAmount = (nowStamina / maxStamina);
		staminaPointGauge.fillAmount = (nowStamina / maxStamina);
	}

	/// <summary>
	/// ダッシュ.
	/// </summary>
	public bool DashMove()
	{
		if (nowStamina >= consumeInDash)
		{
			nowStamina -= consumeInDash;
			//yellowGauge.fillAmount = (nowStamina / maxStamina);
			staminaPointGauge.fillAmount = (nowStamina / maxStamina);
			return true;
		}
		else
		{
			return false;
		}
	}
}