﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Spine.Unity;

public class PlayerWeapon : MonoBehaviour
{
	[SerializeField, Range(0, 99)]
	private int attackPoint = 20;               // 攻撃力.

	BoundingBoxFollower follower;
	private BoxCollider2D boxCollider2D;        // BoxCollider2D.
	private Vector2 StandardColliderOffset;     // コライダーの標準位置.
	private Vector2 StandardColliderSize;       // コライダーの標準サイズ.

	private bool flipX = false;

	public int IsAttackPoint
	{
		get { return attackPoint; }
		set { attackPoint = value; }
	}


	void Start()
	{
		follower = GetComponent<BoundingBoxFollower>();
		boxCollider2D = GetComponent<BoxCollider2D>();
		StandardColliderOffset = boxCollider2D.offset;
		StandardColliderSize = boxCollider2D.size;
	}

	/// <summary>
	/// 向きと当たり判定
	/// </summary>
	public void ReverseCollider(bool flip)
	{
		flipX = flip;
		if (flip)
		{
			transform.rotation = Quaternion.Euler(0, 180, 0);
		}
		else
		{
			transform.rotation = Quaternion.Euler(0, 0, 0);
		}
	}

	/// <summary>
	/// 判定のON-OFF.
	/// </summary>
	public void SetActiveCollider(bool type)
	{
		if (boxCollider2D.enabled == type)
		{
			boxCollider2D.enabled = !type;

			StartCoroutine(WaitTimer(0.01f));

			boxCollider2D.enabled = type;
		}
		else
		{
			boxCollider2D.enabled = type;
			//follower.enabled = type;
		}
	}

	IEnumerator WaitTimer(float time)
	{
		yield return new WaitForSeconds(time);
	}

	/// <summary>
	/// 判定位置のリセット.
	/// </summary>
	public void ResetColliderOffset()
	{
		boxCollider2D.offset = StandardColliderOffset;
		boxCollider2D.size = StandardColliderSize;
	}
	#region Collider2D

	void OnTriggerEnter2D(Collider2D col)
	{
		//col.SendMessage("Hit", follower.CurrentAttachmentName, SendMessageOptions.DontRequireReceiver);

		if (TagUtility.getParentTagName(col.gameObject) == "Enemy")
		{
			// Enemyのダメージ処理へ.
			col.gameObject.transform.Find("Life").GetComponent<EnemyLife>().LifeDown(attackPoint);
			//col.gameObject.GetComponent<Enemy>().Damage();
			col.gameObject.GetComponent<DamageIndication>().Damage(col, attackPoint);

			boxCollider2D.enabled = false;
		}

		if (TagUtility.getParentTagName(col.gameObject) == "Boss")
		{
			// BossEnemyのダメージ処理へ.
			col.gameObject.transform.Find("Life").GetComponent<BossLife>().LifeDown(attackPoint);
			//col.gameObject.GetComponent<Boss>().Damage();
			col.gameObject.GetComponent<DamageIndication>().Damage(col, attackPoint);

			boxCollider2D.enabled = false;
		}
	}

	/// <summary>
	/// TriggerチェックがON で離れたとき.
	/// </summary>
	/// <param name="col"></param>
	//void OnTriggerExit2D(Collider2D col)
	//{
	//}

	/// <summary>
	/// TriggerチェックがON で触れている間.
	/// </summary>
	/// <param name="col"></param>
	//void OnTriggerStay2D(Collider2D col)
	//{
	//}
	#endregion

	#region Collision2D
	/// <summary>
	/// オブジェクトが衝突したとき.
	/// </summary>
	/// <param name="col"></param>
	//void OnCollisionEnter2D(Collision2D col)
	//{
	//}

	/// <summary>
	/// オブジェクトが離れた時.
	/// </summary>
	/// <param name="col"></param>
	//void OnCollisionExit2D(Collision2D col)
	//{
	//}

	/// <summary>
	/// オブジェクトが触れている間.
	/// </summary>
	/// <param name="col"></param>
	//void OnCollisionStay2D(Collision2D col)
	//{
	//}
	#endregion
}