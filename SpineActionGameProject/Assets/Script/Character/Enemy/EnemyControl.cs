﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MonoBehaviour
{
	public bool isStop = false;

	public bool IsStop
	{
		get { return isStop; }
		set { isStop = value; }
	}
}