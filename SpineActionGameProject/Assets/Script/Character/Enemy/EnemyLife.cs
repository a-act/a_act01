﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class EnemyLife : MonoBehaviour
{
	[SerializeField]
	private bool lifeDisplay = false;	// Lifeを表示するか.
	[SerializeField, Range(0,999)]
	private float maxLife = 200;        // 最大体力値.
	[SerializeField, Range(0,999)]
	private float nowLife = 200;        // 現在の体力値.
	[SerializeField]
	SpriteRenderer spriteBack;
	[SerializeField]
	SpriteRenderer spriteGauge;
	[SerializeField]
	Transform maskTransform;
	[SerializeField]
	Enemy enemy;

	private float rateLife;
	private float maskGaugeX;	// -2.0~0(now)可変.
	private Vector2 size;       // 2.0	 (max)固定.

	public int IsMaxLife
	{
		get { return (int)maxLife; }
	}
	public float IsNowLife
	{
		get	{ return nowLife; }
		set	{ nowLife = value; }
	}


	void Start()
	{
		rateLife = 1f;
		maskGaugeX = 0f;

		// 画像サイズ(1/100)を取得.
		// spr_bg.size (100/1の画像サイズ固定).
		// spr.bounds.size(1/100の画像サイズ可変（Scaleに比例して値が変わる）).
		size = new Vector2(spriteBack.bounds.size.x, spriteBack.bounds.size.y);

		iTween.FadeTo(gameObject, iTween.Hash("alpha", 0.0f,
												"time", 0.0f));
	}

	/// <summary>
	/// Lifeを減らす.
	/// </summary>
	/// <param name="attackPoint"></param>
	public void LifeDown(int attackPoint)
	{
		nowLife -= attackPoint;

		if (nowLife < 0)
		{
			nowLife = 0;
		}

		// Lifeの割合を求める.
		rateLife = nowLife / maxLife;
		// マスクゲージの座標を求める.
		maskGaugeX = rateLife * size.x;
		// マスクゲージは0がMax値なので値を反転.
		float x = -(size.x - maskGaugeX);

		maskTransform.localPosition = new Vector3(x, maskTransform.localPosition.y, maskTransform.localPosition.z);

		if (lifeDisplay)
		{
			StartCoroutine(showLifeFade());
		}
	}

	/// <summary>
	/// フェードアウト.
	/// </summary>
	/// <returns></returns>
	IEnumerator showLifeFade()
	{
		iTween.Stop(gameObject);

		iTween.FadeTo(gameObject, iTween.Hash("alpha", 1.0f,
												"time", 0.0f));

		yield return new WaitForSeconds(3f);

		iTween.FadeTo(gameObject, iTween.Hash("alpha", 0.0f,
												"time", 0.5f));
	}
}