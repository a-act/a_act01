﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BossWeapon : MonoBehaviour
{
	[SerializeField, Range(0, 99)]
	private int attackPoint = 10;           // 攻撃力.
	[SerializeField]
	private BossOrc boss;

	private bool flipX = false;             // 向き.

	private float attackCount = 0.0f;
	
	public int IsAttackPoint
	{
		get { return attackPoint; }
	}


	/// <summary>
	/// 向きと当たり判定
	/// </summary>
	public void ReverseCollider(bool flip)
	{
		flipX = flip;
		if (flip)
		{
			transform.rotation = Quaternion.Euler(0, 180, 0);
		}
		else
		{
			transform.rotation = Quaternion.Euler(0, 0, 0);
		}
	}

	/// <summary>
	/// 判定のON-OFF.
	/// </summary>
	public void SetActiveCollider(bool type)
	{
		boss.SetWeponeActiveCollider(type);
	}

	/// <summary>
	/// タグの変更.
	/// </summary>
	public void ChangeTag()
	{
		gameObject.tag = "Death";
		gameObject.layer = LayerMask.NameToLayer("Death");
	}


	#region Collider
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			col.SendMessage("HitAttackName",
							TagUtility.getChildTagName(gameObject.transform.parent.tag),
							SendMessageOptions.DontRequireReceiver);

			SetActiveCollider(false);

			col.gameObject.GetComponent<Player>().Damage(attackPoint, flipX);
		}
	}
	#endregion
}