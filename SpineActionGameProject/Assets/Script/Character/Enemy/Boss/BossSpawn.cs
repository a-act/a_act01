﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawn : MonoBehaviour
{
	[SerializeField, Tooltip("スポーン位置")]
	private Vector3 bossOrcSpawnPos = new Vector3(775.0f, 5.0f, -1.0f);
	[SerializeField, Tooltip("ボスのPrefab")]
	private GameObject bossOrcPrefab;
	[SerializeField, Tooltip("LifeゲージのPrefab")]
	private GameObject bossLifeGaugePrefab;
	[SerializeField]
	private GameObject despongObject;
	public AudioClip audioData;

	private GameObject canvas;
	private GameObject character;
	private GameObject bossObject;
	private GameObject bossLifeGaugeObject;
	private BossOrc boss;
	private BGMManager bgmManager;


	private void Start()
	{
		canvas = GameObject.Find("CanvasUI");
		character = GameObject.Find("Character/Enemy");
		bgmManager = GameObject.FindGameObjectWithTag("BGMManager").GetComponent<BGMManager>();
	}

	private void BossOrcCreater()
	{
		bossObject = Instantiate(bossOrcPrefab, new Vector3(bossOrcSpawnPos.x, bossOrcSpawnPos.y, -1), Quaternion.identity);
		bossObject.transform.parent = character.transform;
		boss = bossObject.GetComponent<BossOrc>();

		bossLifeGaugeObject = Instantiate(bossLifeGaugePrefab) as GameObject;
		bossLifeGaugeObject.transform.SetParent(canvas.transform, false);
	}

	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			BossOrcCreater();
			bgmManager.PlayBGM(audioData);
			despongObject.SetActive(true);
			gameObject.SetActive(false);
		}
	}
}