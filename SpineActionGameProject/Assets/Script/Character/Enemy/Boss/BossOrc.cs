﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using System;
using Spine;

public class BossOrc : MonoBehaviour
{
	#region Field
	[SerializeField, Range(0, 10)]
	private float moveSpeed = 4f;               // 移動速度.
	[SerializeField, Range(0, 30)]
	private float attckStepTimer = 1f;          // 攻撃するまでの時間.
	[SerializeField, Range(0, 20)]
	private float horizontalSearchRangeRadius = 6.0f;   // 索敵範囲(横).
	[SerializeField, Range(0, 20)]
	private float verticalSearchRangeRadius = 3.0f;     // 索敵範囲(縦).
	[SerializeField, Range(0, 20)]
	private float shortRangeValue = 4.0f;       // 近距離攻撃範囲.
	[SerializeField]
	private GameObject shortAttackEffect;       // 近距離攻撃エフェクト.
	[SerializeField, Range(0, 20)]
	private float mediumRangeValue = 10.0f;     // 中距離攻撃範囲.
												//[SerializeField]
												//private GameObject mediumAttackEffect;		// 遠距離攻撃エフェクト.
	[SerializeField, Range(0, 20)]
	private float longRangeValue = 18.0f;       // 遠距離攻撃範囲.
	[SerializeField]
	private GameObject longAttackEffect;        // 遠距離攻撃エフェクト.
	[SerializeField, Range(0, 10)]
	private float invincibleTimer = 1.0f;       // 無敵時間.
	[SerializeField]
	private Vector2 blowOffPower = new Vector2(5, 5); // ダメージを食らった際に吹き飛ぶ力の設定
	[SerializeField, Range(0, 10)]
	private int knockbackResistance = 10;        // ノックバック耐性.
	[SerializeField]
	private bool blowResist = false;            // ノックバックのキャンセル.
	[SerializeField, Range(0, 20)]
	private float flashingSpeed = 10;           // 点滅速度.
	[SerializeField, Range(0, 1)]
	private float flashingColor = 0.8f;         // 点滅時の色.
	[SerializeField]
	private LayerMask groundLayer;              // Linecastで判定するLayer.
	[SerializeField]
	private LayerMask targetLayer;              // Linecastで判定するLayer.
	[SerializeField]
	private new Rigidbody2D rigidbody2D;
	[SerializeField]
	private BossWeapon weapon;
	[SerializeField]
	private BossLife bossLife;
	[SerializeField]
	private Animator animator;
	[SerializeField]
	private GameObject deathEffect;     // 殺られエフェクト.


	private Player player;
	Player.FlagTypes currentFlag;

	private GameObject effect = null;
	private Vector3 transPos;
	private Vector3 startPos = Vector3.zero;    // 初期ポジション.
	private Vector2 movePos = Vector2.zero;     // 移動ポジション.

	private bool flipX = true;                  // 向き(true:左,false:右).
	private bool isGrounded = true;             // 着地判定.
	private bool isTarget = false;              // 標的が射程圏内に入ったか.
	private bool leftSearchRange = false;       // 左側の索敵結果.
	private bool rightSearchRange = false;      // 右側の索敵結果.
	private bool shortAttackRange = false;      // 近距離攻撃範囲内かどうか.
	private bool mediumAttackRange = false;     // 中距離攻撃範囲内かどうか.
	private bool longAttackRange = false;       // 遠距離攻撃範囲内かどうか.

	private int attackPoint;                    // 攻撃力.
	private int knockbackResistanceMax;        // ノックバック耐性値Max.

	private float attackAreaTimer;              // 攻撃範囲内にどのくらい居たか？
	private float nowLife = 0f;

	[Flags]
	public enum StateTypes
	{
		death = 0x01,           // 死んだか.
		wait = 0x02,            // 待機.
		walk = 0x04,            // 歩く.
		shortAttack = 0x08,     // 近距離攻撃.
		mediumAttack = 0x10,    // 中距離攻撃.
		longAttack = 0x20,      // 遠距離攻撃.
		attackRigor = 0x40,     // 攻撃後の硬直.
		knockback = 0x80,       // ノックバック.
		invincible = 0x100,     // 無敵.
		sex = 0x200,			// Hアニメ.
	}
	private StateTypes state;

	#region Spineモーション.
	public SkeletonAnimation skeletonAnimation;
	[SpineAnimation("attack1")]
	public string rushingAttackAnim = "attack1";
	[SpineAnimation("attack2")]
	public string nagashiroAttackAnim = "attack2";
	[SpineAnimation("attack3")]
	public string roarAttackAnim = "attack3";
	[SpineAnimation("attack")]  // attack1,2_reverberation.
	public string attackRigorAnim = "attack";
	[SpineAnimation("damage")]
	public string damageAnim = "damage";
	[SpineAnimation("wait")]
	public string waitingAnim = "wait";
	[SpineAnimation("walk")]
	public string walkAnim = "walk";
	#endregion

	#region Get/Set
	public bool IsFlipX
	{
		get { return flipX; }
	}
	public bool IsGround
	{
		get { return isGrounded; }
	}
	#endregion
	#endregion

	//private void OnGUI()
	//{
	//	Debug.Log("state:" + state);
	//}

	private void OnDestroy()
	{
		var bgmManager = GameObject.FindGameObjectWithTag("BGMManager").GetComponent<BGMManager>();
		bgmManager.StopBGM();
	}

	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		startPos = gameObject.transform.position;
		skeletonAnimation.Skeleton.FlipX = flipX;
		attackAreaTimer = 0;
		knockbackResistanceMax = knockbackResistance;
		weapon.ReverseCollider(flipX);

		state = StateTypes.wait;
	}

	void Update()
	{
		if ((state & StateTypes.death) == StateTypes.death)
		{
			iTween.Stop(gameObject);
			SetSpineAnimation();
			return;
		}

		transPos = transform.position;

		SearchCast();
		currentFlag = player.CurrentFlag;

		if ((currentFlag & Player.FlagTypes.sex) == Player.FlagTypes.sex)
		{
			skeletonAnimation.skeleton.SetColor(new Color(0, 0, 0, 0));
		}
		else if ((state & StateTypes.invincible) != StateTypes.invincible)
		{
			skeletonAnimation.skeleton.SetColor(new Color(1, 1, 1, 1));
		}
		else
		{
			if ((state & StateTypes.death) != StateTypes.death)
			{
				// 点滅
				float level = Mathf.Abs(Mathf.Sin(Time.time * flashingSpeed));
				skeletonAnimation.skeleton.SetColor(new Color(flashingColor, flashingColor, flashingColor, level));
			}
			else
			{
				StartCoroutine(SkeletonColor());
			}
		}

		AliveCheck();
		SetSpineAnimation();
		MoveCheck();
		Attack();
		AttackWaitTime();

		skeletonAnimation.Skeleton.FlipX = flipX;

		if (isGrounded &&
			(!leftSearchRange && !rightSearchRange && (state & StateTypes.walk) == StateTypes.walk) &&
			(state & StateTypes.shortAttack) != StateTypes.shortAttack &&
			(state & StateTypes.mediumAttack) != StateTypes.mediumAttack &&
			(state & StateTypes.longAttack) != StateTypes.longAttack &&
			(state & StateTypes.attackRigor) != StateTypes.attackRigor)
		{
			state = StateTypes.wait;
		}
	}


	void SearchCast()
	{
		// Linecastで足元に地面があるか判定.
		isGrounded = Physics2D.Linecast(transPos + transform.up * 1,
										transPos - transform.up * 0.05f,
										groundLayer);
		#region Debug
		Debug.DrawLine(transPos + transform.up * 1,
					transPos - transform.up * 0.05f,
					Color.green);
		#endregion

		if ((state & StateTypes.shortAttack) != StateTypes.shortAttack &&
			(state & StateTypes.mediumAttack) != StateTypes.mediumAttack &&
			(state & StateTypes.longAttack) != StateTypes.longAttack)
		{
			#region SearchRange
			leftSearchRange = Physics2D.Linecast(transPos + transform.up * verticalSearchRangeRadius,
												transPos - transform.right * horizontalSearchRangeRadius + transform.up * verticalSearchRangeRadius,
												targetLayer) ||
							Physics2D.Linecast(transPos + transform.up * (verticalSearchRangeRadius / 3),
												transPos - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 3),
												targetLayer) ||
							Physics2D.Linecast(transPos + transform.up * (verticalSearchRangeRadius / 3 * 2),
												transPos - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 3 * 2),
												targetLayer);

			rightSearchRange = Physics2D.Linecast(transPos + transform.up * verticalSearchRangeRadius,
													transPos + transform.right * horizontalSearchRangeRadius + transform.up * verticalSearchRangeRadius,
													targetLayer) ||
								Physics2D.Linecast(transPos + transform.up * (verticalSearchRangeRadius / 3),
													transPos + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 3),
													targetLayer) ||
								Physics2D.Linecast(transPos + transform.up * (verticalSearchRangeRadius / 3 * 2),
													transPos + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 3 * 2),
													targetLayer);
			#endregion
			#region Debug
			Debug.DrawLine(transPos - transform.right * horizontalSearchRangeRadius + transform.up * verticalSearchRangeRadius,
							transPos + transform.right * horizontalSearchRangeRadius + transform.up * verticalSearchRangeRadius,
							Color.blue);
			Debug.DrawLine(transPos - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 3),
							transPos + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 3),
							Color.blue);
			Debug.DrawLine(transPos - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 3 * 2),
							transPos + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 3 * 2),
							Color.blue);
			#endregion
		}


		if (flipX)
		{
			#region AttackRange
			mediumAttackRange = Physics2D.Linecast(transPos + (transform.up - transform.right * shortRangeValue),
												transPos + (transform.up - transform.right * mediumRangeValue),
												targetLayer) ||
							Physics2D.Linecast(transPos + (transform.up * 3.0f - transform.right * shortRangeValue),
												transPos + (transform.up * 3.0f - transform.right * mediumRangeValue),
												targetLayer) ||
							Physics2D.Linecast(transPos + (transform.up * 5.0f - transform.right * shortRangeValue),
												transPos + (transform.up * 5.0f - transform.right * mediumRangeValue),
												targetLayer);

			longAttackRange = Physics2D.Linecast(transPos + (transform.up - transform.right * mediumRangeValue),
													transPos + (transform.up - transform.right * longRangeValue),
													targetLayer) ||
								Physics2D.Linecast(transPos + (transform.up * 3.0f - transform.right * mediumRangeValue),
													transPos + (transform.up * 3.0f - transform.right * longRangeValue),
													targetLayer);

			shortAttackRange = Physics2D.Linecast(transPos + (transform.up * 3.0f - transform.right * 1.0f),
													transPos + (transform.up * 3.0f - transform.right * shortRangeValue),
													targetLayer) ||
								Physics2D.Linecast(transPos + (transform.up * 5.0f - transform.right * 1.0f),
													transPos + (transform.up * 5.0f - transform.right * shortRangeValue),
													targetLayer);
			#endregion
			#region Debug
			#region mediumAttackRange
			Debug.DrawLine(transPos + (transform.up - transform.right * shortRangeValue),
							transPos + (transform.up - transform.right * mediumRangeValue),
							Color.yellow);
			Debug.DrawLine(transPos + (transform.up * 3.0f - transform.right * shortRangeValue),
							transPos + (transform.up * 3.0f - transform.right * mediumRangeValue),
							Color.yellow);
			Debug.DrawLine(transPos + (transform.up * 5.0f - transform.right * shortRangeValue),
							transPos + (transform.up * 5.0f - transform.right * mediumRangeValue),
							Color.yellow);
			#endregion
			#region longAttackRange
			Debug.DrawLine(transPos + (transform.up - transform.right * mediumRangeValue),
							transPos + (transform.up - transform.right * longRangeValue),
							Color.green);
			Debug.DrawLine(transPos + (transform.up * 3.0f - transform.right * mediumRangeValue),
							transPos + (transform.up * 3.0f - transform.right * longRangeValue),
							Color.green);
			#endregion
			#region shortAttackRange
			Debug.DrawLine(transPos + (transform.up * 3.0f - transform.right * 1.0f),
							transPos + (transform.up * 3.0f - transform.right * shortRangeValue),
							Color.red);
			Debug.DrawLine(transPos + (transform.up * 5.0f - transform.right * 1.0f),
							transPos + (transform.up * 5.0f - transform.right * shortRangeValue),
							Color.red);
			#endregion
			#endregion
		}
		else
		{
			#region AttackRange
			mediumAttackRange = Physics2D.Linecast(transPos + (transform.up + transform.right * shortRangeValue),
													transPos + (transform.up + transform.right * mediumRangeValue),
													targetLayer) ||
								Physics2D.Linecast(transPos + (transform.up * 3.0f + transform.right * shortRangeValue),
													transPos + (transform.up * 3.0f + transform.right * mediumRangeValue),
													targetLayer) ||
								Physics2D.Linecast(transPos + (transform.up * 5.0f + transform.right * shortRangeValue),
													transPos + (transform.up * 5.0f + transform.right * mediumRangeValue),
													targetLayer);

			longAttackRange = Physics2D.Linecast(transPos + (transform.up + transform.right * mediumRangeValue),
													transPos + (transform.up + transform.right * longRangeValue),
													targetLayer) ||
								Physics2D.Linecast(transPos + (transform.up * 3.0f + transform.right * mediumRangeValue),
													transPos + (transform.up * 3.0f + transform.right * longRangeValue),
													targetLayer);

			shortAttackRange = Physics2D.Linecast(transPos + (transform.up * 3.0f + transform.right * 1.0f),
													transPos + (transform.up * 3.0f + transform.right * shortRangeValue),
													targetLayer) ||
								Physics2D.Linecast(transPos + (transform.up * 5.0f + transform.right * 1.0f),
													transPos + (transform.up * 5.0f + transform.right * shortRangeValue),
													targetLayer);
			#endregion
			#region Debug
			#region mediumAttackRange
			Debug.DrawLine(transPos + (transform.up + transform.right * shortRangeValue),
							transPos + (transform.up + transform.right * mediumRangeValue),
							Color.yellow);
			Debug.DrawLine(transPos + (transform.up * 3.0f + transform.right * shortRangeValue),
							transPos + (transform.up * 3.0f + transform.right * mediumRangeValue),
							Color.yellow);
			Debug.DrawLine(transPos + (transform.up * 5.0f + transform.right * shortRangeValue),
							transPos + (transform.up * 5.0f + transform.right * mediumRangeValue),
							Color.yellow);
			#endregion
			#region longAttackRange
			Debug.DrawLine(transPos + (transform.up + transform.right * mediumRangeValue),
							transPos + (transform.up + transform.right * longRangeValue),
							Color.green);
			Debug.DrawLine(transPos + (transform.up * 3.0f + transform.right * mediumRangeValue),
							transPos + (transform.up * 3.0f + transform.right * longRangeValue),
							Color.green);
			#endregion
			#region shortAttackRange
			Debug.DrawLine(transPos + (transform.up * 3.0f + transform.right * 1.0f),
							transPos + (transform.up * 3.0f + transform.right * shortRangeValue),
							Color.red);
			Debug.DrawLine(transPos + (transform.up * 5.0f + transform.right * 1.0f),
							transPos + (transform.up * 5.0f + transform.right * shortRangeValue),
							Color.red);
			#endregion
			#endregion
		}
	}

	#region 移動系.
	/// <summary>
	/// 移動処理.
	/// </summary>
	public void MoveCheck()
	{
		if (!isGrounded || (!leftSearchRange && !rightSearchRange) ||
			(currentFlag & Player.FlagTypes.invincible) == Player.FlagTypes.invincible ||
			(currentFlag & Player.FlagTypes.sex) == Player.FlagTypes.sex ||
			(state & StateTypes.attackRigor) == StateTypes.attackRigor ||
			(state & StateTypes.shortAttack) == StateTypes.shortAttack ||
			(state & StateTypes.mediumAttack) == StateTypes.mediumAttack ||
			(state & StateTypes.longAttack) == StateTypes.longAttack ||
			(state & StateTypes.knockback) == StateTypes.knockback ||
			(state & StateTypes.death) == StateTypes.death ||
			(state & StateTypes.sex) == StateTypes.sex)
		{
			return;
		}

		// 向きを変える.
		if (leftSearchRange)
		{
			flipX = true;
		}
		else if (rightSearchRange)
		{
			flipX = false;
		}

		if (flipX)
		{
			movePos.x = -moveSpeed * 0.5f;
			weapon.ReverseCollider(flipX);
		}
		else
		{
			movePos.x = moveSpeed * 0.5f;
			weapon.ReverseCollider(flipX);
		}

		state = StateTypes.walk;

		movePos.y = rigidbody2D.velocity.y;
		rigidbody2D.velocity = movePos;
	}
	#endregion

	#region 攻撃系.
	/// <summary>
	/// 攻撃をする.
	/// </summary>
	void Attack()
	{
		if (!shortAttackRange && !mediumAttackRange && !longAttackRange)
		{
			return;
		}

		currentFlag = player.CurrentFlag;

		// Playerが無敵中でなければ.
		if ((currentFlag & Player.FlagTypes.invincible) != Player.FlagTypes.invincible &&
			(currentFlag & Player.FlagTypes.sex) != Player.FlagTypes.sex &&
			(state & StateTypes.shortAttack) != StateTypes.shortAttack &&
			(state & StateTypes.mediumAttack) != StateTypes.mediumAttack &&
			(state & StateTypes.longAttack) != StateTypes.longAttack &&
			(state & StateTypes.attackRigor) != StateTypes.attackRigor &&
			(state & StateTypes.knockback) != StateTypes.knockback &&
			(state & StateTypes.attackRigor) != StateTypes.attackRigor)
		{
			if ((state & StateTypes.sex) != StateTypes.sex)
			{
				if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != roarAttackAnim &&
				skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != rushingAttackAnim &&
				skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != nagashiroAttackAnim)
				{
					if (shortAttackRange)
					{
						state = StateTypes.shortAttack;
						return;
					}

					if (mediumAttackRange)
					{
						state = StateTypes.mediumAttack;
						return;
					}

					if (longAttackRange)
					{
						state = StateTypes.longAttack;
						return;
					}
				}
			}
			else
			{
				state &= ~StateTypes.sex;
			}
		}
	}

	void AttackWaitTime()
	{
		if (attackAreaTimer > 0)
		{
			attackAreaTimer -= Time.deltaTime;
			state = StateTypes.attackRigor;
		}
		else
		{
			state &= ~StateTypes.attackRigor;
		}
	}
	#endregion

	#region ダメージ系.
	/// <summary>
	/// 攻撃を受けた.
	/// </summary>
	public void Damage()
	{
		if ((state & StateTypes.death) != StateTypes.death)
		{
			StartCoroutine(DamageAction());
		}
	}

	/// <summary>
	/// ダメージ処理.
	/// </summary>
	/// <returns></returns>
	public IEnumerator DamageAction()
	{
		//state = StateTypes.invincible;

		//attackAreaTimer = attckStepTimer;
		nowLife = bossLife.IsNowLife;

		skeletonAnimation.AnimationState.TimeScale = 0.5f;
		yield return null;
		skeletonAnimation.AnimationState.TimeScale = 1.0f;

		StartCoroutine(knockbackCorutine());

		//state &= ~StateTypes.invincible;

		yield return null;
	}

	/// <summary>
	/// ノックバック処理.
	/// </summary>
	/// <returns></returns>
	IEnumerator knockbackCorutine()
	{
		if (!blowResist ||
			(state & StateTypes.shortAttack) != StateTypes.shortAttack &&
			(state & StateTypes.mediumAttack) != StateTypes.mediumAttack &&
			(state & StateTypes.longAttack) != StateTypes.longAttack)
		{
			if (knockbackResistance > 0)
			{
				knockbackResistance--;
			}
			else if (knockbackResistance <= 0)
			{
				state &= ~(StateTypes.shortAttack | StateTypes.mediumAttack | StateTypes.longAttack);
				state = StateTypes.knockback;

				// Y軸が移動中でなければノックバック.
				if (rigidbody2D.velocity.y == 0)
				{
					if (player.IsFlipX)
					{
						//rigidbody2D.velocity += new Vector2(-blowOffPower.x, blowOffPower.y);
						rigidbody2D.AddForce(new Vector2(-blowOffPower.x, blowOffPower.y), ForceMode2D.Impulse);
					}
					else
					{
						//rigidbody2D.velocity += new Vector2(blowOffPower.x, blowOffPower.y);
						rigidbody2D.AddForce(new Vector2(blowOffPower.x, blowOffPower.y), ForceMode2D.Impulse);
					}
				}
				knockbackResistance = knockbackResistanceMax;
			}
			yield return new WaitForSeconds(invincibleTimer);
		}
		state &= ~StateTypes.knockback;
	}
	#endregion

	#region 生死判定.
	/// <summary>
	/// 生きているかのチェック.
	/// </summary>
	/// <returns></returns>
	void AliveCheck()
	{
		nowLife = bossLife.IsNowLife;

		if (nowLife <= 0)
		{
			if ((state & StateTypes.death) != StateTypes.death)
			{
				state = (StateTypes.death | StateTypes.invincible);
				gameObject.tag = "Death";
				gameObject.layer = LayerMask.NameToLayer("Death");
				weapon.ChangeTag();
				if (effect == null)
				{
					effect = Instantiate(deathEffect, transPos, transform.rotation) as GameObject;
				}

				Destroy(gameObject, 3f);
			}
		}
	}
	#endregion

	#region 武器Collision系.
	/// <summary>
	/// 武器コリジョン操作.
	/// </summary>
	/// <param name="type"></param>
	public void SetWeponeActiveCollider(bool type)
	{
		animator.SetBool("RushingAttack", type);
	}
	#endregion

	/// <summary>
	/// スポーン位置保存.
	/// </summary>
	public void SaveStartPos()
	{
		startPos = gameObject.transform.position;
	}

	#region skeletonのカラー.
	IEnumerator SkeletonColor()
	{
		for (int i = 255; i > 0; i -= 5)
		{
			skeletonAnimation.skeleton.SetColor(new Color((float)0 / 255, (float)0 / 255, (float)0 / 255, (float)i / 255));

			if (i <= 0)
			{
				Destroy(gameObject);
			}

			yield return 0;
		}
	}
	#endregion

	#region SpineAnimation
	/// <summary>
	/// Spineモーション制御.
	/// </summary>
	void SetSpineAnimation()
	{
		switch (state)
		{
			case StateTypes.death:
				OnAttackRigorAnim();
				break;
			case StateTypes.wait:
				OnWatingAnim();
				break;
			case StateTypes.walk:
				OnWalkAnim();
				break;
			case StateTypes.shortAttack:
				OnRoarAttackAnim();
				break;
			case StateTypes.mediumAttack:
				OnRushingAttackAnim();
				break;
			case StateTypes.longAttack:
				OnNagashiroAttackAnim();
				break;
			case StateTypes.attackRigor:
				OnAttackRigorAnim();
				break;
			case StateTypes.knockback:
				OnDamageAnim();
				break;
			default:
				break;
		}
	}

	/// <summary>
	/// 近接攻撃アニメーション(咆哮).
	/// </summary>
	void OnRoarAttackAnim()
	{
		if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != roarAttackAnim)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, roarAttackAnim, false);

			StartCoroutine(OnRoarAttackAnimCoroutine());

			skeletonAnimation.AnimationState.Complete += AttackRigor;
		}
	}

	IEnumerator OnRoarAttackAnimCoroutine()
	{
		yield return new WaitForSeconds(0.9f);

		if (effect == null)
		{
			effect = Instantiate(shortAttackEffect, transform.position, transform.rotation) as GameObject;
			effect.transform.SetParent(transform);
			effect.transform.position = new Vector3(transform.position.x, 6.3f, -5f);

			Destroy(effect, 2.7f);
		}
	}

	/// <summary>
	/// 中距離攻撃(2段階突進).
	/// </summary>
	void OnRushingAttackAnim()
	{
		if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != rushingAttackAnim)
		{
			SetWeponeActiveCollider(true);

			skeletonAnimation.AnimationState.SetAnimation(0, rushingAttackAnim, false);

			StartCoroutine(OnRushingAttackAnimCoroutine());

			skeletonAnimation.AnimationState.Complete += AttackRigor;
		}
	}

	IEnumerator OnRushingAttackAnimCoroutine()
	{
		yield return new WaitForSeconds(1.5f);

		if (!flipX)
		{
			iTween.MoveAdd(gameObject, iTween.Hash("x", 5.0f,
													"time", 0.8f));
		}
		else
		{
			iTween.MoveAdd(gameObject, iTween.Hash("x", -5.0f,
										"time", 0.8f));
		}

		yield return new WaitForSeconds(0.9f);

		if (!flipX)
		{
			iTween.MoveAdd(gameObject, iTween.Hash("x", 5.0f,
													"time", 0.8f));
		}
		else
		{
			iTween.MoveAdd(gameObject, iTween.Hash("x", -5.0f,
										"time", 0.8f));
		}
	}

	/// <summary>
	/// 遠距離攻撃(衝撃波).
	/// </summary>
	void OnNagashiroAttackAnim()
	{
		if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != nagashiroAttackAnim)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, nagashiroAttackAnim, false);
			skeletonAnimation.AnimationState.Complete += AttackRigor;

			StartCoroutine(OnNagashiroAttackAnimCoroutine());
		}
	}

	IEnumerator OnNagashiroAttackAnimCoroutine()
	{
		yield return new WaitForSeconds(2.7f);

		if (effect == null)
		{
			GameObject effect2, effect3;

			if (!flipX)
			{
				effect = Instantiate(longAttackEffect, transform.position, new Quaternion(0, 180, 0, 1)) as GameObject;
				effect.transform.position = new Vector3(transform.position.x + 5f, transform.position.y - 0.5f, -5f);
				effect.tag = gameObject.tag;
				yield return new WaitForSeconds(0.2f);

				effect2 = Instantiate(longAttackEffect, transform.position, new Quaternion(0, 180, 0, 1)) as GameObject;
				effect2.transform.position = new Vector3(effect.transform.position.x + 3f, transform.position.y - 0.5f, -5f);
				effect2.tag = gameObject.tag;
				yield return new WaitForSeconds(0.2f);

				effect3 = Instantiate(longAttackEffect, transform.position, new Quaternion(0, 180, 0, 1)) as GameObject;
				effect3.transform.position = new Vector3(effect2.transform.position.x + 3f, transform.position.y - 0.5f, -5f);
				effect3.tag = gameObject.tag;

				//iTween.MoveAdd(effect, iTween.Hash("x", -10.0f,
				//									"time", 10.0f));
			}
			else
			{
				effect = Instantiate(longAttackEffect, transform.position, transform.rotation) as GameObject;
				effect.transform.position = new Vector3(transform.position.x - 5f, transform.position.y - 0.5f, -5f);
				effect.tag = gameObject.tag;
				yield return new WaitForSeconds(0.2f);

				effect2 = Instantiate(longAttackEffect, transform.position, transform.rotation) as GameObject;
				effect2.transform.position = new Vector3(effect.transform.position.x - 3f, transform.position.y - 0.5f, -5f);
				effect2.tag = gameObject.tag;
				yield return new WaitForSeconds(0.2f);

				effect3 = Instantiate(longAttackEffect, transform.position, transform.rotation) as GameObject;
				effect3.transform.position = new Vector3(effect2.transform.position.x - 3f, transform.position.y - 0.5f, -5f);
				effect3.tag = gameObject.tag;

				//iTween.MoveAdd(effect, iTween.Hash("x", -10.0f,
				//									"time", 10.0f));
			}

			Destroy(effect);
			Destroy(effect2, 0.3f);
			Destroy(effect3, 0.8f);
		}
	}

	/// <summary>
	/// 硬直アニメーション.
	/// </summary>
	void OnAttackRigorAnim()
	{
		if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != attackRigorAnim)
		{
			skeletonAnimation.AnimationState.AddAnimation(0, attackRigorAnim, true, 0);

			state &= ~(StateTypes.shortAttack | StateTypes.mediumAttack | StateTypes.longAttack);
		}
	}

	// ダメージモーション.
	void OnDamageAnim()
	{
		if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != damageAnim)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, damageAnim, false);
		}
	}

	// 待機モーション.
	void OnWatingAnim()
	{
		if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != waitingAnim)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, waitingAnim, true);
		}
	}

	// 歩くモーション.
	void OnWalkAnim()
	{
		if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != walkAnim)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, walkAnim, true);
		}
	}

	void AttackRigor(TrackEntry trackEntry)
	{
		if (trackEntry.animation.Name == roarAttackAnim)
		{
			state &= ~StateTypes.shortAttack;
			skeletonAnimation.AnimationState.Complete -= AttackRigor;
		}
		else
		{
			attackAreaTimer = attckStepTimer;
			skeletonAnimation.AnimationState.Complete -= AttackRigor;
		}
	}
	#endregion


	#region Collider2D
	void OnTriggerEnter2D(Collider2D col)
	{
		//if (col.gameObject.tag == "DeathPoint")
		//{
		//	//gameObject.transform.position = startPos;
		//	Destroy(gameObject, 3f);
		//}

		if (col.gameObject.tag == "PlayerWeapon" || col.gameObject.tag == "Knife")
		{
			Damage();
		}

		//if (col.gameObject.tag == "NeedleWall")
		//{
		//	bossLife.LifeDown(bossLife.IsMaxLife);
		//}
	}

	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			currentFlag = player.CurrentFlag;

			if ((currentFlag & Player.FlagTypes.invincible) != Player.FlagTypes.invincible &&
				(currentFlag & Player.FlagTypes.stan) == Player.FlagTypes.stan)
			{
				col.SendMessage("HitAttackName",
					TagUtility.getChildTagName(gameObject.tag),
					SendMessageOptions.DontRequireReceiver);
				player.Damage(0, flipX);

				state = StateTypes.sex;
			}
		}
	}
	#endregion
}