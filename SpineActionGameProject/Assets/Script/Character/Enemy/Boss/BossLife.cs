﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BossLife : MonoBehaviour
{
	[SerializeField, Range(0, 9999)]
	public int nowLife = 400;       // 体力値.
	[SerializeField, Range(0.1f, 1.0f)]
	private float fillProp = 1.0f;  // Lifeゲージの最大割合.
	[SerializeField, Range(0.1f, 10.0f)]
	private float moveTime = 1.0f;  // 赤ゲージの動く速度.
	[SerializeField, Range(0.1f, 10.0f)]
	private float waitTime = 1.0f;  // ダメージを受けてからゲージを動かすまでのインターバル.

	private GameObject bossLife;
	private Image lifeGauge;

	private int maxLife;       // 最大体力値.

	public int IsNowLife
	{
		get { return nowLife; }
	}


	void Start()
	{
		bossLife = GameObject.Find("CanvasUI/BossEnemyLife(Clone)");
		lifeGauge = GameObject.Find("CanvasUI/BossEnemyLife(Clone)/Life").GetComponent<Image>();

		maxLife = nowLife;
		lifeGauge.fillAmount *= ((float)nowLife / (float)maxLife) * fillProp;
	}

	private void Update()
	{
		if (nowLife <= 0)
		{
			Destroy(bossLife, 3f);
		}
	}

	/// <summary>
	/// Life減算.
	/// </summary>
	/// <param name="attackPoint"></param>
	public void LifeDown(int attackPoint)
	{
		downLifeGreenGauge(nowLife - attackPoint);
		nowLife -= attackPoint;
	}

	/// <summary>
	/// ゲージを動かす.
	/// </summary>
	/// <param name="point"></param>
	void SetLife(float point)
	{
		if (point > maxLife)
		{
			point = maxLife;
		}

		//iTween.ValueTo(redGauge.gameObject, iTween.Hash(
		//		"from", nowLife,
		//		"to", point,
		//		"time", moveTime,
		//		"delay", waitTime,
		//		"easetype", iTween.EaseType.linear,
		//		"onupdate", "downLifeRedGauge",
		//		"onupdatetarget", gameObject));
	}

	/// <summary>
	/// 緑ゲージの見た目変更.
	/// </summary>
	/// <param name="damage"></param>
	void downLifeGreenGauge(float point)
	{
		lifeGauge.fillAmount = (point / (float)maxLife) * fillProp;
	}

	/// <summary>
	/// 赤ゲージの見た目変更.
	/// </summary>
	/// <param name="damage"></param>
	void downLifeRedGauge(float damage)
	{
		//if (nowLife <= maxLife)
		//{
		//	redGauge.fillAmount = (damage / (float)maxLife) * fillProp;
		//}
	}
}