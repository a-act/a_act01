﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDespong : MonoBehaviour
{
	[SerializeField]
	private GameObject spawnObject;

	private BGMManager bgmManager;


	private void Start()
	{
		bgmManager = GameObject.FindGameObjectWithTag("BGMManager").GetComponent<BGMManager>();
	}

	public void BossOrcDelete()
	{
		var obj = GameObject.Find("Character/Enemy/BossOrc(Clone)");
		var obj2 = GameObject.Find("CanvasUI/BossEnemyLife(Clone)");

		if (obj != null || obj2 != null)
		{
			Destroy(obj);
			Destroy(obj2);

			bgmManager.StopBGM();
			SetActivate();
		}
		else
		{
			SetActivate();
		}
	}

	void SetActivate()
	{
		spawnObject.SetActive(true);
		gameObject.SetActive(false);
	}

	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			BossOrcDelete();
		}
	}
}
