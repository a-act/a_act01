﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyWeapon : MonoBehaviour
{
	[SerializeField, Range(0, 99)]
	private int attackPoint = 10;			// 攻撃力.
	[SerializeField]
	private Enemy enemy;

	private bool flipX = false;				// 向き.

	private float attackCount = 0.0f;

	public int IsAttackPoint
	{
		get { return attackPoint; }
	}


	private void Update()
	{
		flipX = enemy.IsFlipX;
	}

	/// <summary>
	/// 判定のON-OFF.
	/// </summary>
	public void SetActiveCollider(bool type)
	{
		enemy.SetWeponeActiveCollider(type);
	}

	/// <summary>
	/// タグの変更.
	/// </summary>
	public void ChangeTag()
	{
		gameObject.tag = "Death";
		gameObject.layer = LayerMask.NameToLayer("Death");
	}


	#region Collider
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			col.SendMessage("HitAttackName",
							TagUtility.getChildTagName(gameObject.transform.parent.tag),
							SendMessageOptions.DontRequireReceiver);

			//SetActiveCollider(false);

			col.gameObject.GetComponent<Player>().Damage(attackPoint, flipX);
		}
	}
	#endregion
}