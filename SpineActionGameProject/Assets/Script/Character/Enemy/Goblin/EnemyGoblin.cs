﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGoblin : MonoBehaviour
{
	[SerializeField]
	private Enemy enemy;
	[SerializeField]
	private LayerMask targetLayer;      // Linecastで判定するLayer.
	[SerializeField, Range(0, 15)]
	private float horizontalSearchRangeRadius = 9.0f;
	[SerializeField, Range(0, 15)]
	private float verticalSearchRangeRadius = 3.0f;

	private bool searchRange = false;           // 索敵結果(true:left, false:right).
	private bool leftSearchRange = false;       // 左側の索敵結果.
	private bool rightSearchRange = false;      // 右側の索敵結果.
	private bool attackRange = false;           // 攻撃範囲内かどうか.

	Enemy.StateTypes state;

	void Update()
	{
		state = enemy.EnemyState;

		if ((state & Enemy.StateTypes.death) == Enemy.StateTypes.death)
		{
			gameObject.tag = "Death";
			gameObject.layer = LayerMask.NameToLayer("Death");

			return;
		}

		SearchCast();

		if ((state & Enemy.StateTypes.attack) != Enemy.StateTypes.attack)
		{
			// 攻撃範囲内にPlayerがいたら.
			if (attackRange)
			{
				enemy.IsAttackRange = attackRange;
				enemy.Attack();
			}
			else
			{
				enemy.IsAttackRange = attackRange;

				// 左側索敵範囲内に反応があったら.
				if (leftSearchRange)
				{
					// キャラの向きを左にし、移動.
					searchRange = true;
					enemy.IsFlipX = searchRange;
					enemy.IsMove = true;
				}
				// 右側索敵範囲内に反応があったら.
				else if (rightSearchRange)
				{
					// キャラの向きを右にし、移動.
					searchRange = false;
					enemy.IsFlipX = searchRange;
					enemy.IsMove = true;
				}
				else
				{
					// 移動しない.
					enemy.IsMove = false;
				}
			}
		}
	}

	void SearchCast()
	{
		if ((state & Enemy.StateTypes.death) == Enemy.StateTypes.death)
		{
			return;
		}

		if (enemy.IsFlipX)
		{
			if ((state & Enemy.StateTypes.attack) != Enemy.StateTypes.attack)
			{
				leftSearchRange = Physics2D.Linecast(transform.position + transform.up * verticalSearchRangeRadius,
													transform.position - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
													targetLayer) ||
								Physics2D.Linecast(transform.position,
													transform.position - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
													targetLayer);

				rightSearchRange = Physics2D.Linecast(transform.position + transform.up * verticalSearchRangeRadius,
														transform.position + transform.right * horizontalSearchRangeRadius / 2 + transform.up * (verticalSearchRangeRadius / 2),
														targetLayer) ||
									Physics2D.Linecast(transform.position,
														transform.position + transform.right * horizontalSearchRangeRadius / 2 + transform.up * (verticalSearchRangeRadius / 2),
														targetLayer);
			}

			attackRange = Physics2D.Linecast(transform.position + (transform.up * 0.5f + transform.right * -0.5f),
												transform.position + (transform.up * 0.5f + transform.right * -2.0f),
												targetLayer) ||
							Physics2D.Linecast(transform.position + (transform.up + transform.right * -0.5f),
												transform.position + (transform.up + transform.right * -2.0f),
												targetLayer);

			#region Debug
			#region leftSearchRange
			Debug.DrawLine(transform.position + transform.up * verticalSearchRangeRadius,
							transform.position - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);
			Debug.DrawLine(transform.position,
							transform.position - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);
			#endregion
			#region rightSearchRange
			Debug.DrawLine(transform.position + transform.up * verticalSearchRangeRadius,
							transform.position + transform.right * horizontalSearchRangeRadius / 3 + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);
			Debug.DrawLine(transform.position,
							transform.position + transform.right * horizontalSearchRangeRadius / 3 + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);
			#endregion
			#region attackRange
			Debug.DrawLine(transform.position + (transform.up * 0.5f + transform.right * -0.5f),
							transform.position + (transform.up * 0.5f + transform.right * -2.0f),
							Color.green);
			Debug.DrawLine(transform.position + (transform.up + transform.right * -0.5f),
							transform.position + (transform.up + transform.right * -2.0f),
							Color.green);
			#endregion
			#endregion
		}
		else
		{
			if ((state & Enemy.StateTypes.attack) != Enemy.StateTypes.attack)
			{
				leftSearchRange = Physics2D.Linecast(transform.position + transform.up * verticalSearchRangeRadius,
														transform.position - transform.right * horizontalSearchRangeRadius / 2 + transform.up * (verticalSearchRangeRadius / 2),
														targetLayer) ||
								Physics2D.Linecast(transform.position,
													transform.position - transform.right * horizontalSearchRangeRadius / 2 + transform.up * (verticalSearchRangeRadius / 2),
													targetLayer);

				rightSearchRange = Physics2D.Linecast(transform.position + transform.up * verticalSearchRangeRadius,
														transform.position + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
														targetLayer) ||
									Physics2D.Linecast(transform.position,
														transform.position + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
														targetLayer);
			}

			attackRange = Physics2D.Linecast(transform.position + (transform.up * 0.5f + transform.right * 0.5f),
												transform.position + (transform.up * 0.5f + transform.right * 2.0f),
												targetLayer) ||
							Physics2D.Linecast(transform.position + (transform.up + transform.right * 0.5f),
												transform.position + (transform.up + transform.right * 2.0f),
												targetLayer);

			#region Debug
			Debug.DrawLine(transform.position + transform.up * verticalSearchRangeRadius,
							transform.position - transform.right * horizontalSearchRangeRadius / 3 + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);
			Debug.DrawLine(transform.position,
							transform.position - transform.right * horizontalSearchRangeRadius / 3 + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);

			Debug.DrawLine(transform.position + transform.up * verticalSearchRangeRadius,
							transform.position + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);
			Debug.DrawLine(transform.position,
							transform.position + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);

			Debug.DrawLine(transform.position + (transform.up * 0.5f + transform.right * 0.5f),
							transform.position + (transform.up * 0.5f + transform.right * 2.0f),
							Color.green);
			Debug.DrawLine(transform.position + (transform.up + transform.right * 0.5f),
							transform.position + (transform.up + transform.right * 2.0f),
							Color.green);
			#endregion
		}
	}
}