﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using System;
using Spine;

public class Enemy : MonoBehaviour
{
	#region Field
	[SerializeField, Range(0, 10)]
	private float moveSpeed = 4f;               // 移動速度.
	[SerializeField, Range(0, 30)]
	private float attckStepTimer = 1f;          // 攻撃するまでの時間.
	[SerializeField, Range(0, 5)]
	private float attackHitTime = 0.8f;         // 攻撃の判定をONにするまでの時間.
	[SerializeField, Range(0, 10)]
	private float invincibleTimer = 1f;         // 無敵時間.
	[SerializeField]
	private Vector2 blowOffPower = new Vector2(5, 5); // ダメージを食らった際に吹き飛ぶ力の設定.
	[SerializeField, Range(0, 5)]
	private int knockbackResistance = 0;        // ノックバック耐性.
	[SerializeField]
	private bool blowResist = false;            // ノックバックのキャンセル.
	[SerializeField, Range(0, 20)]
	private float flashingSpeed = 10;           // 点滅速度.
	[SerializeField, Range(0, 1)]
	private float flashingColor = 0.8f;         // 点滅時の色.
	[SerializeField]
	private LayerMask groundLayer;              // Linecastで地面を判定するLayer.
	[SerializeField]
	private LayerMask wallLayer;                // Linecastで壁を判定するLayer.

	[SerializeField]
	private new Rigidbody2D rigidbody2D;        // Rigidbidy2d;
	[SerializeField]
	private Animator animator;                  // Animator.
	[SerializeField]
	private EnemyWeapon weapon;                 // Weapon.
	[SerializeField]
	private EnemyLife enemyLife;                // Life.
	[SerializeField]
	private GameObject disappearanceEffect;     // 殺られエフェクト.

	private Player player;
	private Player.FlagTypes currentFlag;

	private GameObject effect;
	private Vector3 transPos;                   // transformPosition.
	private Vector2 movePos = Vector2.zero;     // 移動ポジション.

	private bool move = false;                  // 動くかどうか.
	private bool flipX = true;                  // 向き(true:左,false:右).
	private bool isGrounded = true;             // 着地判定.
	private bool isWall = false;                // 壁にぶつかったか.
	private bool attackRange = false;           // 攻撃範囲内かどうか.
	private bool attackDelay = false;           // 攻撃後の硬直時間.

	private int knockbackResistanceMax;         // ノックバック耐性値Max.

	private float attackAreaTimer;              // 攻撃範囲内にどのくらい居たか？
	private float nowLife = 0f;

	[Flags]
	public enum StateTypes
	{
		death = 0x01,       // 死んだか.
		run = 0x02,         // 走る.
		attack = 0x04,      // 攻撃.
		knockback = 0x08,   // ノックバック.
		invincible = 0x10,  // 無敵モード中.
		sex = 0x20,         // Hアニメ中.
		flag7 = 0x40,       // 0100 0000.
		flag8 = 0x80,       // 1000 0000.
	}
	private StateTypes state;

	#region Spineモーション.
	public SkeletonAnimation skeletonAnimation;
	[SpineAnimation("attack")]
	public string attackAnim = "attack";
	[SpineAnimation("damage")]
	public string fallAnim = "damage";
	[SpineAnimation("wait")]
	public string waitingAnim = "wait";
	[SpineAnimation("walk")]
	public string walkAnim = "walk";
	#endregion

	#region Get/Set
	public StateTypes EnemyState
	{
		get { return state; }
	}
	public bool IsFlipX
	{
		get { return flipX; }
		set { flipX = value; }
	}
	public bool IsMove
	{
		get { return move; }
		set { move = value; }
	}
	public bool IsGround
	{
		get { return isGrounded; }
	}
	public bool IsWall
	{
		get { return isWall; }
	}
	public bool IsAttackRange
	{
		set { attackRange = value; }
	}
	public bool IsBlowResist
	{
		set { blowResist = value; }
	}
	#endregion
	#endregion


	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		skeletonAnimation.Skeleton.FlipX = flipX;
		attackAreaTimer = 0;
		knockbackResistanceMax = knockbackResistance;
		OnWatingAnim();
	}

	void Update()
	{
		transPos = transform.position;

		SearchCast();

		currentFlag = player.CurrentFlag;

		if ((state & StateTypes.sex) == StateTypes.sex)
		{
			skeletonAnimation.skeleton.SetColor(new Color(0, 0, 0, 0));
		}
		else if ((state & StateTypes.invincible) != StateTypes.invincible)
		{
			skeletonAnimation.skeleton.SetColor(new Color(1, 1, 1, 1));
		}
		else
		{
			if ((state & StateTypes.death) != StateTypes.death)
			{
				// 点滅
				float level = Mathf.Abs(Mathf.Sin(Time.time * flashingSpeed));
				skeletonAnimation.skeleton.SetColor(new Color(flashingColor, flashingColor, flashingColor, level));
			}
			else
			{
				StartCoroutine(SkeletonColor());
			}
			// 武器の判定をOFFに.
			SetWeponeActiveCollider(false);
		}

		if (attackRange &&
			(player.CurrentFlag & Player.FlagTypes.invincible) == Player.FlagTypes.invincible ||
			(player.CurrentFlag & Player.FlagTypes.sex) == Player.FlagTypes.sex)
		{
			move = false;
		}
		else if ((player.CurrentFlag & Player.FlagTypes.stan) == Player.FlagTypes.stan)
		{
			move = true;
		}

		StartCoroutine(AliveCheck());
		AttackWaitTime();
		MoveCheck();

		skeletonAnimation.Skeleton.FlipX = flipX;

		if (isGrounded && !move &&
			(state & StateTypes.attack) != StateTypes.attack &&
			(state & StateTypes.death) != StateTypes.death &&
			(state & StateTypes.run) != StateTypes.run &&
			(state & StateTypes.knockback) != StateTypes.knockback &&
			skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != waitingAnim)
		{
			// 武器の判定をOFFに.
			SetWeponeActiveCollider(false);

			OnWatingAnim();
		}
	}

	void SearchCast()
	{
		// Linecastで足元に地面があるか判定.
		isGrounded = Physics2D.Linecast(transform.position + transform.up * 1,
										transform.position - transform.up * 0.05f,
										groundLayer);
		#region Debug
		Debug.DrawLine(transform.position + transform.up * 1,
					transform.position - transform.up * 0.05f,
					Color.green);
		#endregion

		if (flipX)
		{
			isWall = Physics2D.Linecast(transform.position + (transform.up * 0.5f + transform.right * -0.5f),
										transform.position + (transform.up * 0.5f + transform.right * -1.1f),
										wallLayer);

			#region Debug
			Debug.DrawLine(transform.position + (transform.up * 0.5f + transform.right * -0.5f),
							transform.position + (transform.up * 0.5f + transform.right * -1.1f),
							Color.red);

			#endregion
		}
		else
		{
			isWall = Physics2D.Linecast(transform.position + (transform.up * 0.5f + transform.right * 0.5f),
										transform.position + (transform.up * 0.5f + transform.right * 1.1f),
										wallLayer);
			#region Debug
			Debug.DrawLine(transform.position + (transform.up * 0.5f + transform.right * 0.5f),
							transform.position + (transform.up * 0.5f + transform.right * 1.1f),
							Color.red);
			#endregion
		}
	}

	#region 移動系.
	/// <summary>
	/// 移動処理.
	/// </summary>
	public void MoveCheck()
	{
		if (!move || !isGrounded || attackDelay ||
			(state & StateTypes.knockback) == StateTypes.knockback ||
			(state & StateTypes.death) == StateTypes.death ||
			(state & StateTypes.attack) == StateTypes.attack ||
			(state & StateTypes.sex) == StateTypes.sex)
		{
			return;
		}

		// 武器の判定をOFFに.
		SetWeponeActiveCollider(false);

		// 向きを変える.
		if (isWall)
		{
			iTween.Stop(gameObject);
			//flipX = !flipX;
		}

		if (flipX)
		{
			movePos.x = -moveSpeed * 0.5f;
		}
		else
		{
			movePos.x = moveSpeed * 0.5f;
		}

		if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != walkAnim)
		{
			OnWalkAnim();
		}

		movePos.y = rigidbody2D.velocity.y;
		rigidbody2D.velocity = movePos;
	}
	#endregion

	#region 攻撃系.
	/// <summary>
	/// 攻撃をする.
	/// </summary>
	public void Attack()
	{
		currentFlag = player.CurrentFlag;

		// Playerが無敵中でなければ.
		if ((currentFlag & Player.FlagTypes.invincible) != Player.FlagTypes.invincible &&
			(currentFlag & Player.FlagTypes.stan) != Player.FlagTypes.stan &&
			(currentFlag & Player.FlagTypes.sex) != Player.FlagTypes.sex &&
			(state & StateTypes.attack) != StateTypes.attack &&
			(state & StateTypes.knockback) != StateTypes.knockback &&
			!attackDelay)
		{
			if ((state & StateTypes.sex) != StateTypes.sex)
			{
				if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != attackAnim)
				{
					OnAttackAnim();
					state |= StateTypes.attack;
					// 武器の判定をONに.
					SetWeponeActiveCollider(true);

					attackAreaTimer = attckStepTimer;
					attackDelay = true;
				}
			}
			else
			{
				state &= ~StateTypes.sex;
			}
		}
		else if ((state & StateTypes.knockback) == StateTypes.knockback)
		{
			state &= ~StateTypes.attack;
			iTween.Stop(gameObject);
		}
	}

	void AttackWaitTime()
	{
		if (attackAreaTimer > 0)
		{
			attackAreaTimer -= Time.deltaTime;
		}
		else
		{
			attackDelay = false;
		}
	}
	#endregion

	#region ダメージ系.
	/// <summary>
	/// 攻撃を受けた.
	/// </summary>
	public void Damage()
	{
		if ((state & StateTypes.death) != StateTypes.death)
		{
			StartCoroutine(DamageAction());
		}
	}

	/// <summary>
	/// ダメージ処理.
	/// </summary>
	/// <returns></returns>
	public IEnumerator DamageAction()
	{
		state |= StateTypes.invincible;

		attackAreaTimer = attckStepTimer;
		nowLife = enemyLife.IsNowLife;

		skeletonAnimation.AnimationState.TimeScale = 0.5f;
		yield return null;
		skeletonAnimation.AnimationState.TimeScale = 1.0f;

		StartCoroutine(knockbackCorutine());

		state &= ~StateTypes.invincible;
	}

	/// <summary>
	/// ノックバック処理.
	/// </summary>
	/// <returns></returns>
	IEnumerator knockbackCorutine()
	{
		if (!blowResist)
		{
			if (knockbackResistance > 0)
			{
				knockbackResistance--;
			}
			else if (knockbackResistance <= 0)
			{
				state = StateTypes.knockback;

				iTween.Stop(gameObject);

				// Y軸が移動中でなければノックバック.
				if (rigidbody2D.velocity.y == 0)
				{
					if (player.IsFlipX)
					{
						//rigidbody2D.velocity += new Vector2(-blowOffPower.x, blowOffPower.y);
						//rigidbody2D.AddForce(new Vector2(-blowOffPower.x, blowOffPower.y), ForceMode2D.Impulse);

						// ノックバック
						iTween.MoveAdd(gameObject, iTween.Hash(
							"x", -blowOffPower.x,
							"y", blowOffPower.y,
							"time", 1f,
							"oncomplete", "WaitInvincibleCoroutine"
						));
					}
					else
					{
						//rigidbody2D.velocity += new Vector2(blowOffPower.x, blowOffPower.y);
						//rigidbody2D.AddForce(new Vector2(blowOffPower.x, blowOffPower.y), ForceMode2D.Impulse);

						// ノックバック
						iTween.MoveAdd(gameObject, iTween.Hash(
							"x", blowOffPower.x,
							"y", blowOffPower.y,
							"time", 1f,
							"oncomplete", "WaitInvincibleCoroutine"
						));
					}
				}

				if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != fallAnim)
				{
					OnFallAnim();
				}
				knockbackResistance = knockbackResistanceMax;
			}
			yield return new WaitForSeconds(invincibleTimer);
		}
		state &= ~StateTypes.knockback;
	}
	#endregion

	#region 武器Collision系.
	/// <summary>
	/// 武器コリジョン操作.
	/// </summary>
	/// <param name="type"></param>
	public void SetWeponeActiveCollider(bool type)
	{
		animator.SetBool("Attack", type);
	}
	#endregion

	#region 生死判定.
	/// <summary>
	/// 生きているかのチェック.
	/// </summary>
	/// <returns></returns>
	IEnumerator AliveCheck()
	{
		nowLife = enemyLife.IsNowLife;

		if (nowLife <= 0)
		{
			state |= (StateTypes.death | StateTypes.invincible);
			move = false;
			gameObject.tag = "Death";
			gameObject.layer = LayerMask.NameToLayer("Death");
			weapon.ChangeTag();
			yield return new WaitForSeconds(0.5f);

			if (effect == null)
			{
				effect = Instantiate(disappearanceEffect, transform.position, transform.rotation) as GameObject;
				effect.transform.position = new Vector3(transPos.x, transPos.y, -3f);
			}

			Destroy(effect, 0.5f);
			Destroy(gameObject);
		}
	}
	#endregion

	#region skeletonのカラー.
	IEnumerator SkeletonColor()
	{
		for (int i = 255; i > 0; i -= 5)
		{
			skeletonAnimation.skeleton.SetColor(new Color((float)0 / 255, (float)0 / 255, (float)0 / 255, (float)i / 255));
			yield return 0;
		}
	}
	#endregion

	#region SpineAnimation
	// 攻撃モーション.
	public void OnAttackAnim()
	{
		skeletonAnimation.AnimationState.SetAnimation(0, attackAnim, false);
		skeletonAnimation.AnimationState.Complete += PlayIdleAnimation;
	}

	// ノックバックモーション.
	public void OnFallAnim()
	{
		skeletonAnimation.AnimationState.SetAnimation(0, fallAnim, false);

		// 武器の判定をOFFに.
		SetWeponeActiveCollider(false);
	}

	// 待機モーション.
	public void OnWatingAnim()
	{
		skeletonAnimation.AnimationState.SetAnimation(0, waitingAnim, true);
	}

	// 走るモーション.
	public void OnRunAnim()
	{
		//skeletonAnimation.AnimationState.SetAnimation(0, runAnim, true);
	}

	// 歩くモーション.
	public void OnWalkAnim()
	{
		skeletonAnimation.AnimationState.SetAnimation(0, walkAnim, true);
	}

	// 攻撃モーションが終わったら待機モーションに移る.
	public void PlayIdleAnimation(TrackEntry trackEntry)
	{
		if (trackEntry.animation.Name == attackAnim)
		{
			skeletonAnimation.AnimationState.SetAnimation(0, waitingAnim, true);
			skeletonAnimation.AnimationState.Complete -= PlayIdleAnimation;

			// 武器の判定をOFFに.
			SetWeponeActiveCollider(false);
			state &= ~StateTypes.attack;
		}
	}
	#endregion


	#region Collider2D
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "DeathPoint")
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != fallAnim)
			{
				OnFallAnim();
			}
			Destroy(gameObject, 3f);
		}

		if (col.gameObject.tag == "PlayerWeapon" || col.gameObject.tag == "Knife")
		{
			Damage();
		}

		if (col.gameObject.tag == "NeedleWall")
		{
			if (skeletonAnimation.AnimationState.GetCurrent(0).Animation.name != fallAnim)
			{
				OnFallAnim();
			}
			enemyLife.LifeDown(enemyLife.IsMaxLife);
		}
	}

	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			currentFlag = player.CurrentFlag;

			if ((currentFlag & Player.FlagTypes.invincible) != Player.FlagTypes.invincible &&
				(currentFlag & Player.FlagTypes.stan) == Player.FlagTypes.stan)
			{
				col.SendMessage("HitAttackName",
					TagUtility.getChildTagName(gameObject.tag),
					SendMessageOptions.DontRequireReceiver);
				player.Damage(0, flipX);

				state = StateTypes.sex;
			}
		}
	}
	#endregion
}