﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOrc : MonoBehaviour
{
	[SerializeField]
	private Enemy enemy;
	[SerializeField]
	private LayerMask targetLayer;      // Linecastで判定するLayer.
	[SerializeField, Range(0, 15)]
	private float horizontalSearchRangeRadius = 6.0f;
	[SerializeField, Range(0, 15)]
	private float verticalSearchRangeRadius = 3.0f;
	[SerializeField]
	private bool iTweenMove;

	private bool searchRange = false;           // 索敵結果(true:left, false:right).
	private bool leftSearchRange = false;       // 左側の索敵結果.
	private bool rightSearchRange = false;      // 右側の索敵結果.
	private bool attackRange = false;           // 攻撃範囲内かどうか.

	Enemy.StateTypes state;

	void Update()
	{
		state = enemy.EnemyState;

		if ((state & Enemy.StateTypes.death) == Enemy.StateTypes.death)
		{
			gameObject.tag = "Death";
			gameObject.layer = LayerMask.NameToLayer("Death");

			return;
		}

		SearchCast();

		if ((state & Enemy.StateTypes.attack) != Enemy.StateTypes.attack)
		{
			if (attackRange)
			{
				enemy.IsAttackRange = attackRange;
				enemy.Attack();
			}
			else
			{
				enemy.IsAttackRange = attackRange;

				if (leftSearchRange)
				{
					searchRange = true;
					enemy.IsFlipX = searchRange;
					enemy.IsMove = true;
				}
				else if (rightSearchRange)
				{
					searchRange = false;
					enemy.IsFlipX = searchRange;
					enemy.IsMove = true;
				}
				else
				{
					enemy.IsMove = false;
				}
			}
		}
		else if (iTweenMove && !enemy.IsWall)
		{
			AttackMove();
		}
	}

	void SearchCast()
	{
		if ((state & Enemy.StateTypes.death) == Enemy.StateTypes.death)
		{
			return;
		}

		if (enemy.IsFlipX)
		{
			if ((state & Enemy.StateTypes.attack) != Enemy.StateTypes.attack)
			{
				leftSearchRange = Physics2D.Linecast(transform.position + transform.up * verticalSearchRangeRadius,
													transform.position - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
													targetLayer) ||
								Physics2D.Linecast(transform.position,
													transform.position - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
													targetLayer);

				rightSearchRange = Physics2D.Linecast(transform.position + transform.up * verticalSearchRangeRadius,
														transform.position + transform.right * horizontalSearchRangeRadius / 2 + transform.up * (verticalSearchRangeRadius / 2),
														targetLayer) ||
									Physics2D.Linecast(transform.position,
														transform.position + transform.right * horizontalSearchRangeRadius / 2 + transform.up * (verticalSearchRangeRadius / 2),
														targetLayer);
			}

			attackRange = Physics2D.Linecast(transform.position + (transform.up * 0.5f + transform.right * -1.0f),
												transform.position + (transform.up * 0.5f + transform.right * -3.0f),
												targetLayer) ||
							Physics2D.Linecast(transform.position + (transform.up + transform.right * -1.0f),
												transform.position + (transform.up + transform.right * -3.0f),
												targetLayer);

			#region Debug
			Debug.DrawLine(transform.position + transform.up * verticalSearchRangeRadius,
							transform.position - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);
			Debug.DrawLine(transform.position,
							transform.position - transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);

			Debug.DrawLine(transform.position + transform.up * verticalSearchRangeRadius,
							transform.position + transform.right * horizontalSearchRangeRadius / 3 + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);
			Debug.DrawLine(transform.position,
							transform.position + transform.right * horizontalSearchRangeRadius / 3 + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);

			Debug.DrawLine(transform.position + (transform.up * 0.5f + transform.right * -1.0f),
							transform.position + (transform.up * 0.5f + transform.right * -3.0f),
							Color.green);
			Debug.DrawLine(transform.position + (transform.up + transform.right * -1.0f),
							transform.position + (transform.up + transform.right * -3.0f),
							Color.green);
			#endregion
		}
		else
		{
			if ((state & Enemy.StateTypes.attack) != Enemy.StateTypes.attack)
			{
				leftSearchRange = Physics2D.Linecast(transform.position + transform.up * verticalSearchRangeRadius,
														transform.position - transform.right * horizontalSearchRangeRadius / 2 + transform.up * (verticalSearchRangeRadius / 2),
														targetLayer) ||
								Physics2D.Linecast(transform.position,
													transform.position - transform.right * horizontalSearchRangeRadius / 2 + transform.up * (verticalSearchRangeRadius / 2),
													targetLayer);

				rightSearchRange = Physics2D.Linecast(transform.position + transform.up * verticalSearchRangeRadius,
														transform.position + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
														targetLayer) ||
									Physics2D.Linecast(transform.position,
														transform.position + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
														targetLayer);
			}

			attackRange = Physics2D.Linecast(transform.position + (transform.up * 0.5f + transform.right * 1.0f),
												transform.position + (transform.up * 0.5f + transform.right * 3.0f),
												targetLayer) ||
							Physics2D.Linecast(transform.position + (transform.up + transform.right * 1.0f),
												transform.position + (transform.up + transform.right * 3.0f),
												targetLayer);

			#region Debug
			Debug.DrawLine(transform.position + transform.up * verticalSearchRangeRadius,
							transform.position - transform.right * horizontalSearchRangeRadius / 3 + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);
			Debug.DrawLine(transform.position,
							transform.position - transform.right * horizontalSearchRangeRadius / 3 + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);

			Debug.DrawLine(transform.position + transform.up * verticalSearchRangeRadius,
							transform.position + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);
			Debug.DrawLine(transform.position,
							transform.position + transform.right * horizontalSearchRangeRadius + transform.up * (verticalSearchRangeRadius / 2),
							Color.red);

			Debug.DrawLine(transform.position + (transform.up * 0.5f + transform.right * 1.0f),
							transform.position + (transform.up * 0.5f + transform.right * 3.0f),
							Color.green);
			Debug.DrawLine(transform.position + (transform.up + transform.right * 1.0f),
							transform.position + (transform.up + transform.right * 3.0f),
							Color.green);
			#endregion
		}
	}

	void AttackMove()
	{
		GameObject parent = gameObject.transform.parent.gameObject;

		if (!enemy.IsFlipX)
		{
			iTween.MoveAdd(parent, iTween.Hash("x", 3.0f,
													"time", 0.8f));
		}
		else
		{
			iTween.MoveAdd(parent, iTween.Hash("x", -3.0f,
										"time", 0.8f));
		}
	}
}