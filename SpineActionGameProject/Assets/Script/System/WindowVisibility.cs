﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowVisibility : MonoBehaviour
{
	[SerializeField]
	private GameObject textWindow;
	[SerializeField]
	private TextController textController;


	void Update()
	{
		if (Input.GetMouseButtonDown(1))
		{
			WindowActivate();
		}
	}

	public void WindowActivate()
	{
		if (textWindow.activeSelf)
		{
			textWindow.SetActive(false);
			textController.AutoOff();
			textController.SkipOff();
		}
		else
		{
			textWindow.SetActive(true);
		}
	}
}