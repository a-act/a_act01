﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextArea : MonoBehaviour
{
	EnemyManager enemyManager;
	InputManager inputManager;

	Vector3 nextSpawnPos;
	Vector3 addPos;
	GameObject cameraCollision;
	GameObject playerObject;

	bool waitInput;
	bool doorLock;

	public void SetData(Vector3 pos, float x, bool key)
	{
		nextSpawnPos = new Vector3(pos.x, pos.y, -3f);
		addPos = new Vector3(x, 0f, 0f);
		doorLock = key;
	}

	private void Start()
	{
		cameraCollision = GameObject.FindGameObjectWithTag("CameraCol");
		playerObject = GameObject.FindGameObjectWithTag("Player");
		enemyManager = GameObject.Find("Manager/StageManager").GetComponent<EnemyManager>();
		inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
	}

	IEnumerator ChangeArea()
	{
		waitInput = true;

		FadeManager.Instance.FadeInOut(true, 0.5f);
		yield return new WaitForSeconds(0.1f);

		yield return new WaitUntil(() => ChangeNextArea());

		yield return new WaitForSeconds(0.2f);
		FadeManager.Instance.FadeInOut(false, 0.5f);

		enemyManager.RespawnEnemy();
		waitInput = false;
	}

	bool ChangeNextArea()
	{
		if (playerObject.transform.position != nextSpawnPos)
		{
			playerObject.transform.position = nextSpawnPos;
		}

		cameraCollision.transform.position += addPos;
		return true;
	}


	#region Trigger2D
	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			// ドアに鍵がかかっていなければ.
			if (!doorLock)
			{
				var input = inputManager.DirectionalInput;
				if (!waitInput && input.y == 1)
				{
					StartCoroutine(ChangeArea());
				}
			}
			else
			{
				if (col.transform.Find("ItemsPouch").GetComponent<ItemsPouch>().IsBossOrcDoorKey)
				{
					doorLock = false;
				}
			}
		}
	}
	#endregion
}