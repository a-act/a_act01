﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ActivateUIObj : MonoBehaviour
{
	public KeyCode openKey = KeyCode.Escape;
	[SerializeField]
	private GameObject windowObj;    // 画面UI.
	[SerializeField]
	private ActivateButton select;		// ボタンのインタラクティブに関する処理が書かれているスクリプト.

	void Update()
	{
		// キーを押したら画面UIのオン・オフ.
		if (Input.GetKeyDown(openKey))
		{
			windowObj.SetActive(!windowObj.activeSelf);

			// 画面を開いた時にBackground1のボタンのインタラクティブをtrue、Background2のボタンのインタラクティブをfalseにする.
			if (windowObj.activeSelf)
			{
				select.ActivateOrNotActivate(true);
			}
			// 画面を閉じたら選択を解除.
			else
			{
				EventSystem.current.SetSelectedGameObject(null);
			}
		}
	}
}