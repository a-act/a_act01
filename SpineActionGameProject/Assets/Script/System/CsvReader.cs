﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CsvReader : MonoBehaviour
{
	[Tooltip("マップデータ(CSV形式)")]
	public TextAsset csvFile;           // CSVファイルを読み込むために使う.

	string str = "";                    // CSVの全文文字を保存する.
	string strget = "";                 // 取り出した文字を保存する.

	[SerializeField]
	[Tooltip("CSVデータの行数")]
	private int rows = 15;              // CSVデータの行数.
	[Tooltip("CSVデータの列数")]
	private int columns = 3;			// CSVデータの列数.

	int[,] csvData = new int[20, 20];	// マップ番号を格納するマップ用変数.
	int[] iDat = new int[15];			// 文字検索用.

	int a = 0;                          // 濫用 数値型変数.
	int b = 0;                          // 濫用 数値型変数.
	int c = 0;                          // 濫用 数値型変数.

	void Start()
	{
		// マップ番号を格納するマップ用変数.
		csvData = new int[rows, columns];

		#region ここでCSVデータをstrに保存.
		//csvFile = Resources.Load("Map/MapChip/textMap01") as TextAsset;
		if (csvFile == null)
		{
			Debug.LogError("There is no CSVFile!!");
			return;
		}
		StringReader reader = new StringReader(csvFile.text);

		while (reader.Peek() > -1)
		{
			string line = reader.ReadLine();
			// 最後に検索文字列の","を追記。 これがないと最後の文字を取りこぼす.
			str += ",";
			str += line;
		}

		// 最後に検索文字列の","を追記。 これがないと最後の文字を取りこぼす.
		str += ",";
		#endregion

		#region ここでCSVデータを配列変数csvDataに保存.
		for (int c = 0; c < rows; c++)
		{
			for (int i = 0; i < columns; i++)
			{
				try
				{
					// ","を検索.
					iDat[0] = str.IndexOf(",", iDat[0]);
				}
				catch
				{
					break;
				}

				try
				{
					// 次の","を検索.
					iDat[1] = str.IndexOf(",", iDat[0] + 1);
				}
				catch
				{
					break;
				}

				// 何文字取り出すか決定.
				iDat[2] = iDat[1] - iDat[0] - 1;

				try
				{
					// iDat[2]文字ぶんだけ取り出す.
					strget = str.Substring(iDat[0] + 1, iDat[2]);
				}
				catch
				{
					break;
				}

				try
				{
					// 取り出した文字列を数値型に変換.
					iDat[3] = int.Parse(strget);
				}
				catch
				{
					break;
				}

				// マップ用変数に保存。1とか6とか数字が入るよ.
				csvData[a, b] = iDat[3];
				// 一つ右のマップ用変数へ.
				b++;
				// 次のインデックスへ.
				iDat[0]++;
			}

			// 一つ下のマップチップへ.
			a++;
			// マップチップ格納を一番左に戻す.
			b = 0;
		}
		#endregion
	}

}