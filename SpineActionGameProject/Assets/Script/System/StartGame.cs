﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
	[SerializeField]
	Volume volume;

	void Start()
	{
		volume.LoadOption();

		//シーン切替 .
		SceneManager.LoadScene(Constants.SCENE_NAME.Title.ToString());
	}
}