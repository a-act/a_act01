﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObject : MonoBehaviour
{
	[SerializeField]
	GameObject gameObj;

	public void OnActivateObj()
	{
		if (gameObj.activeSelf)
		{
			gameObj.SetActive(false);
		}
		else
		{
			gameObj.SetActive(true);
		}
	}
}