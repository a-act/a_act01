﻿using UnityEngine;
using System.Collections;

/*
 セーブデータはpublic又は[SerializeField]されたデータしか保存できない.
*/
[System.Serializable]
public class OptionData : object
{
	#region Field
	[SerializeField, Range(0, 1)]
	private float timeApplyInDisplay = 0.05f;	// テキストの表示速度.
	[SerializeField, Range(0, 5)]
	private float autoSpeed = 2f;				// オート時のテキスト再生スピード.
	[SerializeField, Range(-40, 0)]
	private float masterValue;					// 全体ボリューム.
	[SerializeField, Range(-40, 0)]
	private float bgmValue;						// BGMボリューム.
	[SerializeField, Range(-40, 0)]
	private float seValue;						// SEボリューム.
	[SerializeField, Range(-40, 0)]
	private float hseValue;						// HSEボリューム.
	[SerializeField, Range(-40, 0)]
	private float voiceValue;					// Voiceボリューム.

	#region get/set
	public float IsTimeApplyInDisplay
	{
		get { return timeApplyInDisplay; }
		set { timeApplyInDisplay = value; }
	}
	public float IsAutoSpeed
	{
		get { return autoSpeed; }
		set { autoSpeed = value; }
	}
	public float IsMasterValue
	{
		get { return masterValue; }
		set { masterValue = value; }
	}
	public float IsBGMValue
	{
		get { return bgmValue; }
		set { bgmValue = value; }
	}
	public float IsSEValue
	{
		get { return seValue; }
		set { seValue = value; }
	}
	public float IsHSEValue
	{
		get { return hseValue; }
		set { hseValue = value; }
	}
	public float IsVoiceValue
	{
		get { return voiceValue; }
		set { voiceValue = value; }
	}
	#endregion get/set
	#endregion Field


	/// <summary>
	/// ログをセット.
	/// </summary>
	/// <returns></returns>
	public string GetNormalData()
	{
		string log;
		log = "timeApplyInDisplay:" + timeApplyInDisplay + ", " +
				"autoSpeed:" + autoSpeed + ", " +
				"masterValue: " + masterValue + ", " +
				"bgmValue: " + bgmValue + ", " +
				"seValue: " + seValue + ", " +
				"hseValue: " + hseValue + ", " +
				"voiceValue: " + voiceValue;
		return log;
	}

	/// <summary>
	/// JsonDataの取得.
	/// </summary>
	/// <returns></returns>
	public string GetJsonData()
	{
		return JsonUtility.ToJson(this);
	}

	/// <summary>
	/// データの初期化.
	/// </summary>
	public void ResetValue()
	{
		timeApplyInDisplay = 0.05f;
		autoSpeed = 2f;
		masterValue = 0f;
		bgmValue = 0f;
		seValue = 0f;
		hseValue = 0f;
		voiceValue = 0f;
	}
}