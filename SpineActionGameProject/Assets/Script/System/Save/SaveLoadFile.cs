﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class SaveLoadFile : MonoBehaviour
{
	#region Field
	public SaveData saveData = new SaveData();  // データを生成し保持しているスクリプト.
	public SaveData.SexCount sexCount = new SaveData.SexCount();  // データを生成し保持しているスクリプト.

	[SerializeField, Tooltip("どのキーでメニューを開くか")]
	private KeyCode openMenu = KeyCode.Escape;
	[SerializeField]
	private GameObject menuWindow;              // 停止中に表示する画面.
	[SerializeField]
	private Text dataText;                      // データ表示テキスト.
	[SerializeField]
	private BGMManager bgmManager;

	#region 入力(InputField).
	//[SerializeField]
	//private InputField nameField;
	#endregion

	private Player player;

	private GameObject cameraCollision = null;

	private bool sceneContinu = false;
	#region Get/Set
	public Vector3 IsPlayerPos
	{
		get { return saveData.IsPlayerPos; }
		set { saveData.IsPlayerPos = value; }
	}
	public Vector2 IsSavePointPos
	{
		get { return saveData.IsSavePoint; }
		set { saveData.IsSavePoint = value; }
	}
	public Constants.SCENE_NAME IsStageNumber
	{
		get { return saveData.IsStageNumber; }
		set { saveData.IsStageNumber = value; }
	}
	public Vector3 IsCameraCollisionPos
	{
		get { return saveData.IsCameraCollisionPos; }
		set { saveData.IsCameraCollisionPos = value; }
	}
	#endregion
	#endregion


	void Update()
	{
		if (SceneManager.GetActiveScene().name != Constants.SCENE_NAME.Title.ToString())
		{
			if (SceneManager.GetActiveScene().name != "Main" &&
				SceneManager.GetActiveScene().name != Constants.SCENE_NAME.CG.ToString())
			{
				if (player == null && cameraCollision == null)
				{
					player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
					cameraCollision = GameObject.FindGameObjectWithTag("CameraCol");

					if (sceneContinu)
					{
						player.InitToContinue(saveData.IsPlayerPos);
						cameraCollision.transform.position = saveData.IsCameraCollisionPos;
						sceneContinu = false;
					}
				}
			}

			if (Debug.isDebugBuild && Input.GetKeyDown(openMenu))
			{
				if (menuWindow.activeSelf)
				{
					menuWindow.SetActive(false);

				}
				else
				{
					menuWindow.SetActive(true);
				}
			}
		}
	}

	/// <summary>
	/// データ表示のテキストを空にする.
	/// </summary>
	public void ResetText()
	{
		dataText.text = "";
	}

	/// <summary>
	/// 現在のオブジェクトの変数のデータを表示する.
	/// </summary>
	public void ShowParameter()
	{
		ResetText();
		dataText.text = saveData.GetNormalData();
	}

	/// <summary>
	/// 現在のオブジェクトのJSONデータを表示.
	/// </summary>
	public void ShowJsonData()
	{
		ResetText();
		dataText.text = saveData.GetJsonData();
	}

	/// <summary>
	/// コンティニュー.
	/// </summary>
	public void ToContinue()
	{
		var sceneName = Constants.SCENE_NAME.Stage01.ToString();
		if (SceneManager.GetActiveScene().name == Constants.SCENE_NAME.CG.ToString())
		{
			// シーン切替.
			FadeManager.Instance.LoadScene(sceneName, 1.0f);
			sceneContinu = true;
		}
		else
		{
			//saveData.ResetPosValue();
			player.InitToContinue(saveData.IsPlayerPos);
			cameraCollision.transform.position = saveData.IsCameraCollisionPos;
		}
		bgmManager.StopBGM();
	}

	/// <summary>
	/// SaveDataの値をJsonにしてログに出力する.
	/// </summary>
	public void ShowDebugLog()
	{
		Debug.Log(JsonUtility.ToJson(saveData));
	}

	/// <summary>
	/// 新しくデータを作成.
	/// </summary>
	public void CreateNewData()
	{

	}

	/// <summary>
	/// データを削除.
	/// </summary>
	public void DeleteData()
	{
		saveData = null;
	}

	/// <summary>
	/// SaveDataからの値をJsonに書き込む.
	/// </summary>
	public void Save()
	{
		ResetText();
		//SetData();

		if (saveData != null)
		{
			if (XJ.Unity3D.IO.FileReadWriter.WriteFileToAssets
				(Constants.SAVEDATA_PATH, JsonUtility.ToJson(saveData)) == false)
			{
				// セーブ(ファイルの書き込み)に失敗したら.
				dataText.text = "ファイルの書き込みに失敗しました。";
				Debug.Log("File Write Failed.");
			}
			else
			{
				// セーブ(ファイルの書き込み)が完了したら.
				dataText.text = "セーブ完了！";
				Debug.Log("Save completed.");
			}
		}
		else
		{
			dataText.text = "セーブするデータがありません！";
			Debug.Log("There is no data to save.");
		}
	}

	/// <summary>
	/// Jsonから値を読み込み、saveDataに設定する.
	/// </summary>
	public void Load()
	{
		try
		{
			ResetText();
			saveData = null;
			saveData = JsonUtility.FromJson<SaveData>
					(XJ.Unity3D.IO.FileReadWriter.ReadFileFromAssets(Constants.SAVEDATA_PATH));
		}
		catch
		{
			dataText.text = "エラー：セーブデータがないか、正常にロードができませんでした。";
			Debug.Log("error：There was no saved data or loading was not successful.");
			/*ここでセーブデータ作成処理 or データがないのでロード処理の中断をする*/
			//saveData.IsStageNumber = Constants.SCENE_NAME.Title;
		}
		finally
		{
			if (saveData != null)
			{
				//GetDataCoroutine();

				dataText.text = "ロード完了！";
				Debug.Log("Load complete!");
			}
		}
	}

	public void ToTitle()
	{
		// タイトル画面に戻す.
		FadeManager.Instance.LoadScene(Constants.SCENE_NAME.Title.ToString(), 1.0f);
	}

	public void ToExit()
	{
#if UNITY_EDITOR
		EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
		Application.Quit();
#endif
	}

	/// <summary>
	/// データを記録.
	/// </summary>
	public void SetData()
	{
		saveData.IsPlayerPos = player.IsPos;
		//saveData.IsSavePoint = savePointPos;
		//saveData.IsStageNumber = stageNumber;
	}

	/// <summary>
	/// データを読込.
	/// </summary>
	void GetData()
	{
		player.IsPos = saveData.IsPlayerPos;
		player.playerLife.ResetLife();
	}

	/// <summary>
	/// ステージ遷移のための処理.
	/// </summary>
	public void SetPlayerDataStageChange()
	{
		saveData.ResetPosValue();
		//SetData();
	}

	/// <summary>
	/// データの初期化.
	/// </summary>
	public void ResetData()
	{
		saveData.ResetValue();
	}
}