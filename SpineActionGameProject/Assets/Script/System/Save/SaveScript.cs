﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveScript : MonoBehaviour
{
	[SerializeField]
	private Constants.SCENE_NAME stageNumber;

	SaveLoadFile saveManager;
	GameObject cameraCollision;
	BossDespong bossDespong;

	private void Start()
	{
		saveManager = GameObject.Find("Manager/SaveManager").GetComponent<SaveLoadFile>();
		cameraCollision = GameObject.FindGameObjectWithTag("CameraCol");
		bossDespong = GameObject.Find("Stage/BossDespong").GetComponent<BossDespong>();
	}

	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			//saveManager.DataSave(col.gameObject.transform.position);
			saveManager.IsPlayerPos = new Vector3(col.gameObject.transform.position.x, col.gameObject.transform.position.y, -3f);
			saveManager.IsSavePointPos = transform.position;
			saveManager.IsStageNumber = stageNumber;
			saveManager.IsCameraCollisionPos = cameraCollision.transform.position;
			saveManager.Save();

			if (bossDespong.gameObject.activeSelf)
			{
				bossDespong.BossOrcDelete();
			}
		}

		if (Debug.isDebugBuild &&
			col.gameObject.tag == "PlayerWeapon")
		{
			//saveManager.DataDelete();
			saveManager.ResetData();
			saveManager.Save();
		}
	}
}