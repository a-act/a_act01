﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System;

namespace XJ.Unity3D.IO
{
	/// <summary>
	/// Unityでファイルを読み書きするための機能.
	/// </summary>
	public static class FileReadWriter
	{
		#region Json
		/// <summary>
		/// 指定したオブジェクトをJsonファイルに出力します。
		/// ファイルは"T.json"としてStreamingAssetsに出力されます。
		/// </summary>
		/// <typeparam name="T">
		/// Jsonに出力するオブジェクトのタイプ.
		/// Sampleタイプのとき、~StreamingAssets/Sample.jsonファイルを出力します。
		/// </typeparam>
		/// <param name="obj">
		/// Jsonに出力するオブジェクト.
		/// </param>
		/// <returns>
		/// 出力に成功するとtrue, 失敗するとfalse.
		/// </returns>
		public static bool WriteJsonToStreamingAssets<T>(T obj)
		{
			return WriteFileToStreamingAssets(obj.GetType().Name + ".json", JsonUtility.ToJson(obj));
		}

		/// <summary>
		/// StreamingAssetsから"T.json"ファイルを読み込み、
		/// タイプ T の新しいインスタンスとして取得します。
		/// </summary>
		/// <typeparam name="T">
		/// Jsonを読み込み、新しいインスタンスを生成するタイプ。
		/// Sampleタイプのとき、~streamingAssets/Sample.jsonから読み込みます。
		/// </typeparam>
		/// <returns>
		/// Jsonから読み込んだ値が設定されたタイプ T のインスタンス。
		/// </returns>
		public static T ReadJsonFromStreamingAssets<T>()
		{
			return JsonUtility.FromJson<T>(ReadFileFromStreamingAssets(typeof(T).Name + ".json"));
		}

		/// <summary>
		/// StramingAssetsから"T.json"ファイルを読み込み、
		/// 指定したインスタンスに読み込んだ結果を上書きします。
		/// </summary>
		/// <typeparam name="T">
		/// Jsonを読み込み、その値を上書きするインスタンスのタイプ。
		/// Sampleタイプのとき、~StreamingAssets/Sample.jsonから読み込みます。
		/// </typeparam>
		/// <param name="obj">
		/// Jsonを読み込み、その値を上書きするインスタンス。
		/// </param>
		public static void ReadJsonFormStreamingAssets<T>(T obj)
		{
			JsonUtility.FromJsonOverwrite(ReadFileFromStreamingAssets(typeof(T).Name + ".json"), obj);
		}
		#endregion

		#region StreaamingAssets
		/// <summary>
		/// 指定した内容をUTF-8形式でファイルに書き込みます。ファイルの内容は上書きされます。
		/// </summary>
		/// <param name="relativeFilePath">
		/// ~StramingAssets以降のファイルパス.
		/// Sample.jsonとするとき、~StreamingAssets/Sample.jsonファイルになります。
		/// </param>
		/// <param name="content">
		/// ファイルに書き込む内容.
		/// </param>
		/// <returns>
		/// 書き込みに成功するとtrue, 失敗するとfalse.
		/// </returns>
		public static bool WriteFileToStreamingAssets(string relativeFilePath, string content)
		{
			relativeFilePath = OptimizeRelativeFilePath(relativeFilePath);

			return XJ.NET.IO.FileReadWriter.WriteFile
					(Application.dataPath + "/StreamingAssets" + relativeFilePath,
					content,
					Encoding.UTF8);
		}

		/// <summary>
		/// ファイルの内容をUTF-8で読み取り、文字列として取得します。
		/// ファイルパスはStreamingAssetsディレクトリから始まる文字列で指定します。
		/// </summary>
		/// <param name="relativeFilePath">
		/// ~StreamingAssets以降のファイルパス.
		/// Sample.jsonとするとき、~StramingAssets/Sample.jsonファイルになります。
		/// </param>
		/// <returns>
		/// 読み取った文字列。例外が起きると、null.
		/// </returns>
		public static string ReadFileFromStreamingAssets(string relativeFilePath)
		{
			relativeFilePath = OptimizeRelativeFilePath(relativeFilePath);

			return XJ.NET.IO.FileReadWriter.ReadFile
					(Application.dataPath + "/StreamingAssets" + relativeFilePath,
					Encoding.UTF8);
		}
		#endregion

		#region Assets
		/// <summary>
		/// 指定した内容をUTF-8形式でファイルに書き込みます。
		/// ファイルの内容は上書きされます。
		/// </summary>
		/// <param name="relativeFilePath">
		/// ~Assets以降のファイルパス.
		/// Sample.jsonとするとき、~Assets/Sample.jsonファイルになります。
		/// </param>
		/// <param name="content">
		/// ファイルに書き込む内容.
		/// </param>
		/// <returns>
		/// 書き込みに成功するとtrue, 失敗するとfalse.
		/// </returns>
		public static bool WriteFileToAssets(string relativeFilePath, string content)
		{
			relativeFilePath = OptimizeRelativeFilePath(relativeFilePath);

			return XJ.NET.IO.FileReadWriter.WriteFile(Application.dataPath + relativeFilePath, content, Encoding.UTF8);
		}

		/// <summary>
		/// ファイルの内容をUTF-8で読み取り、文字列として取得します。
		/// </summary>
		/// <param name="relativeFilePath">
		/// ~Assets以降のファイルパス。Sample.Jsonとするとき、~Assets/Sample.jsonファイルになります。
		/// </param>
		/// <returns>
		/// 読み取った文字列。例外が起きると、null.
		/// </returns>
		public static string ReadFileFromAssets(string relativeFilePath)
		{
			relativeFilePath = OptimizeRelativeFilePath(relativeFilePath);
			Debug.Log("relativeFilePath:" + relativeFilePath.ToString());
			return XJ.NET.IO.FileReadWriter.ReadFile(Application.dataPath + relativeFilePath, Encoding.UTF8);
		}
		#endregion

		/// <summary>
		/// ファイルパスが相対パスかどうかを検証し、相対パスでないときは相対パスにして返します。
		/// </summary>
		/// <param name="relativeFilePath">
		/// 検証するファイルパス.
		/// </param>
		/// <returns>
		/// 相対パス.
		/// </returns>
		public static string OptimizeRelativeFilePath(string relativeFilePath)
		{
			if (relativeFilePath[0] == '/')
			{
				return relativeFilePath;
			}
			else
			{
				return "/" + relativeFilePath;
			}
		}
	}
}