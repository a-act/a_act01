﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveDataLoader : MonoBehaviour
{
	public SaveLoadFile saveLoadFile;

	[SerializeField]
	private Text dataText;

	/// <summary>
	/// Jsonから値を読み込む.
	/// </summary>
	public void Load()
	{
		try
		{
			saveLoadFile.saveData = null;
			saveLoadFile.saveData = JsonUtility.FromJson<SaveData>
					(XJ.Unity3D.IO.FileReadWriter.ReadFileFromAssets(Constants.SAVEDATA_PATH));
		}
		catch
		{
			dataText.text = "エラー：セーブデータがないか、正常にロードができませんでした。";
			Debug.Log("error：There was no saved data or loading was not successful.");
			/*ここでセーブデータ作成処理 or データがないのでロード処理の中断をする*/
			saveLoadFile.saveData.IsStageNumber = Constants.SCENE_NAME.Title;
		}
		finally
		{
			if (saveLoadFile.saveData != null)
			{
				dataText.text = "ロード完了！";
				Debug.Log("Load complete!");
			}
		}
	}
}