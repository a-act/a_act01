﻿using UnityEngine;
using System.Collections;

/*
 セーブデータはpublic又は[SerializeField]されたデータしか保存できない.
*/
[System.Serializable]
public class SaveData : object
{
	#region Field
	[SerializeField]
	private Vector3 playerPos;          // Playerの位置.
	[SerializeField]
	private Vector3 cameraCollisionPos;	// カメラコリジョンの位置.
	[SerializeField]
	private Vector2 savePoint;			// どこのセーブポイントか.
	[SerializeField]
	private Constants.SCENE_NAME stageNumber;   // ステージ番号.

	public class SexCount
	{
		[SerializeField]
		private int gobline = 0;
		[SerializeField]
		private int orc = 0;
		[SerializeField]
		private int bossOrc = 0;

		#region get/set
		public int IsGobline
		{
			get { return gobline; }
			set { gobline += value; }
		}
		public int IsOrc
		{
			get { return orc; }
			set { orc += value; }
		}
		public int IsBossOrc
		{
			get { return bossOrc; }
			set { bossOrc += value; }
		}
		#endregion
	}

	#region get/set
	public Vector3 IsPlayerPos
	{
		get { return playerPos; }
		set { playerPos = value; }
	}
	public Vector3 IsCameraCollisionPos
	{
		get { return cameraCollisionPos; }
		set { cameraCollisionPos = value; }
	}
	public Vector2 IsSavePoint
	{
		get { return savePoint; }
		set { savePoint = value; }
	}
	public Constants.SCENE_NAME IsStageNumber
	{
		get { return stageNumber; }
		set	{ stageNumber = value; }
	}
	#endregion get/set
	#endregion Field


	/// <summary>
	/// ログをセット.
	/// </summary>
	/// <returns></returns>
	public string GetNormalData()
	{
		string log;
		log = "playerPos:" + playerPos + ", " +
				"cameraCollisionPos:" + cameraCollisionPos + ", " +
				"savePoint:" + savePoint + ", " +
				"stageNumber:" + stageNumber;
		return log;
	}

	/// <summary>
	/// JsonDataの取得.
	/// </summary>
	/// <returns></returns>
	public string GetJsonData()
	{
		return JsonUtility.ToJson(this);
	}

	/// <summary>
	/// Playerポジションの初期化.
	/// </summary>
	public void ResetPosValue()
	{
		IsPlayerPos = new Vector3(5.0f, 1.3f, -3.0f);
		cameraCollisionPos = Vector3.zero;
	}

	/// <summary>
	/// データの初期化.
	/// </summary>
	public void ResetValue()
	{
		ResetPosValue();
		savePoint = IsPlayerPos;
		stageNumber = Constants.SCENE_NAME.Stage01;
	}
}