﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextStage : MonoBehaviour
{
	[SerializeField]
	private Constants.SCENE_NAME nextSceneName;

	SaveLoadFile saveManager;
	InputManager inputManager;

	public Constants.SCENE_NAME IsNextSceneName
	{
		get { return nextSceneName; }
		set	{ nextSceneName = value; }
	}


	void Start()
	{
		saveManager = GameObject.Find("Manager/SaveManager").GetComponent<SaveLoadFile>();
		inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
	}

	void ChangeNextScene()
	{
		//saveManager.ResetSponsePosition();
		saveManager.SetPlayerDataStageChange();
		FadeManager.Instance.LoadScene(nextSceneName.ToString(), 0.5f);
	}

	private void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			var input = inputManager.DirectionalInput;
			if (input.y == 1)
			{
				ChangeNextScene();
			}
		}
	}
}