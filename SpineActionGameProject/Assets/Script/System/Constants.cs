﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public sealed class Constants
{
	#region Path
	public static readonly string SCENARIO_PATH = "Scenario/";
	public static readonly string EFFECT_PATH = "Prefab/Effect/";
	public static readonly string SHURIKEN = "Shuriken/";

	public static readonly string EXPLOSION_PATH = EFFECT_PATH + SHURIKEN + "eff_burst_spark";
	public static readonly string SAVEDATA_PATH = "SaveData/saveData.dat";
	public static readonly string OPTIONDATA_PATH = "SaveData/optionData.dat";


	#region CG関係のパス.
	public static readonly string UNDERSCORE = "_";

	public static readonly string CG_LABEL_GOBLIN = "Goblin";
	public static readonly string CG_LABEL_ORC = "Orc";
	public static readonly string CG_LABEL_BOSSORC = "BossOrc";

	public static readonly string ENEMY_TYPE_NORMAL = "_A";
	public static readonly string ENEMY_TYPE_ABNORMALITY = "_B";

	public static readonly string PATTERN_01 = "_01";
	public static readonly string PATTERN_02 = "_02";
	public static readonly string PATTERN_03 = "_03";


	public static readonly string CG_IMAGE_PATH = "CG/Images";

	public static readonly string CG_PATH_GOBLIN = "/Goblin";
	public static readonly string CG_PATH_BOSSORC = "/BossOrc";
	
	public static readonly string ENEMYTYPE_NORMAL = "/Normal";

	public static readonly string PATTERN = "/Pattern";
	#endregion


	public static readonly int SAVE_DATA_QUANTITY = 3;
	#endregion

	public static readonly string LAYER_NAME_PLAYER = "Player";
	public static readonly string LAYER_NAME_DASH = "Dash";
	public static readonly string LAYER_NAME_DEATH = "Death";

	public enum SCENE_NAME
	{
		Title,
		Stage01,
		Stage02,
		Stage03,
		Stage04,
		Stage05,
		CG,
	}

	public enum BGM_NAME
	{
		Title,
		Stage01,
		BossOrc,
		GameOver,
		Hscene,
	}

	public enum HEADER_NAME
	{
		MainMenu,
		Load,
		Save,
		Animation,
		Gallery,
		Option,
		Pose,
	}

	public enum ENEMY_NAME
	{
		gobline,
		orc,
		bossOrc,
		bossOrc2,
	}


	public static void ExportLogText(string txt)
	{
#if UNITY_EDITOR
		Debug.Log(txt);
#elif UNITY_STANDALONE
		StreamWriter streamWriter = new StreamWriter("LogData.txt", true);
		//StreamWriter streamWriter = new StreamWriter("LogData.txt", false);
		streamWriter.WriteLine(txt);
		streamWriter.Flush();
		streamWriter.Close();
#endif
	}
}