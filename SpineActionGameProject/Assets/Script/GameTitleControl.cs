﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTitleControl : MonoBehaviour
{
	[SerializeField]
	private Animator gameTitle;
	[SerializeField]
	private GameObject MainMenuObject;      // メインメニュー.

	GameObject header;

	void Start()
	{
		header = GameObject.Find("DontDestroyObjects/Canvas/Op_Header");
		header.SetActive(false);
	}

	void Update()
	{
		if (Input.anyKeyDown)
		{
			StartCoroutine(PushButton());
		}
	}

	IEnumerator PushButton()
	{
		AnimatorStateInfo nowState = gameTitle.GetCurrentAnimatorStateInfo(0);
		if (!nowState.IsName("Loop"))
		{
			yield break;
		}

		gameTitle.SetBool("PushButton", true);
	}

	void ActiveMenu()
	{
		MainMenuObject.SetActive(true);
		header.SetActive(true);
		gameObject.SetActive(false);
	}
}