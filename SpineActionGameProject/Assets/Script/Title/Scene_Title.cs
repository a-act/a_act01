﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Scene_Title : MonoBehaviour
{
	[SerializeField]
	private GameObject loadMenuObject;      // ロード.
	[SerializeField]
	private Constants.SCENE_NAME loadSceneName;
	[SerializeField]
	private GameObject loadWindow;          // ロード画面.

	SaveLoadFile saveManager;
	ChangeText changeText;
	GameObject header;


	public void Start()
	{
		saveManager = GameObject.Find("Manager/SaveManager").GetComponent<SaveLoadFile>();
		header = GameObject.Find("DontDestroyObjects/Canvas/Op_Header");
		changeText = GameObject.Find("DontDestroyObjects/Canvas/Op_Header/Op_Position/Text_Title").GetComponent<ChangeText>();
	}


	/// <summary>
	/// 新しくゲームを始める.
	/// </summary>
	public void OnNewGameButton()
	{
		FadeManager.Instance.LoadScene(Constants.SCENE_NAME.Stage01.ToString(), 1.0f);
		header.SetActive(false);
	}

	public void OnLoadGameButton()
	{
		changeText.ChangeHeaderText(Constants.HEADER_NAME.Load);
		//MainMenuObject.SetActive(false);
		//loadWindow.SetActive(true);
	}

	public void OnAnimationButton()
	{
		changeText.ChangeHeaderText(Constants.HEADER_NAME.Animation);
	}

	public void OnGalleryButton()
	{
		changeText.ChangeHeaderText(Constants.HEADER_NAME.Gallery);
	}

	public void OnOptionButton()
	{
		changeText.ChangeHeaderText(Constants.HEADER_NAME.Option);
	}

	public void LoadingData()
	{
		saveManager.Load();
		if (saveManager.IsStageNumber != Constants.SCENE_NAME.Title)
		{
			loadSceneName = saveManager.IsStageNumber;
			FadeManager.Instance.LoadScene(loadSceneName.ToString(), 1.5f);
		}
		else
		{
			Debug.Log("error：There was no saved data or loading was not successful.");
		}
	}
}