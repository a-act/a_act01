﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsManager : MonoBehaviour
{
	int frameCount;
	float nextTime;

	public Text fpsText;

	// Use this for initialization
	void Start()
	{
		nextTime = Time.time + 1;
	}

	// Update is called once per frame
	void Update()
	{
		frameCount++;

		if (Time.time >= nextTime)
		{
			// 1秒経ったらFPSを表示
			//Debug.Log("FPS : " + frameCount);
			fpsText.text = frameCount + "fps";
			frameCount = 0;
			nextTime += 1;
		}
	}
	//void OnGUI()
	//{
	//	string text = string.Format("{0}fps", frameCount);
	//	GUI.Label(new Rect(0, 0, Screen.width, Screen.height), text);
	//}
}