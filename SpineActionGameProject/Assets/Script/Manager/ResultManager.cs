﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultManager : MonoBehaviour
{
	[SerializeField]
	private GameObject gameOverUI;
	[SerializeField]
	private GameObject stillImagePrefab;
	[SerializeField]
	private Sprite[] stillSprites;

	GameObject gameOver;
	GameObject stillImage;


	public void ToGameOver()
	{
		if (gameOver == null)
		{
			gameOver = Instantiate(gameOverUI) as GameObject;
			gameOver.transform.SetParent(GameObject.Find("DontDestroyObjects/Canvas/Result").transform, false);
			gameOver.GetComponent<RectTransform>().localPosition = Vector3.zero;
		}
	}

	public void ToGameOverCG(Constants.ENEMY_NAME eName, int count)
	{
		if (gameOver == null)
		{
			gameOver = Instantiate(gameOverUI) as GameObject;
			gameOver.transform.SetParent(GameObject.Find("DontDestroyObjects/Canvas/Result").transform, false);
			gameOver.GetComponent<RectTransform>().localPosition = Vector3.zero;

			if (count % 3 == 0)
			{
				stillImage = Instantiate(stillImagePrefab) as GameObject;
				stillImage.transform.SetParent(GameObject.Find("DontDestroyObjects/Canvas/Result/Continue(Clone)/BG_Image").transform, false);
				var image = stillImage.GetComponent<Image>();

				switch (CGController.enemyName)
				{
					case Constants.ENEMY_NAME.gobline:
					case Constants.ENEMY_NAME.orc:
						stillImage.SetActive(true);
						image.sprite = stillSprites[0];
						break;
					case Constants.ENEMY_NAME.bossOrc:
					case Constants.ENEMY_NAME.bossOrc2:
						stillImage.SetActive(true);
						image.sprite = stillSprites[1];
						break;
					default:
						break;
				}
			}
		}
	}
}