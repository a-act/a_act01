﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
	[SerializeField, Tooltip("ゴブリン召喚キー")]
	private KeyCode goblinCreateKey = KeyCode.Alpha1;
	[SerializeField, Tooltip("オーク召喚キー")]
	private KeyCode orcCreateKey = KeyCode.Alpha2;
	[SerializeField, Tooltip("Bossオーク召喚キー")]
	private KeyCode bossOrcCreateKey = KeyCode.Alpha3;
	[SerializeField, Tooltip("スポーン位置")]
	private Vector2 bossOrcSpawnPos;
	[SerializeField]
	Entity_EnemyPosData posData;

	public GameObject goblinPrefab;
	public GameObject orcPrefab;
	public GameObject bossOrcPrefab;
	public GameObject bossLifeGaugePrefab;

	private List<GameObject> enemyObject = new List<GameObject>();

	private GameObject canvas;
	private GameObject character;
	private GameObject enemyPrefab;
	private GameObject bossObject;
	private GameObject bossLifeGaugeObject;
	private Transform player;

	int respawnCount;


	void Start()
	{
		canvas = GameObject.Find("CanvasUI");
		character = GameObject.Find("Character/Enemy");
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

		for (int i = 0; i < posData.param.Count; i++)
		{
			switch (posData.param[i].ID)
			{
				case 1:
					enemyPrefab = goblinPrefab;
					break;
				case 2:
					enemyPrefab = orcPrefab;
					break;
				default:
					enemyPrefab = goblinPrefab;
					break;
			}

			EnemyCreater(enemyPrefab, new Vector2(posData.param[i].X, posData.param[i].Y));
		}
	}

	void Update()
	{
		if (Debug.isDebugBuild)
		{
			if (Input.GetKeyDown(goblinCreateKey))
			{
				EnemyCreater(goblinPrefab);
			}
			else if (Input.GetKeyDown(orcCreateKey))
			{
				EnemyCreater(orcPrefab);
			}

			if (Input.GetKeyDown(bossOrcCreateKey))
			{
				BossOrcCreater();
			}
		}
	}


	private void EnemyCreater(GameObject obj, Vector2 pos)
	{
		enemyObject.Add(Instantiate(obj, new Vector3(pos.x, pos.y, -1.0f), Quaternion.identity));
		var tmp = enemyObject.Count - 1;
		enemyObject[tmp].transform.parent = character.transform;
	}
	private void EnemyCreater(GameObject obj)
	{
		var tmpPos = player.transform.position;

		var num = enemyObject.IndexOf(null);
		if (num >= 0)
		{
			enemyObject[num] = Instantiate(obj, new Vector3(tmpPos.x + 5.0f, tmpPos.y + 5.0f, -1.0f), Quaternion.identity);
			enemyObject[num].transform.parent = character.transform;
		}
		else
		{
			enemyObject.Add(Instantiate(obj, new Vector3(tmpPos.x + 5.0f, tmpPos.y + 5.0f, -1.0f), Quaternion.identity));
			var tmp = enemyObject.Count - 1;
			enemyObject[tmp].transform.parent = character.transform;
		}
	}
	public void RespawnEnemy()
	{
		respawnCount++;
		if (respawnCount >= 3)
		{
			var num = enemyObject.IndexOf(null);
			while(num >= 0)
			{
				switch (posData.param[num].ID)
				{
					case 1:
						enemyPrefab = goblinPrefab;
						break;
					case 2:
						enemyPrefab = orcPrefab;
						break;
					default:
						enemyPrefab = goblinPrefab;
						break;
				}
				enemyObject[num] = Instantiate(enemyPrefab, new Vector3(posData.param[num].X, posData.param[num].Y, -1.0f), Quaternion.identity);
				enemyObject[num].transform.parent = character.transform;

				num = enemyObject.IndexOf(null);
			}
			respawnCount = 0;
		}
	}

	private void BossOrcCreater()
	{
		if (bossObject == null)
		{
			var tmpPos = player.transform.position;
			//bossObject = Instantiate(bossOrcPrefab, new Vector3(bossOrcSpawnPos.x, bossOrcSpawnPos.y, -1.0f), Quaternion.identity);
			bossObject = Instantiate(bossOrcPrefab, new Vector3(tmpPos.x + 5.0f, tmpPos.y + 5.0f, -1.0f), Quaternion.identity);
			bossObject.transform.parent = character.transform;

			bossLifeGaugeObject = Instantiate(bossLifeGaugePrefab) as GameObject;
			bossLifeGaugeObject.transform.SetParent(canvas.transform, false);
		}
	}
}