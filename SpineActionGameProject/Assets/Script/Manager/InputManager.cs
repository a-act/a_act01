﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure; // Required in C#

public class InputManager : MonoBehaviour
{
	#region キー入力.
	[Tooltip("左移動(←)")]
	public KeyCode LeftMoveKey = KeyCode.LeftArrow;
	public KeyCode LeftMoveSubKey = KeyCode.A;
	//public KeyCode LeftMoveJoystick = "joystick button 8";
	[Tooltip("右移動(→)")]
	public KeyCode rightMoveKey = KeyCode.RightArrow;
	public KeyCode rightMoveSubKey = KeyCode.D;
	[Tooltip("しゃがみ(↓)")]
	public KeyCode crouchKey = KeyCode.DownArrow;
	public KeyCode crouchSubKey = KeyCode.S;
	[Tooltip("エリア移動(↑)")]
	public KeyCode upKey = KeyCode.UpArrow;
	public KeyCode upSubKey = KeyCode.W;
	[Tooltip("近接攻撃")]
	public KeyCode nearAttackKey = KeyCode.Z;
	public KeyCode nearAttackSubKey = KeyCode.Keypad4;
	[Tooltip("遠距離攻撃")]
	public KeyCode farAttackKey = KeyCode.X;
	public KeyCode farAttackSubKey = KeyCode.Keypad6;
	[Tooltip("必殺技")]
	public KeyCode specialSkillKey = KeyCode.V;
	public KeyCode specialSkillSubKey = KeyCode.Keypad0;
	[Tooltip("Life回復")]
	public KeyCode healingKey = KeyCode.F;
	public KeyCode healingSubKey = KeyCode.Keypad9;
	[Tooltip("AP回復")]
	public KeyCode healingArmorPointKey = KeyCode.G;
	public KeyCode healingArmorPointSubKey = KeyCode.Keypad7;
	[Tooltip("ジャンプ")]
	public KeyCode jumpKey = KeyCode.C;
	public KeyCode jumpSubKey = KeyCode.Keypad8;
	[Tooltip("ダッシュ")]
	public KeyCode dashKey = KeyCode.LeftShift;
	public KeyCode dashSubKey = KeyCode.Keypad5;
	#endregion

	private AchievementManager achievementManager;
	private GamePadState state;
	private GamePadState prevState;
	
	private Vector2 directionalInput = Vector2.zero;

	private PlayerIndex playerIndex;

	private bool playerIndexSet = false;
	private bool padJump = false;
	private bool nearAttackUp = false;
	private bool nearAttackDown = false;
	private bool farAttack = false;
	private bool padDash = false;

	private float hori;
	private float vert;

	[Flags]
	public enum InputField
	{
		jump = 0x01,
		dash = 0x02,
		crouch = 0x04,
		crouchAttack = 0x08,
		nearAttack = 0x10,
		farAttack = 0x20,
		lifeHeel = 0x40,
		apHeel = 0x80,
	}
	protected InputField inputFlag;

	public Vector2 DirectionalInput
	{
		get { return directionalInput; }
	}

	public InputField InputFlag
	{
		get { return inputFlag; }
	}


	//void Start()
	//{
	//	achievementManager = GameObject.Find("Manager/AchievementManager").GetComponent<AchievementManager>();
	//}

	void Update()
	{
		directionalInput = Vector2.zero;
		hori = Input.GetAxis("GamePadHorizontal");
		vert = Input.GetAxis("GamePadVertical");
		padJump = Input.GetButtonDown("GamePadJump");
		nearAttackUp = Input.GetButtonUp("GamePadNearAttack");
		nearAttackDown = Input.GetButtonDown("GamePadNearAttack");
		farAttack = Input.GetButtonDown("GamePadFarAttack");
		padDash = Input.GetButtonDown("GamePadDash");


		// 右移動.
		if ((Input.GetKey(rightMoveKey) || Input.GetKey(rightMoveSubKey)) || hori == 1)
		{
			directionalInput.x = 1;
		}
		// 左移動.
		else if ((Input.GetKey(LeftMoveKey) || Input.GetKey(LeftMoveSubKey)) || hori == -1)
		{
			directionalInput.x = -1;
		}
		else
		{
			directionalInput.x = 0;
		}

		// しゃがむ.
		if ((Input.GetKey(crouchKey) || Input.GetKey(crouchSubKey)) || vert == -1)
		{
			directionalInput.y = -1;
			inputFlag = InputField.crouch;

			// しゃがみ攻撃.
			if ((Input.GetKeyDown(nearAttackKey) || Input.GetKeyDown(nearAttackSubKey)) || nearAttackDown == true)
			{
				inputFlag |= InputField.crouchAttack;
			}
			else
			{
				inputFlag &= ~InputField.crouchAttack;
			}
		}
		else if ((Input.GetKeyDown(upKey) || Input.GetKeyDown(upSubKey)) || vert == 1)
		{
			directionalInput.y = 1;
		}
		else
		{
			directionalInput.y = 0;
			inputFlag &= ~InputField.crouch;

			// 近接攻撃.
			if ((Input.GetKeyDown(nearAttackKey) || Input.GetKeyDown(nearAttackSubKey)) || nearAttackDown == true)
			{
				inputFlag |= InputField.nearAttack;
			}
			else
			{
				inputFlag &= ~InputField.nearAttack;
			}

			// 遠距離攻撃.
			//if ((Input.GetKeyDown(farAttackKey) || Input.GetKeyDown(farAttackSubKey)) || farAttack == true)
			//{
			//	player.OnFarAttack();
			//}

			// ジャンプ.
			if ((Input.GetKeyDown(jumpKey) || Input.GetKeyDown(jumpSubKey)) || padJump == true)
			{
				inputFlag |= InputField.jump;
			}
			else
			{
				inputFlag &= ~InputField.jump;
			}

			// ダッシュ.
			if ((Input.GetKeyDown(dashKey) || Input.GetKeyDown(dashSubKey)) || padDash == true)
			{
				inputFlag |= InputField.dash;
			}
			else
			{
				inputFlag &= ~InputField.dash;
			}
		}


		// 必殺技.
		//if (Input.GetKeyDown(specialSkillKey) || Input.GetKeyDown(specialSkillSubKey))
		//{
		//	//player.OnSpecialSkill();
		//}

		// Life回復.
		if (Input.GetKeyDown(healingKey) || Input.GetKeyDown(healingSubKey))
		{
			inputFlag = InputField.lifeHeel;
		}
		else
		{
			inputFlag &= ~InputField.lifeHeel;
		}

		// AP回復.
		if (Debug.isDebugBuild && (Input.GetKeyDown(healingArmorPointKey) || Input.GetKeyDown(healingArmorPointSubKey)))
		{
			inputFlag = InputField.apHeel;
		}
		else
		{
			inputFlag &= ~InputField.apHeel;
		}
	}

	private void OnDisable()
	{
		InvalidInput();
	}

	void InvalidInput()
	{
		directionalInput = Vector2.zero;
		inputFlag = 0;
	}
}