﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour
{
	[SerializeField, Tooltip("どのキーで開くか")]
	private KeyCode openKey = KeyCode.G;
	[SerializeField]
	private GameObject pauseMenuObj;
	[SerializeField]
	private WhereAnimator whereAnimator;
	[SerializeField]
	private Pausable pausable;

	private GameObject optionSound;


	private void Start()
	{
		optionSound = GameObject.Find("DontDestroyObjects/Canvas/Option_Sound");
	}

	void Update()
	{
		if (Input.GetKeyDown(openKey))
		{
			if (pauseMenuObj.activeSelf)
			{
				// アニメーションの終了を待ってから非アクティブに.
				if (!optionSound.activeSelf)
				{
					Pauser.Resume();
					Enable();
				}
			}
			else
			{
				Activate();
				Pauser.Pause();
			}
		}
	}

	public void Activate()
	{
		if (pausable.SetPausable)
		{
			pausable.SetPausable = false;
			pauseMenuObj.SetActive(false);
		}
		else
		{
			pausable.SetPausable = true;
			pauseMenuObj.SetActive(true);
		}
	}

	public void Enable()
	{
		whereAnimator.OnWhereAnimation();
	}
}