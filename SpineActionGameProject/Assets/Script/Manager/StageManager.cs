﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
	#region Field
	public GameObject playerPrefab;
	public GameObject playerLifeGaugePrefab;

	private GameObject playerLifeGaugeObject;
	private GameObject canvas;
	private GameObject result;
	#endregion


	void Awake()
	{
		canvas = GameObject.Find("CanvasUI");

		playerLifeGaugeObject = Instantiate(playerLifeGaugePrefab) as GameObject;
		playerLifeGaugeObject.transform.SetParent(canvas.transform, false);
	}
}