﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class AchievementManager : MonoBehaviour
{
	[SerializeField]
	private GameObject achievementPanel;
	private Image image;
	private Text title;
	private Text text;

	[Tooltip("マップデータ(CSV形式)")]
	public TextAsset csvFile;                   // CSVファイルを読み込むために使う.
	[SerializeField]
	[Tooltip("CSVデータの行数")]
	private int rows = 15;                      // CSVデータの行数.
	[Tooltip("CSVデータの列数")]
	private int columns = 3;                    // CSVデータの列数.

	string str = "";                            // CSVの全文文字を保存する.
	string strget = "";                         // 取り出した文字を保存する.

	string[,] csvData = new string[20, 20];     // 文字列を格納する変数[行(縦), 列(横)].
	int[] iDat = new int[15];                   // 文字検索用.

	private GameObject panelObject = null;
	private GameObject panelBaseObject = null;
	private GameObject canvas;

	[Flags]
	public enum AchievementFlag
	{
		Achievement01 = 0x0001,     // こいつ…動くぞ！(初めて歩いた).
		Achievement02 = 0x0002,     // 2進数だと0000 0000 0000 0010.
		Achievement03 = 0x0004,     // 2進数だと0000 0000 0000 0100.
		Achievement04 = 0x0008,     // 2進数だと0000 0000 0000 1000.
		Achievement05 = 0x0010,     // 2進数だと0000 0000 0001 0000.
		Achievement06 = 0x0020,     // 2進数だと0000 0000 0010 0000.
		Achievement07 = 0x0040,     // 2進数だと0000 0000 0100 0000.
		Achievement08 = 0x0080,     // 2進数だと0000 0000 1000 0000.
		Achievement09 = 0x0100,     // 2進数だと0000 0001 0000 0000.
		Achievement10 = 0x0200,     // 2進数だと0000 0010 0000 0000.
		Achievement11 = 0x0400,     // 2進数だと0000 0100 0000 0000.
		Achievement12 = 0x0800,     // 2進数だと0000 1000 0000 0000.
		Achievement13 = 0x1000,     // 2進数だと0001 0000 0000 0000.
		Achievement14 = 0x2000,     // 2進数だと0010 0000 0000 0000.
		Achievement15 = 0x4000,     // 2進数だと0100 0000 0000 0000.
		Achievement16 = 0x8000,     // 2進数だと1000 0000 0000 0000.
	}
	private AchievementFlag achievementFlag;
	private AchievementFlag achievementFlagStoring;

	int a = 0;                                  // 濫用 数値型変数.
	int b = 0;                                  // 濫用 数値型変数.
	int c = 0;                                  // 濫用 数値型変数.


	void Start()
	{
		canvas = GameObject.Find("CanvasUI");

		// マップ番号を格納するマップ用変数.
		csvData = new string[rows, columns];

		#region ここでCSVデータをstrに保存.
		if (csvFile == null)
		{
			Debug.LogError("There is no CSVFile!!");
			return;
		}
		StringReader reader = new StringReader(csvFile.text);

		while (reader.Peek() > -1)
		{
			string line = reader.ReadLine();
			// 最後に検索文字列の","を追記。 これがないと最後の文字を取りこぼす.
			str += ",";
			str += line;
		}

		// 最後に検索文字列の","を追記。 これがないと最後の文字を取りこぼす.
		str += ",";
		#endregion

		#region ここでCSVデータを配列変数csvDataに保存.
		for (int c = 0; c < rows; c++)
		{
			for (int i = 0; i < columns; i++)
			{
				try
				{
					// ","を検索.
					iDat[0] = str.IndexOf(",", iDat[0]);
				}
				catch
				{
					break;
				}

				try
				{
					// 次の","を検索.
					iDat[1] = str.IndexOf(",", iDat[0] + 1);
				}
				catch
				{
					break;
				}

				// 何文字取り出すか決定.
				iDat[2] = iDat[1] - iDat[0] - 1;

				try
				{
					// iDat[2]文字ぶんだけ取り出す.
					strget = str.Substring(iDat[0] + 1, iDat[2]);
				}
				catch
				{
					break;
				}

				// マップ用変数に保存。1とか6とか数字が入るよ.
				csvData[a, b] = strget;
				// 一つ右のマップ用変数へ.
				b++;
				// 次のインデックスへ.
				iDat[0]++;
			}

			// 一つ下のマップチップへ.
			a++;
			// マップチップ格納を一番左に戻す.
			b = 0;
		}
		#endregion
	}

	//void Update()
	//{
	//	if (Input.GetKeyDown(KeyCode.Alpha1))
	//	{
	//		SetAchievementFlag(AchievementFlag.Achievement01);
	//	}
	//	if (Input.GetKeyDown(KeyCode.Alpha2))
	//	{
	//		SetAchievementFlag(AchievementFlag.Achievement02);
	//	}
	//	if (Input.GetKeyDown(KeyCode.Alpha3))
	//	{
	//		SetAchievementFlag(AchievementFlag.Achievement03);
	//	}
	//	if (Input.GetKeyDown(KeyCode.Alpha4))
	//	{
	//		SetAchievementFlag(AchievementFlag.Achievement04);
	//	}
	//}

	/// <summary>
	/// 実績解除フラグをたてる.
	/// </summary>
	/// <param name="flag"></param>
	public void SetAchievementFlag(AchievementFlag flag)
	{
		achievementFlag |= flag;

		if (achievementFlagStoring != achievementFlag)
		{
			StartCoroutine(AchievementNewCreate());
			StartCoroutine(AchievementTextChanger(flag));
		}
	}

	/// <summary>
	/// 実績解除フラグをおる.
	/// </summary>
	/// <param name="flag"></param>
	public void FoldAchievementFlag(AchievementFlag flag)
	{
		achievementFlag ^= flag;
	}

	IEnumerator AchievementNewCreate()
	{
		if (panelObject != null)
		{
			Destroy(panelObject);
		}
		panelObject = Instantiate(achievementPanel) as GameObject;
		panelObject.transform.parent = canvas.transform;
		panelObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
		panelBaseObject = GameObject.Find("CanvasUI/Achievement(Clone)/BaseImage");
		image = panelBaseObject.transform.Find("Image").GetComponent<Image>();
		title = panelBaseObject.transform.Find("Title").GetComponent<Text>();
		text = panelBaseObject.transform.Find("Text").GetComponent<Text>();
		achievementFlagStoring |= achievementFlag;
		yield return null;
	}

	/// <summary>
	/// 実績の表示する内容を変更.
	/// </summary>
	/// <returns></returns>
	IEnumerator AchievementTextChanger(AchievementFlag flag)
	{
		int num = 0;

		switch ((achievementFlagStoring & achievementFlag) & flag)
		{
			case AchievementFlag.Achievement01:
				num = 1;
				break;
			case AchievementFlag.Achievement02:
				num = 2;
				break;
			case AchievementFlag.Achievement03:
				num = 3;
				break;
			case AchievementFlag.Achievement04:
				num = 4;
				break;
			case AchievementFlag.Achievement05:
				num = 5;
				break;
			case AchievementFlag.Achievement06:
				num = 6;
				break;
			case AchievementFlag.Achievement07:
				num = 7;
				break;
			case AchievementFlag.Achievement08:
				num = 8;
				break;
			case AchievementFlag.Achievement09:
				num = 9;
				break;
			case AchievementFlag.Achievement10:
				num = 10;
				break;
			case AchievementFlag.Achievement11:
				num = 11;
				break;
			case AchievementFlag.Achievement12:
				num = 12;
				break;
			case AchievementFlag.Achievement13:
				num = 13;
				break;
			case AchievementFlag.Achievement14:
				num = 14;
				break;
			case AchievementFlag.Achievement15:
				num = 15;
				break;
			case AchievementFlag.Achievement16:
				num = 16;
				break;
			default:
				break;
		}

		title.text = csvData[num, 1];
		text.text = csvData[num, 2];

		yield return null;
	}
}