﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
/* https://onosendai.net/70/ */
/// <summary>
/// シーン遷移時のフェードイン・アウトを制御するためのクラス.
/// </summary>
public class FadeManager : MonoBehaviour
{

	#region Singleton

	private static FadeManager instance;

	public static FadeManager Instance
	{
		get
		{
			if (instance == null)
			{
				instance = (FadeManager)FindObjectOfType(typeof(FadeManager));

				if (instance == null)
				{
					Debug.LogError(typeof(FadeManager) + "is nothing");
				}
			}

			return instance;
		}
	}

	#endregion Singleton

	/// <summary>フェード中の透明度</summary>
	private float fadeAlpha = 0;
	/// <summary>フェード中かどうか</summary>
	private bool isFading = false;
	/// <summary>フェード色</summary>
	public Color fadeColor = Color.black;

	// フェードインアウトのフラグ
	public static bool isFadeIn = false;
	public static bool isFadeOut = false;

	public bool IsFading
	{
		get { return isFading; }
	}

	public void Awake()
	{
		if (this != Instance)
		{
			Destroy(this.gameObject);
			return;
		}

		DontDestroyOnLoad(this.gameObject);
	}

	public void OnGUI()
	{
		// Fade.
		if (this.isFading)
		{
			// 色と透明度を更新して白テクスチャを描画.
			this.fadeColor.a = this.fadeAlpha;
			GUI.color = this.fadeColor;
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Texture2D.whiteTexture);
		}

		if (Debug.isDebugBuild)
		{
			if (!this.isFading)
			{
				// Scene一覧を作成.
				// (UnityEditor名前空間を使わないと自動取得できなかったので決めうちで作成).
				List<string> scenes = new List<string>();
				scenes.Add("Main");
				scenes.Add(Constants.SCENE_NAME.Title.ToString());
				scenes.Add(Constants.SCENE_NAME.Stage01.ToString());
				// scenes.Add (Constants.SCENE_NAME.Stage02.ToString());
				// scenes.Add (Constants.SCENE_NAME.Stage03.ToString());
				// scenes.Add (Constants.SCENE_NAME.Stage04.ToString());
				// scenes.Add (Constants.SCENE_NAME.Stage05.ToString());
				scenes.Add(Constants.SCENE_NAME.CG.ToString());


				// Sceneが一つもない.
				if (scenes.Count == 0)
				{
					GUI.Box(new Rect(10, 10, 200, 50), "Fade Manager(Debug Mode)");
					GUI.Label(new Rect(20, 35, 180, 20), "Scene not found.");
					return;
				}


				GUI.Box(new Rect(10, 10, 300, 50 + scenes.Count * 25), "Fade Manager(Debug Mode)");
				GUI.Label(new Rect(20, 30, 280, 20), "Current Scene : " + SceneManager.GetActiveScene().name);

				int i = 0;
				foreach (string sceneName in scenes)
				{
					if (GUI.Button(new Rect(20, 55 + i * 25, 100, 20), "Load Scene"))
					{
						LoadScene(sceneName, 1.0f);
					}
					GUI.Label(new Rect(125, 55 + i * 25, 1000, 20), sceneName);
					i++;
				}
			}
		}
	}

	private void Start()
	{
		isFading = true;
		StartCoroutine(FadeIn(1.0f));
	}

	/// <summary>
	/// 画面遷移.
	/// </summary>
	/// <param name='scene'>シーン名</param>
	/// <param name='interval'>暗転にかかる時間(秒)</param>
	public void LoadScene(string scene, float interval)
	{
		StartCoroutine(TransScene(scene, interval));
	}

	/// <summary>
	/// シーン遷移用コルーチン.
	/// </summary>
	/// <param name='scene'>シーン名</param>
	/// <param name='interval'>暗転・明転にかかる時間(秒)</param>
	private IEnumerator TransScene(string sceneName, float interval)
	{
		// だんだん暗く.
		this.isFading = true;
		float time = 0;
		while (time <= interval)
		{
			this.fadeAlpha = Mathf.Lerp(0f, 1f, time / interval);
			time += Time.deltaTime;
			yield return null;
		}

		if (SceneManager.GetActiveScene().name != sceneName)
		{
			// シーン切替.
			SceneManager.LoadScene(sceneName);
			// シーンが読み込まれたか.
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		// だんだん明るく.
		time = 0;
		while (time <= interval)
		{
			this.fadeAlpha = Mathf.Lerp(1f, 0f, time / interval);
			time += Time.deltaTime;
			yield return null;
		}

		this.isFading = false;
	}

	/// <summary>
	/// フェードイン・フェードアウト.
	/// </summary>
	/// <param name="fadeType"></param>
	/// <param name="interval"></param>
	public void FadeInOut(bool fadeType, float interval = 0f)
	{
		if (fadeType)
		{
			StartCoroutine(FadeOut(interval));
		}
		else
		{
			StartCoroutine(FadeIn(interval));
		}
	}

	/// <summary>
	/// フェードアウト.
	/// </summary>
	/// <param name="interval">暗転にかかる時間(秒)</param>
	/// <returns></returns>
	private IEnumerator FadeOut(float interval)
	{
		// だんだん暗く.
		this.isFading = true;
		float time = 0;
		while (time <= interval)
		{
			this.fadeAlpha = Mathf.Lerp(0f, 1f, time / interval);
			time += Time.deltaTime;
		}
			yield return null;
	}

	/// <summary>
	/// フェードイン.
	/// </summary>
	/// <param name="interval"></param>
	/// <returns></returns>
	private IEnumerator FadeIn(float interval)
	{
		// だんだん明るく.
		float time = 0;
		while (time <= interval)
		{
			this.fadeAlpha = Mathf.Lerp(1f, 0f, time / interval);
			time += Time.deltaTime;
		}
			yield return null;

		this.isFading = false;
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		Debug.Log(scene.name + " scene loaded");
	}
}