== Usage ==
To add the effect to your scene:

- Go to GameObject -> 3D Object -> Quad
- Assign the "CensorEffectMat" material to the object

You can of course apply this to any kind of mesh.

== Shaders ==
All shaders can be found under the FX/ parent.

- Censor, applies the censor effect over the entire mesh
- Censor (Masked Cutout), takes an alpha map to mask out the effect (hard transitions)
- Censor (Masked Smooth), takes an alpha map to mask out the effect (smooth transitions)

== Known issues ==
Anti-aliasing may induce some flickering. Though if the censored object is slightly moving, this isn't noticable.

==使用法==
シーンにエフェクトを追加するには：

- ゲームオブジェクト - > 3Dオブジェクト - >クワッド
- オブジェクトに "CensorEffectMat"マテリアルを割り当てます

もちろん、これをあらゆる種類のメッシュに適用することができます。

==シェーダ==
すべてのシェーダはFX /親の下にあります。

- Censor：メッシュ全体に検閲効果を適用します。
- Censor（Masked Cutout） - アルファマップを使用してエフェクトをマスクします（ハード遷移）
- Censor（Masked Smooth） - アルファマップを使用してエフェクトをマスクします（スムーズなトランジション）

==既知の問題==
アンチエイリアシングにより、ちらつきが発生することがあります。
 検閲されたオブジェクトがわずかに動いていても、これは目立つものではありません。