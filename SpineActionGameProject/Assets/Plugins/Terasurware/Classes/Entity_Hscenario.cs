using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_Hscenario : ScriptableObject
{	
	public List<Param> param = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		
		public int ID;
		public string CharaName;
		public string ScenarioText;
		public int CG;
		public string SE;
		public string Voice;
	}
}