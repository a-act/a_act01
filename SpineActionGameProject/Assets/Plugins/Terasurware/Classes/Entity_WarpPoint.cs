using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_WarpPoint : ScriptableObject
{	
	public List<Param> param = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		
		public string ID;
		public float Create_X;
		public float Create_Y;
		public float Spawn_X;
		public float Spawn_Y;
		public float Add;
		public bool Lock;
	}
}