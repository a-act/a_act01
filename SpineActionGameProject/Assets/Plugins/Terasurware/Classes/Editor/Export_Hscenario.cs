﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Xml.Serialization;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

public class Export_Hscenario : AssetPostprocessor
{
	private static readonly string filePath = "Assets/ExcelData/Hscenario.xlsx";
	private static readonly string[] sheetNames =
		{
			"Hscenario_BOSSOrc_01", "Hscenario_BOSSOrc_02", "Hscenario_BOSSOrc_03",
			"Hscenario_Orc_01",
			"Hscenario_Goblin_01", "Hscenario_Goblin_02", "Hscenario_Goblin_03",
		};

	static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
	{
		foreach (string asset in importedAssets)
		{
			if (!filePath.Equals(asset))
				continue;

			using (FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
			{
				IWorkbook book = null;
				if (Path.GetExtension(filePath) == ".xls")
				{
					book = new HSSFWorkbook(stream);
				}
				else
				{
					book = new XSSFWorkbook(stream);
				}

				foreach (string sheetName in sheetNames)
				{
					var exportPath = "Assets/ExcelData/LocalResource/" + sheetName + ".asset";

					// check scriptable object
					var data = (Entity_Hscenario)AssetDatabase.LoadAssetAtPath(exportPath, typeof(Entity_Hscenario));
					if (data == null)
					{
						data = ScriptableObject.CreateInstance<Entity_Hscenario>();
						AssetDatabase.CreateAsset((ScriptableObject)data, exportPath);
						data.hideFlags = HideFlags.NotEditable;
					}
					data.param.Clear();

					// check sheet
					var sheet = book.GetSheet(sheetName);
					if (sheet == null)
					{
						Debug.LogError("[QuestData] sheet not found:" + sheetName);
						continue;
					}

					// add infomation
					for (int i = 2; i <= sheet.LastRowNum; i++)
					{
						IRow row = sheet.GetRow(i);
						ICell cell = null;

						var p = new Entity_Hscenario.Param();

						cell = row.GetCell(0); p.ID = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(1); p.CharaName = (cell == null ? "" : cell.StringCellValue);
						cell = row.GetCell(2); p.ScenarioText = (cell == null ? "" : cell.StringCellValue);
						cell = row.GetCell(3); p.CG = (int)(cell == null ? 0 : cell.NumericCellValue);
						cell = row.GetCell(4); p.SE = (cell == null ? "" : cell.StringCellValue);
						cell = row.GetCell(5); p.Voice = (cell == null ? "" : cell.StringCellValue);

						data.param.Add(p);
					}

					// save scriptable object
					ScriptableObject obj = AssetDatabase.LoadAssetAtPath(exportPath, typeof(ScriptableObject)) as ScriptableObject;
					EditorUtility.SetDirty(obj);
				}
			}
		}
	}
}