using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity_EnemyPosData : ScriptableObject
{	
	public List<Param> param = new List<Param> ();

	[System.SerializableAttribute]
	public class Param
	{
		
		public int ID;
		public float X;
		public float Y;
	}
}